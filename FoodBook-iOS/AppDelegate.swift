//
//  AppDelegate.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 04/08/2021.
//

import UIKit
import ObjectMapper
import SwiftyJSON
import IQKeyboardManagerSwift
import QuickLook
import Photos

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    var pendingNotification: [AnyHashable: Any]? = nil
    let notificationCenter = UNUserNotificationCenter.current()
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UITextField.appearance().placeHolderColor = .TextPlaceholderColor
        IQKeyboardManager.shared.enable = true
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

extension AppDelegate{
    // MARK: - Functions
    class func setRootToLoginView(error: String? = nil) {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navi = storyboard.instantiateViewController(withIdentifier: "LoginNavi") as! UINavigationController
            if let window = appDelegate.window{
                window.rootViewController = navi
            }else{
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                appDelegate.window?.rootViewController = navi
            }
            appDelegate.window?.makeKeyAndVisible()
            if let error = error{
                MessageManager.showMessage(message: error, type: .STAlertError)
            }

            self.removeUserSessionInUserDefault()
        }
    }
    
    class func setRootToMainView() {
        DispatchQueue.main.async {
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let navi = storyboard.instantiateViewController(withIdentifier: "MainNav") as! UINavigationController
            if let window = appDelegate.window{
                window.rootViewController = navi
            }else{
                appDelegate.window = UIWindow(frame: UIScreen.main.bounds)
                appDelegate.window?.rootViewController = navi
            }
            appDelegate.window?.makeKeyAndVisible()
        }
    }
    
    fileprivate class func removeUserSessionInUserDefault() {
        let keys = [
            UserModel.userKey
        ]
        
        for key in keys {
            UserDefaults.standard.removeObject(forKey: key)
            UserDefaults.standard.synchronize()
        }
    }
    
    func showLocalNotification(title: String, body: String, userinfo: [AnyHashable: Any]) {
        //creating the notification content
        let content = UNMutableNotificationContent()

        //adding title, subtitle, body and badge
        content.title = title
        //content.subtitle = "local notification"
        content.body = body
        //content.badge = 1
        content.sound = UNNotificationSound.default
        content.userInfo = userinfo

        //getting the notification trigger
        //it will be called after 5 seconds
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)

        //getting the notification request
        let request = UNNotificationRequest(identifier: "SimplifiedIOSNotification", content: content, trigger: trigger)

        //adding the notification to notification center
        notificationCenter.add(request, withCompletionHandler: nil)
    }
}
