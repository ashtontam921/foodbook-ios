

import Foundation
import CommonCrypto
import UIKit
import Localize_Swift


enum LocalizedFile: String{
    case FassPay
}

extension String{
    func localizedString(with localizedFile: LocalizedFile) -> String {
        return self.localized(using: localizedFile.rawValue, in: nil)
    }
    var isNameString: Bool {
        guard self.count > 0 else { return false }
        
        return self.range(of: "[^a-zA-Z0-9/ ]", options: .regularExpression) == nil && self != ""
    }
    
    var isNumber: Bool {
        guard self.count > 0 else { return false }
        
        let nums: Set<Character> = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        return Set(self).isSubset(of: nums)
    }
    
    public func mimeType() -> String {
        return (self as NSString).mimeType()
    }
    
    func formattedServerDate(_ fromStr: String, dateFormat: String = "yyyy-MM-dd HH:mm:ss") -> Date?{
        let dateFormatter = DateFormatter.currentDateFormatter
        dateFormatter.dateFormat = dateFormat
        return dateFormatter.date(from: self)
    }
    
    public func isValidCurrencyDouble(maxDecimalPlaces: Int, minimumValue: Double? = nil, maximumValue: Double? = nil) -> Bool {
        if prefix(1) == "0", prefix(2) == "00", self.count > 1 {
            return false
        }
        let formatter = NumberFormatter()
        formatter.locale = Locale.init(identifier: "en")
        formatter.allowsFloats = true
        let decimalSeparator = formatter.decimalSeparator ?? "."
        if self == "" {
            return true
        } else if formatter.number(from: self) != nil {
            let split = self.components(separatedBy: decimalSeparator)
            guard split.count <= 2 else {
                return false
            }
            
            let digit = split.count == 2 ? split.last ?? "" : ""
            guard let doubleValue = Double(self), doubleValue != 0.00 else{
                if let minimumValue = minimumValue {
                    let minimumValueSplit = "\(minimumValue)".components(separatedBy: decimalSeparator)
                    let minimumValueDigit = minimumValueSplit.count == 2 ? minimumValueSplit.last ?? "" : ""
                    if digit.count >= minimumValueDigit.count{
                        let doubleValue = Double(self) ?? 0.0
                        return doubleValue >= minimumValue && digit.count <= maxDecimalPlaces
                    }
                }
                return digit.count <= maxDecimalPlaces
            }
            
            if let maximumValue = maximumValue {
                guard doubleValue <= maximumValue else {
                    return false
                }
            }
            
            if let minimumValue = minimumValue {
                guard doubleValue >= minimumValue else {
                    return false
                }
            }
            
            return digit.count <= maxDecimalPlaces
        }
        return false
    }
    
    func hidesMobileNumber() -> String{
        let hideCount = Int(round(Double(count)/3.3))
        guard count > 4 else { return self }
        
        let showLastTextCount = -4
        let showFirstTextCount = count+showLastTextCount-hideCount
        
        let start = index(startIndex, offsetBy: showFirstTextCount)
        let end = index(endIndex, offsetBy: showLastTextCount)
        let range = start..<end
        
        var newValue = String(self[startIndex..<endIndex])
        newValue.replaceSubrange(range, with: repeatElement("*", count: newValue.count - showFirstTextCount + showLastTextCount))
        return newValue
    }
    
    public var html2AttributedString: NSMutableAttributedString? {
        guard let data = data(using: String.Encoding.unicode) else {
            return nil
        }
        do {
            return try NSMutableAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    
    func doSHA256(key: String = "") -> String {
        var digest = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        CCHmac(CCHmacAlgorithm(kCCHmacAlgSHA256), key, key.count, self, self.count, &digest)
        let data = Data(bytes: digest)
        return data.map { String(format: "%02hhx", $0) }.joined()
    }
    
    func checkPluralWithLocalized(quantity: Any) -> String{
        var isPlural = false
        
        if let unitDouble = quantity as? Double{
            if unitDouble > 0 || unitDouble < 0{
               isPlural = true
            }
        }else if let unitInt = quantity as? Int{
            if unitInt > 0 || unitInt < 0{
                isPlural = true
            }
        }
        
        if isPlural && self.suffix(1) != "s"{
            return "\(self)s".localized()
        }
         
        return self.localized()
    }
    
    func getLocalizedCountryName() -> String {
        let locale : NSLocale = NSLocale(localeIdentifier: Localize.currentLanguage())
        if let name = locale.displayName(forKey: .countryCode, value: self) {
            // Country name was found
            return name
        } else {
            // Country name cannot be found
            return self
        }
    }
    
    func validateEmail() -> Bool {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let comparator = NSPredicate(format:"SELF MATCHES %@", emailRegex)
        return comparator.evaluate(with: self)
    }
    func separate(every stride: Int = 4, with separator: Character = " ") -> String {
           return String(enumerated().map { $0 > 0 && $0 % stride == 0 ? [separator, $1] : [$1]}.joined())
    }
    
    func removeFrontZero() -> String{
        var newText = self
        
        for _ in self{
            if newText.first == "0"{
                newText.removeFirst()
            }else{
                break
            }
        }
        
        return newText
    }
}
