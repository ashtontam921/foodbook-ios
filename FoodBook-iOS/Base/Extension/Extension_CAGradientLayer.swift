

import Foundation
import UIKit

extension CAGradientLayer {
    class func formGradientLayer(for bounds: CGRect, with firstColor: CGColor, to secondColor: CGColor) -> CAGradientLayer {
        let layer = CAGradientLayer()
        layer.frame = bounds
        layer.colors = [firstColor, secondColor]
        return layer
    }
}
