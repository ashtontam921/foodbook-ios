

import Foundation
import UIKit
import WebKit

extension UILabel {
    private func boldRange(_ range : NSRange){
        var mutableAttributedText = NSMutableAttributedString()
        if self.attributedText == nil {
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        mutableAttributedText.addAttributes([NSAttributedString.Key.font:UIFont.boldSystemFont(ofSize: self.font.pointSize)], range: range)
        self.attributedText = mutableAttributedText
    }
    
    private func boldCustomFontRange(_ range : NSRange){
        var mutableAttributedText = NSMutableAttributedString()
        if self.attributedText == nil {
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
//        mutableAttributedText.addAttributes([NSAttributedString.Key.font:UIFont(name: GothamRounded.bold.rawValue, size: self.font.pointSize)!], range: range)
//        self.attributedText = mutableAttributedText
    }
    
    private func colorRange(_ range : NSRange , color : UIColor){
        var mutableAttributedText = NSMutableAttributedString()
        if self.attributedText == nil {
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        mutableAttributedText.addAttributes([NSAttributedString.Key.foregroundColor:color], range: range)
        self.attributedText = mutableAttributedText
    }
    
    private func differentFontRange(_ range : NSRange , font : UIFont){
        var mutableAttributedText = NSMutableAttributedString()
        if self.attributedText == nil {
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        mutableAttributedText.addAttributes([NSAttributedString.Key.font:font], range: range)
        self.attributedText = mutableAttributedText
    }
    
    private func strikethroughSubstring(_ range : NSRange ,strikethroughColor : UIColor ){
        var mutableAttributedText = NSMutableAttributedString()
        if self.attributedText == nil {
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        mutableAttributedText.addAttribute(NSAttributedString.Key.strikethroughStyle, value: NSNumber(value: NSUnderlineStyle.single.rawValue), range:range)
        mutableAttributedText.addAttribute(NSAttributedString.Key.strikethroughColor, value: strikethroughColor, range: range)
        mutableAttributedText.addAttribute(NSAttributedString.Key.baselineOffset, value: 0, range: range)
        self.attributedText = mutableAttributedText
    }
    
    func underline(){
        let textRange = NSMakeRange(0, self.text!.count)
        var attributedText = NSMutableAttributedString(string: self.text!)
        
        if self.attributedText != nil{
            attributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        attributedText.addAttribute(NSAttributedString.Key.underlineStyle , value: NSUnderlineStyle.single.rawValue, range: textRange)
        self.attributedText = attributedText
    }
    
    func boldSubString (_ substring : String){
        let text = NSString(string: self.text!)
        let range = text.range(of: substring)
        self.boldRange(range)
    }
    
    func boldCustomSubString (_ substring : String){
        let text = NSString(string: self.text!)
        let range = text.range(of: substring)
        self.boldCustomFontRange(range)
    }
    
    func colorSubstring(_ substring : String , color:UIColor) {
        let text = NSString(string: self.text!)
        let range = text.range(of: substring)
        self.colorRange(range, color: color)
    }
    
    func colorSubstringIgnoreCase(_ substring : String , color:UIColor) {
        let text = NSString(string: self.text!.lowercased())
        let range = text.range(of: substring.lowercased())
        self.colorRange(range, color: color)
    }
    
    func differentFontSubstring(_ substring : String , font : UIFont){
        let text = NSString(string: self.text!)
        let range = text.range(of: substring)
        self.differentFontRange(range, font: font)
    }
    
    func strikethroughSubstring(_ substring : String, strikethroughColor : UIColor){
        let text = NSString(string: self.text!)
        let range = text.range(of: substring)
        self.strikethroughSubstring(range, strikethroughColor: strikethroughColor)
    }
    
    func setLineSpacing(lineSpacing: CGFloat){
        let paragraphStyle = NSMutableParagraphStyle()
        var mutableAttributedText = NSMutableAttributedString()
        
        if self.attributedText == nil{
            mutableAttributedText = NSMutableAttributedString(string: self.text!)
        }else{
            mutableAttributedText = NSMutableAttributedString(attributedString: self.attributedText!)
        }
        
        paragraphStyle.lineSpacing = lineSpacing
        paragraphStyle.alignment = self.textAlignment
        
        mutableAttributedText.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraphStyle, range: NSMakeRange(0, mutableAttributedText.length))
        
        self.attributedText = mutableAttributedText
    }
    
    func withLinkDetector(fitContent: Bool = false){
        self.isUserInteractionEnabled = true
        self.isEnabled = true
        self.viewWithTag(3333)?.removeFromSuperview()
        
        let textView = UITextView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        
        if fitContent{
            self.sizeToFit()
            textView.isScrollEnabled = false
        }else{
            textView.isScrollEnabled = true
        }
        
        textView.text = self.text
        textView.attributedText = self.attributedText
        textView.font = self.font
        textView.textColor = self.textColor
        textView.textAlignment = self.textAlignment
        textView.textContainer.lineFragmentPadding = 0
        textView.textContainerInset = UIEdgeInsets.zero
        textView.isEditable = false
        textView.isSelectable = true
        textView.isUserInteractionEnabled = true
        textView.dataDetectorTypes = .link
        textView.linkTextAttributes = [NSAttributedString.Key.underlineStyle : 1, NSAttributedString.Key.foregroundColor : UIColor.blue]
        textView.tag = 3333
        
        self.text = nil
        self.attributedText = nil
        self.addSubview(textView)
    }
    
    func loadhtmlString(with html: String){
        let webView = WKWebView(frame: self.frame)
        webView.loadHTMLString(html, baseURL: nil)
        
        self.addSubview(webView)
    }
    
    //MARK: IBInspectable
    @IBInspectable var kerning: Float {
        get {
            var range = NSMakeRange(0, (text ?? "").count)
            guard let kern = attributedText?.attribute(NSAttributedString.Key.kern, at: 0, effectiveRange: &range),
                let value = kern as? NSNumber
                else {
                    return 0
            }
            return value.floatValue
        }
        set {
            var attText:NSMutableAttributedString
            
            if let attributedText = attributedText {
                attText = NSMutableAttributedString(attributedString: attributedText)
            } else if let text = text {
                attText = NSMutableAttributedString(string: text)
            } else {
                attText = NSMutableAttributedString(string: "")
            }
            
            let range = NSMakeRange(0, attText.length)
            attText.addAttribute(NSAttributedString.Key.kern, value: NSNumber(value: newValue as Float), range: range)
            self.attributedText = attText
        }
    }
}
