

import UIKit

extension UINavigationController {
    open override var preferredStatusBarStyle: UIStatusBarStyle {
        return topViewController?.preferredStatusBarStyle ?? .lightContent
    }
    
    public func popToRootViewController(animated: Bool,
                                        completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popToRootViewController(animated: animated)
        CATransaction.commit()
    }
    
    public func pushViewController(viewController: UIViewController,
                                   animated: Bool,
                                   completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        pushViewController(viewController, animated: animated)
        CATransaction.commit()
    }
    
    public func popViewController(animated: Bool,
                                  completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popViewController(animated: animated)
        CATransaction.commit()
    }
    
    func popToViewController(numOfController: Int, animated: Bool){
        if let controller = self.viewControllers[safe: self.viewControllers.count - 1 - numOfController]{
            self.popToViewController(controller, animated: animated)
        }
    }
    
    func popToViewController<T>(type: T.Type, animated: Bool, completion: (() -> Void)?){
        if let controller = self.viewControllers.last(where: { Swift.type(of: $0) == type }) {
            CATransaction.begin()
            CATransaction.setCompletionBlock(completion)
            self.popToViewController(controller, animated: animated)
            CATransaction.commit()
        }
    }
    
    public func popToViewController(_ controller: UIViewController, animated: Bool,
                                        completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        popToViewController(controller, animated: animated)
        CATransaction.commit()
    }
    
    public func setViewControllers(_ viewControllers: [UIViewController], animated: Bool,
                                        completion: (() -> Void)?) {
        CATransaction.begin()
        CATransaction.setCompletionBlock(completion)
        setViewControllers(viewControllers, animated: animated)
        CATransaction.commit()
    }
    
    func clearNavigationStackOf<T>(type: T.Type){
        self.viewControllers.removeAll(where: { $0 is T })
    }
    
    func clearNavigationStackTo<T>(type: T.Type, completion: (() -> Void)?){
        if let lastIndex = self.viewControllers.lastIndex(where: { Swift.type(of: $0) == type }) {
            let controllers = self.viewControllers
            
            for (index, controller) in controllers.enumerated(){
                if index > 0  && index > lastIndex && index < controllers.count - 1{
                    self.viewControllers.remove(controller)
                }
            }
        }
    }
    
    func clearToRootNavigationStack(){
        let count = self.viewControllers.count
        
        for (index, controller) in self.viewControllers.enumerated(){
            if index > 0  && index < count - 1{
                self.viewControllers.remove(controller)
            }
        }
    }
}
