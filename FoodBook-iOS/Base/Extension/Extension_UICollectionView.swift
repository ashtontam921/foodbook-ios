

import UIKit

// MARK: Vibrate the views
enum Direction: Int {
    case horizontal = 0
    case vertical
}


extension UICollectionView {
    func formDefaultFlowLayout(width: CGFloat, height: CGFloat, direction: UICollectionView.ScrollDirection, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), inset: UIEdgeInsets) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = direction
        layout.sectionInset = inset
        layout.minimumInteritemSpacing = spacing.item
        layout.minimumLineSpacing = spacing.line
        layout.itemSize = CGSize(width: width, height: height)
        
        return layout
    }
    
    func setDefaultFlowLayout(width: CGFloat, height: CGFloat, direction: UICollectionView.ScrollDirection, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), inset: UIEdgeInsets) {
        let layout = formDefaultFlowLayout(width: width, height: height, direction: direction, spacing: spacing, inset: inset)

        self.isPagingEnabled = false
        self.collectionViewLayout = layout
    }

    func formSquareFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) -> UICollectionViewFlowLayout {
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        let itemWidth = getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets)
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth)
        
        return layout
    }
    
    func setSquareFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) {
        self.collectionViewLayout = formSquareFlowLayout(itemPerRow: itemPerRow, spacing: spacing, insets: insets)
    }
    
    func formRectangularFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), itemHeight: CGFloat, insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) -> UICollectionViewFlowLayout {
        layoutIfNeeded()
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        layout.itemSize = CGSize(width: getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets), height: itemHeight)
        
        return layout
    }
    
    func setRectangularFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), itemHeight: CGFloat?, insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) {
        self.collectionViewLayout = formRectangularFlowLayout(itemPerRow: itemPerRow, spacing: spacing, itemHeight: itemHeight ?? self.frame.size.height - (insets.top + insets.bottom), insets: insets)
    }
    
    func formRectangularFlowLayout(direction: Direction, itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), itemHeight: CGFloat, insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), overrideWidth: CGFloat? = nil) -> UICollectionViewFlowLayout {
        layoutIfNeeded()
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        layout.scrollDirection = (direction == .horizontal) ?
            .horizontal :
            .vertical
        layout.itemSize = CGSize(width: getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets, overrideWidth: overrideWidth), height: itemHeight)
        
        return layout
    }
    
    func setRectangularFlowLayout(direction: Direction, itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), itemHeight: CGFloat?, insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), overrideWidth: CGFloat? = nil) {
        self.collectionViewLayout = formRectangularFlowLayout(direction: direction, itemPerRow: itemPerRow, spacing: spacing, itemHeight: itemHeight ?? self.frame.size.height - (insets.top + insets.bottom), insets: insets, overrideWidth: overrideWidth)
    }
    
    func setPagingFlowLayout(direction: Direction, spacing: CGFloat = 0.0, edges: UIEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)) {
        layoutIfNeeded()
        isPagingEnabled = true
        let inset = (direction == .horizontal) ?
            UIEdgeInsets(top: 0, left: spacing, bottom: 0, right: spacing) :
            UIEdgeInsets(top: spacing, left: 0, bottom: spacing, right: 0)
        
        let layout = formInitialFlowLayout(insets: inset, spacing: (item: spacing * 2, line: spacing * 2))
        layout.scrollDirection = (direction == .horizontal) ?
            .horizontal :
            .vertical
        
        let itemHeight = self.frame.size.height - (inset.top + inset.bottom)
        let itemWidth = self.frame.size.width - (inset.left + inset.right)
        layout.itemSize = CGSize(width: itemWidth, height: itemHeight)
        
        self.collectionViewLayout = layout
    }
    
    // Specific for Omnity only
    func formCustomRectangularFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 0, line: 0), insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)) -> UICollectionViewFlowLayout {
        layoutIfNeeded()
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        let itemWidth = getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets)
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth*1.09)
        
        return layout
    }
    
    func setCustomSquareFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), leftOffset: CGFloat) {
        collectionViewLayout = formCustomSquareFlowLayout(itemPerRow: itemPerRow, spacing: spacing, insets: insets, leftOffset: leftOffset)
    }
    
    func formCustomSquareFlowLayout(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), leftOffset: CGFloat) -> UICollectionViewFlowLayout {
        layoutIfNeeded()
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        let itemWidth = getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets, offsetValue: leftOffset)
        layout.itemSize = CGSize(width: max(itemWidth, 0.1), height: max(itemWidth, 0.1))
        
        return layout
    }
    
    // MARK: Class Functions
    func formItemSize(for layout: UICollectionViewFlowLayout, totalItemPerRow: Int, ratio: CGFloat, height: CGFloat? = nil) -> CGSize {
        var adjustedRatio = ratio
        if (0.0...1.0) ~= ratio {
            if ratio < 0.0 {
                adjustedRatio = 0.0
            } else if ratio > 1.0 {
                adjustedRatio = 1.0
            }
        }
        
        let standardWidth = self.getItemWidth(from: layout, collectionView: self, with: CGFloat(totalItemPerRow), and: layout.sectionInset)
        let width = standardWidth * (adjustedRatio + 1.0)
        return CGSize(width: width, height: height ?? width)
    }
    
    // MARK: Private Functions
    private func formInitialFlowLayout(insets: UIEdgeInsets, spacing: (item: CGFloat, line: CGFloat)) -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = insets
        layout.minimumInteritemSpacing = spacing.item
        layout.minimumLineSpacing = spacing.line
        
        return layout
    }
    
    func getItemWidth(from layout: UICollectionViewFlowLayout, collectionView: UICollectionView, with rowCount: CGFloat, and insets: UIEdgeInsets, overrideWidth: CGFloat? = nil, offsetValue: CGFloat = 0) -> CGFloat {
        
        var constant: CGFloat = collectionView.layer.bounds.size.width - (insets.left + insets.right + offsetValue)
        if let width = overrideWidth {
            constant = width - (insets.left + insets.right + offsetValue)
        }
        let itemGap: CGFloat = layout.minimumInteritemSpacing * (rowCount - 1.0)
        return (constant - itemGap) / rowCount - 0.00001
    }
    
    func getItemSize(itemPerRow: Int, spacing: (item: CGFloat, line: CGFloat) = (item: 5, line: 5), itemHeight: CGFloat, insets: UIEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5), overrideWidth: CGFloat? = nil, layoutIfNeeded: Bool = true) -> CGSize {
        if layoutIfNeeded {
            self.layoutIfNeeded()
        }
        
        let layout = formInitialFlowLayout(insets: insets, spacing: spacing)
        return CGSize(width: getItemWidth(from: layout, collectionView: self, with: CGFloat(itemPerRow), and: insets, overrideWidth: overrideWidth), height: itemHeight)
    }
}
