

import UIKit

extension UIViewController{
    class var defaultIdentifier: String {
        return String(describing: self)
    }
    
    open override func awakeFromNib() {
        if #available(iOS 13.0, *) {
            if self.modalPresentationStyle == .pageSheet || self.modalPresentationStyle == .automatic{
                self.modalPresentationStyle = .fullScreen
            }
        } else {
            // Fallback on earlier versions
        }
    }
    
    func screenSize() -> CGSize{
        return UIScreen.main.bounds.size
    }
    
    func topBarHeight() -> CGFloat{
        if let navBar = self.navigationController?.navigationBar{
            return UIApplication.shared.statusBarFrame.size.height + navBar.bounds.size.height
        }
        
        return UIApplication.shared.statusBarFrame.size.height + 44
    }
    
    func vc(_ storyboard: String? = nil, identifier: String? = nil) -> UIViewController? {
        var _storyboard = self.storyboard
        
        if storyboard != nil {
            _storyboard = UIStoryboard(name: storyboard!, bundle: nil)
        }
        
        if _storyboard != nil{
            if identifier != nil {
                return _storyboard!.instantiateViewController(withIdentifier: identifier!)
            } else {
                return _storyboard!.instantiateInitialViewController()!
            }
        }
        
        return nil
    }
    
    func pushControllerWith(storyBoardID: String?, identifier: String?, completion: ((inout UIViewController?)->Void)?){
        var controller: UIViewController?
        
        if storyBoardID == nil{
            if identifier == nil{
                controller = self.storyboard?.instantiateInitialViewController()
            }else{
                controller = self.storyboard?.instantiateViewController(withIdentifier: identifier!)
            }
        }else{
            controller = vc(storyBoardID!, identifier: identifier!)
        }
        
        completion?(&controller)
        
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    func pushControllerWith<T: UIViewController>(storyBoardID: String?, type: T.Type, isHideBtmBar: Bool = true, completion: ((inout T?)->Void)?){
        var controller: T?
        
        if storyBoardID == nil{
            controller = self.storyboard?.instantiateViewController(withIdentifier: T.defaultIdentifier) as? T
        }else{
            controller = vc(storyBoardID!, identifier: T.defaultIdentifier) as? T
        }
        
        controller?.hidesBottomBarWhenPushed = isHideBtmBar
        completion?(&controller)
        
        if controller != nil{
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    
    func presentControllerWith(storyBoardID: String?, identifier: String?, animated: Bool, completion: ((inout UIViewController?)->Void)?){
        var controller: UIViewController?
        
        if storyBoardID == nil{
            if identifier == nil{
                controller = self.storyboard?.instantiateInitialViewController()
            }else{
                controller = self.storyboard?.instantiateViewController(withIdentifier: identifier!)
            }
        }else{
            controller = vc(storyBoardID!, identifier: identifier!)
        }
        
        controller?.modalPresentationStyle = .overCurrentContext
        completion?(&controller)
        
        if controller != nil{
            self.present(controller!, animated: animated, completion: nil)
        }
    }
    
    func addFakeNavigationView() -> UIView{
        let viewHeight: CGFloat = topBarHeight()
        let navView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize().width, height: viewHeight))
        
        navView.backgroundColor = .clear
        
        self.view.addSubview(navView)
        
        return navView
    }
}
