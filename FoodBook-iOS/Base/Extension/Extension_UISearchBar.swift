

import Foundation
import UIKit

extension UISearchBar{
    var textField : UITextField{
        if #available(iOS 13.0, *) {
            return self.searchTextField
        } else {
            if let tf = self.value(forKey: "_searchField") as? UITextField{
                return tf
            }else if let tf = self.value(forKey: "searchField") as? UITextField{
                return tf
            }else{
                for subview in self.subviews{
                    if subview is UITextField{
                        return subview as! UITextField
                    }
                }
            }
            
            return UITextField()
        }
    }
    
    func setBackgroundCornerRadius(radius: CGFloat){
        if let backgroundview = self.textField.subviews.first {
            DispatchQueue.main.async {
                backgroundview.layer.cornerRadius = radius;
                backgroundview.clipsToBounds = true;
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                backgroundview.layer.cornerRadius = radius;
                backgroundview.clipsToBounds = true;
            }
        }
    }

    func setCancelTitle(_ text: String){
        for subview in subviews {
            for subview1 in subview.subviews {
                for subview2 in subview1.subviews {
                    if subview2 is UIButton {
                        let cancelButton =  subview2 as! UIButton
                        cancelButton.setTitle(text, for: .normal)
                    }
                }

            }
        }
    }
    
    func setupSearchBarContent(placeholder: String = "search".localized(), tintColor: UIColor = .darkText, textColor: UIColor = .darkText, backgroundColor: UIColor = .white, delegate: UISearchBarDelegate?){
        
        self.sizeToFit()
        self.tintColor = tintColor
        self.textField.placeholder = placeholder
        self.textField.textColor = textColor
        self.textField.backgroundColor = backgroundColor
        self.delegate = delegate
        
        let height: CGFloat = 44
        
        if #available(iOS 13.0, *) {
        }else{
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
        if let iconView = self.textField.leftView as? UIImageView {
            iconView.image = iconView.image?.withRenderingMode(UIImage.RenderingMode.alwaysTemplate)
            iconView.tintColor = tintColor
        }
    }
}
