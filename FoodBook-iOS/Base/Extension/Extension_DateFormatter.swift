

import Foundation
extension DateFormatter {
    
    static let defaultDateFormatter: DateFormatter = {
        let calender = Calendar.init(identifier: .gregorian)
        let formatter = DateFormatter()
        formatter.calendar = calender
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        formatter.timeZone = TimeZone.init(identifier: "UTC")
        return formatter
    }()
    
    static let currentDateFormatter: DateFormatter = {
        let calender = Calendar.init(identifier: .gregorian)
        let formatter = DateFormatter()
        formatter.calendar = calender
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return formatter
    }()
}
