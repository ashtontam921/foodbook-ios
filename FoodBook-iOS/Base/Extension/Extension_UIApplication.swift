

import UIKit
import ObjectMapper
import MapKit

enum NavigationApp {
    case appleMap, googleMaps, waze
}

extension UIApplication{
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        if let tabController = controller as? UITabBarController {
            if let selected = tabController.selectedViewController {
                return topViewController(controller: selected)
            }
        }
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
    
    class func shareAction(withData: (url: URL?, content: String?, image: UIImage?), textPriority: Bool = true, on viewController: UIViewController) {
        var contentArray: [ActivityItemSource] = []
        let _textPriority = (withData.content != nil) ? textPriority : false
        
        if let url = withData.url {
            contentArray.append(ActivityItemSource(with: url, textPriority: _textPriority))
        }
        if let content = withData.content {
            contentArray.append(ActivityItemSource(with: content, textPriority: _textPriority))
        }
        if let image = withData.image {
            contentArray.append(ActivityItemSource(with: image, textPriority: _textPriority))
        }
        
        let activity = UIActivityViewController(activityItems: contentArray, applicationActivities: nil)
        
        viewController.present(activity, animated: true, completion: nil)
    }
    
    class func openMap(with coordinates: (lat: Double, lng: Double), appIdentifier: NavigationApp = .appleMap) {
        switch appIdentifier {
        case .googleMaps:
            if let url = URL(string: "comgooglemaps://?center=\(coordinates.lat),\(coordinates.lng)&zoom=14&q=\(coordinates.lat),\(coordinates.lng)"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                let gooleMapAppStore = "https://itunes.apple.com/app/id585027354"
                if let url = URL(string: gooleMapAppStore), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        case .waze:
            if let url = URL(string: "waze://?ll=\(coordinates.lat),\(coordinates.lng)&navigate=yes"), UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                let wazeAppStore = "https://itunes.apple.com/app/id323229106"
                if let url = URL(string: wazeAppStore), UIApplication.shared.canOpenURL(url) {
                    if #available(iOS 10, *) {
                        UIApplication.shared.open(url, options: [:], completionHandler: nil)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        case .appleMap:
            let latitude: CLLocationDegrees = CLLocationDegrees(coordinates.lat)
            let longitude: CLLocationDegrees = CLLocationDegrees(coordinates.lng)
            
            let regionDistance: CLLocationDistance = 10000
            let coordinates = CLLocationCoordinate2DMake(latitude, longitude)
            let regionSpan = MKCoordinateRegion(center: coordinates, latitudinalMeters: regionDistance, longitudinalMeters: regionDistance)
            let options = [
                MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
                MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
            ]
            let placemark = MKPlacemark(coordinate: coordinates, addressDictionary: nil)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.openInMaps(launchOptions: options)
        }
    }
}

public let stringToIntTransform = TransformOf<Int, Any>(fromJSON: { (value: Any?) -> Int? in
    if let value = value as? Int {
        return value
    }
    if let value = value as? String {
        return Int(value)
    }
    return nil
}, toJSON: { (value: Int?) -> Any? in
    // transform value from Int? to String?
    if let value = value {
        return String(value)
    }
    return nil
})

public let stringToDoubleTransform = TransformOf<Double, Any>(fromJSON: { (value: Any?) -> Double? in
    if let value = value as? Double {
        return value
    }
    if let value = value as? String {
        return Double(value)
    }
    if let value = value as? Int {
        return Double(value)
    }
    return nil
}, toJSON: { (value: Double?) -> Any? in
    // transform value from Double? to String?
    if let value = value {
        return String(value)
    }
    return nil
})


let intToStringTransform = TransformOf<String, Any>(fromJSON: { (value: Any?) -> String in
    if let value = value as? String {
        return value
    }
    
    if let value = value as? Int {
        return String(value)
    }
    return ""
}, toJSON: { (value: String?) -> Any? in
    // transform value from String? to Int?
    if let value = value {
        return Int(value)
    }
    return nil
})


// MARK: - UIActivityItemSource
fileprivate class ActivityItemSource: UIActivityItemProvider {
    var content: Any
    var textPriority: Bool = false
    
    init(with content: Any, textPriority: Bool) {
        self.content = content
        self.textPriority = textPriority
        super.init(placeholderItem: content)
    }
    
    override func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return content
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        guard let type = activityType else {
            return content
        }

        if textPriority, type.rawValue == "com.facebook.Messenger.ShareExtension", content is UIImage {
            // NOTE: Prevent sharing image on facebook messenger as it overrides the text. Only triggers when textPriority flag is 'true'
            return nil
        }
        return content
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivity.ActivityType?, suggestedSize size: CGSize) -> UIImage? {
        if let image = content as? UIImage {
            return image
        }
        return nil
    }
}
