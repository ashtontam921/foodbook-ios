

import UIKit
import WebKit

extension WKWebView{
    static let progressbar = UIProgressView(progressViewStyle: .bar)
    
    func withProgressView(){
        self.layoutIfNeeded()
        
        WKWebView.progressbar.sizeToFit()
        WKWebView.progressbar.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: 3)
        addSubview(WKWebView.progressbar)
        
        self.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
    }
    
    override open func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress"{
            if WKWebView.progressbar.isHidden == true && self.estimatedProgress < 1{
                WKWebView.progressbar.progress = Float(self.estimatedProgress)
                WKWebView.progressbar.isHidden = false
            }else{
                WKWebView.progressbar.setProgress(Float(self.estimatedProgress), animated: true)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
                    if self.estimatedProgress >= 1 {
                        WKWebView.progressbar.isHidden = true
                    }
                }
            }
        }
    }
}
