

import Foundation

extension Double{
    
    static func castToDouble(from data: Any?) -> Double {
        if let unwrappedData = data as? String, let castedData = Double(unwrappedData) {
            return castedData
        } else if let unwrappedData = data as? Int {
            return Double(unwrappedData)
        } else if let castedData = data as? Double {
            return castedData
        }
        return -1
    }
    
    /// Rounds the double to decimal places value
    private func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
    
    private func priceString(fractionDigits: Int) -> String {
        return string(usesGroupingSeparator: true, minimumFractionDigits: fractionDigits, maximumFractionDigits: fractionDigits)
    }
    
    private func string(usesGroupingSeparator: Bool = false, minimumFractionDigits: Int = 0, maximumFractionDigits: Int = 0) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = NumberFormatter.Style.decimal
        formatter.usesGroupingSeparator = usesGroupingSeparator
        formatter.minimumFractionDigits = minimumFractionDigits
        formatter.maximumFractionDigits = maximumFractionDigits
        formatter.locale = Locale.init(identifier: "en")
        return formatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
    
    func cyproCurrencyFormat_Market(currency: String? = nil) -> String{
        let digits = 8
        var formattedString = self.rounded(toPlaces: digits).string(maximumFractionDigits: digits)
        if let currency = currency{
            formattedString = formattedString + currency
        }
        
        return formattedString
    }
    
    func cyproCurrencyFormat(currency: String? = nil) -> String{
        let digits = 6
        var formattedString = self.rounded(toPlaces: digits).string(maximumFractionDigits: digits)
        if let currency = currency{
            formattedString = formattedString + " " + currency
        }
        
        return formattedString
    }
    
    func pricingFormat(digits: Int = 2, currency: String? = nil, isLeadingFormat: Bool = true) -> String{
        var formattedString = self.rounded(toPlaces: digits).priceString(fractionDigits: digits)
        if let currency = currency{
            if isLeadingFormat{
                formattedString =  currency + " " + formattedString
            }else{
                formattedString =  formattedString + " " + currency
            }
        }
        
        return formattedString
    }
    
    func pointFormat(currency: String? = nil) -> String{
        let digits = 0
        var formattedString = self.rounded(toPlaces: digits).string(maximumFractionDigits: digits)
        if let currency = currency{
            formattedString =  formattedString + " " + currency
        }
        
        return formattedString
    }
    
    func unitFormat() -> String{
        return "\(self.pricingFormat(digits: 0)) \("unit".checkPluralWithLocalized(quantity: self))"
    }
    
    func voucherFormat() -> String{
        return string(usesGroupingSeparator: true, minimumFractionDigits: 0, maximumFractionDigits: 2)
    }
    
    func formatting(isPricing: Bool, currency: String? = nil) -> String{
        if isPricing{
            return self.pricingFormat(currency: currency)
        }else{
            return self.cyproCurrencyFormat(currency: currency)
        }
    }
    
    func truncate(with dicimal: Int = 0) -> Double{
        let string = String(format: "%.\(dicimal)f", self)
        
        return Double(string) ?? 0.0
    }
    
    func secondsToMinutesSeconds() -> String {
        let minute = (Int(self) % 3600) / 60
        let second = (Int(self) % 3600) % 60
        var minuteText = "\(minute)"
        var secondText = "\(second)"
        
        if minute < 10{
            minuteText = "0\(minute)"
        }
        
        if second < 10{
            secondText = "0\(second)"
        }
        
        return "\(minuteText):\(secondText)"
    }
}
