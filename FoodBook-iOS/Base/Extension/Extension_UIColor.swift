

import UIKit

extension UIColor{
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
    static var highlightTextColor: UIColor{
        return UIColor(named: "HighlightTextColor")!
    }
    
    static var lineColor: UIColor{
        return UIColor(named: "LineColor")!
    }
    
    static var textColor: UIColor{
        return UIColor(named: "TextColor")!
    }
    
    static var navigationColor: UIColor{
        return UIColor(named: "NavigationColor")!
    }
    
    static var TextPlaceholderColor: UIColor{
        return UIColor(named: "TextPlaceholderColor")!
    }
    
    static var barTintColor: UIColor{
        return UIColor(named: "BarTintColor")!
    }
    
    static var greyE5Color: UIColor{
        return UIColor(named: "GreyE5Color")!
    }
    
    static var grey66Color: UIColor{
        return UIColor(named: "Grey66Color")!
    }
    
    static var boarderLineColor: UIColor{
        return UIColor(named: "BoarderLineColor")!
    }
    
    static var greyA8Color: UIColor{
        return UIColor(named: "GreyA8Color")!
    }
    
    static var searchBarBackgroundColor: UIColor{
        return UIColor(named: "SearchBarBackgroundColor")!
    }
    
    static var backgroundEE: UIColor{
        return UIColor(named: "BackgroundEE")!
    }
    
    static var buttonColor: UIColor{
        return UIColor(named: "ButtonColor")!
    }
    
    static var gainColor: UIColor{
        return UIColor(named: "GainColor")!
    }
    
    static var loseColor: UIColor{
        return UIColor(named: "LoseColor")!
    }
    
    static var pendingColor: UIColor{
        return UIColor(named: "PendingColor")!
    }
    
    static var mainColor: UIColor{
        return UIColor(named: "MainColor")!
    }
    static var unreadRedColor: UIColor{
        return UIColor(named: "UnreadRedColor")!
    }
    
    static var F8Color: UIColor{
        return UIColor(named: "F8Color")!
    }
    
    static var selectedCellColor: UIColor{
        return UIColor(named: "SelectedCellColor")!
    }
    
    static var LineC0Color: UIColor{
        return UIColor(named: "LineC0Color")!
    }
    
    static var refundBlueColor: UIColor{
        return UIColor(named: "RefundBlueColor")!
    }
    
    static var ptWalletColor: UIColor{
        return UIColor(named: "PTWalletColor")!
    }
    
    
    static var backgroundLight: UIColor{
        return UIColor(named: "BackgroundLight")!
    }
    static var SettingBackgroudColor: UIColor{
        return UIColor(named: "SettingBackgroudColor")!
    }
    
}

