

import UIKit

extension UIBarButtonItem {
    static func customBarButton(_ target: Any?, action: Selector, imageName: String, width: CGFloat = 24, height: CGFloat = 24) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: imageName), for: .normal)
        button.addTarget(target, action: action, for: .touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: width).isActive = true
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: height).isActive = true

        return menuBarItem
    }
}
