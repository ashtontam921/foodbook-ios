

import MapKit

extension MKCoordinateRegion {
    func formMapRect() -> MKMapRect {
        let deltaLatitude = span.latitudeDelta / 2.0
        let deltaLongitude = span.longitudeDelta / 2.0
        
        let topLeft = MKMapPoint(
            CLLocationCoordinate2DMake(center.latitude + deltaLatitude, center.longitude - deltaLongitude))
        let botRight = MKMapPoint(
            CLLocationCoordinate2DMake(center.latitude - deltaLatitude, center.longitude + deltaLongitude))
        
        return MKMapRect(
            x: topLeft.x,
            y: topLeft.y,
            width: fabs(botRight.x - topLeft.x),
            height: fabs(botRight.y - topLeft.y))
    }
    
    func formUnionWith(region: MKCoordinateRegion) -> MKCoordinateRegion {
        let union = formMapRect().union(region.formMapRect())
        return MKCoordinateRegion(union)
    }
}
