

import UIKit

extension UIView{
    @IBInspectable
    public var shadow :Bool {
        set {
            DispatchQueue.main.async {
                self.addShadow()
                self.setNeedsLayout()
            }
        }
        get {
            return self.shadow
        }
    }
    
    @IBInspectable
    public var shadowCornerRadius :CGFloat {
        set {
            DispatchQueue.main.async {
                self.clipsToBounds = true
                self.layer.masksToBounds = false
                self.layer.cornerRadius = newValue
                self.setNeedsLayout()
            }
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var isRoundCorner :Bool{
        set {
            self.setNeedsLayout()
            layer.masksToBounds = true
            layer.cornerRadius = newValue ? bounds.height/2 : 0
            self.setNeedsLayout()
        }
        get {
            return layer.cornerRadius > 0
        }
    }
    
    @IBInspectable
    public var cornerRadius :CGFloat {
        set {
            layer.masksToBounds = true
            layer.cornerRadius = newValue
            self.setNeedsLayout()
        }
        get {
            return layer.cornerRadius
        }
    }
    
    @IBInspectable
    public var borderColor :UIColor {
        set {
            layer.borderColor = newValue.cgColor
            self.setNeedsLayout()
        }
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
    }
    
    @IBInspectable
    public var borderWidth :CGFloat {
        set {
            layer.borderWidth = newValue
            self.setNeedsLayout()
        }
        get {
            return layer.borderWidth
        }
    }
    
    var parentViewController: UIViewController? {
        var parentResponder: UIResponder? = self
        while parentResponder != nil {
            parentResponder = parentResponder!.next
            if let viewController = parentResponder as? UIViewController {
                return viewController
            }
        }
        return nil
    }
    
    enum Position {
        case all, top, left, bottom, right
    }
    
    func defaultBorder(with cornerRadius: CGFloat = 4.0, width: CGFloat = 0.4, color: UIColor = UIColor.lightGray) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
    }
    
    func drawLine(at position: Position, width: CGFloat, color: UIColor, dashedPattern: [NSNumber]? = nil, beginningMargin: CGFloat = 0.0, endMargin: CGFloat = 0.0) {
        layoutIfNeeded()
        
        let path = UIBezierPath()
        if position == .top || position == .all {
            path.move(to: CGPoint(x: 0 + beginningMargin, y: 0))
            path.addLine(to: CGPoint(x: frame.size.width - endMargin, y: 0))
        }
        if position == .left || position == .all {
            path.move(to: CGPoint(x: 0, y: 0 + beginningMargin))
            path.addLine(to: CGPoint(x: 0, y: frame.size.height - endMargin))
        }
        if position == .bottom || position == .all {
            path.move(to: CGPoint(x: 0 + beginningMargin, y: frame.size.height))
            path.addLine(to: CGPoint(x: frame.size.width - endMargin, y: frame.size.height))
        }
        if position == .right || position == .all {
            path.move(to: CGPoint(x: frame.size.width, y: 0 + beginningMargin))
            path.addLine(to: CGPoint(x:frame.size.width, y: frame.size.height - endMargin))
        }
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.path = path.cgPath
        shapeLayer.strokeColor = color.cgColor
        if let dashedPattern = dashedPattern {
            shapeLayer.lineDashPattern = dashedPattern
        }
        shapeLayer.lineWidth = width
        
        layer.addSublayer(shapeLayer)
    }
    
    func setHeight(height:CGFloat){
        var frame = self.frame
        frame.size.height = height
        self.frame = frame
    }
    
    func setWidth(width:CGFloat){
        var frame = self.frame
        frame.size.width = width
        self.frame = frame
    }
    
    func addTapGesture(target:Any, selector: Selector, cancelsTouchesInView: Bool = false){
        let tapGesture = UITapGestureRecognizer(target: target, action: selector)
        
        tapGesture.cancelsTouchesInView = cancelsTouchesInView
        tapGesture.delegate = target as? UIGestureRecognizerDelegate
//        self.isUserInteractionEnabled = true
        self.addGestureRecognizer(tapGesture)
    }
    
    func createInvisibleBtn(extraPadding: CGFloat = 0, target:Any, selector: Selector) -> UIButton{
        let btn = UIButton(frame: CGRect(x: self.frame.origin.x - extraPadding, y: self.frame.origin.y - extraPadding, width: self.frame.size.width + 2 * extraPadding, height: self.frame.size.height + 2 * extraPadding))
        
        btn.setTitle(nil, for: .normal)
        btn.backgroundColor = .clear
        btn.addTarget(target, action: selector, for: .touchUpInside)
        
        return btn
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let maskPath = UIBezierPath(
            roundedRect: bounds,
            byRoundingCorners: corners,
            cornerRadii: CGSize(width: radius, height: radius))

        let shape = CAShapeLayer()
        shape.path = maskPath.cgPath
        layer.mask = shape
    }
    
    func rounded(){
        DispatchQueue.main.async {
            self.layer.masksToBounds = true
            self.layer.cornerRadius = self.frame.size.height / 2
        }
    }
    
    func addShadow(radius: CGFloat = 3, offset: CGSize = CGSize(width: 0, height: 0), opacity: Float = 0.15, shadowColor: UIColor = .black) {
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = opacity
        layer.shadowOffset = offset
        layer.shadowRadius = radius
    }
    
    func addShadowWithCornerRadius(cornerRadius: CGFloat, radius: CGFloat = 3, offset: CGSize = CGSize(width: 0, height: 0), opacity: Float = 0.15) {
        DispatchQueue.main.async {
            self.layer.cornerRadius = cornerRadius
            self.addShadow(radius: radius, offset: offset, opacity: opacity)
        }
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    
    func applyGradient(with colours: [UIColor], gradient orientation: GradientOrientation) {
        let gradient = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func removeGradient(){
        for layer in self.layer.sublayers ?? []{
            if layer is CAGradientLayer{
                self.layer.sublayers?.removeFirst()
            }
        }
    }
    
//    func mainGradientBtnStyle(){
//        applyGradient(with: [.gradientColor1_1, .gradientColor1_2], gradient: .horizontal)
//    }
//    
//    func welcomeContentStyle(){
//        applyGradient(with: [.gradientColor2_1, .gradientColor2_2], gradient: .vertical)
//    }
    
    func dashLineBorder(){
        for layer in layer.sublayers ?? []{
            if let shaperLayer = layer as? CAShapeLayer, shaperLayer.lineDashPattern != nil{
                shaperLayer.removeFromSuperlayer()
            }
        }
        
        let dashLineBorder = CAShapeLayer()
        dashLineBorder.strokeColor = UIColor.highlightTextColor.cgColor
        dashLineBorder.lineDashPattern = [2, 2]
        dashLineBorder.frame = bounds
        dashLineBorder.fillColor = nil
        dashLineBorder.path = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
        self.layer.addSublayer(dashLineBorder)
    }
    
    func radarAnination(){
        let size = frame.size
        let duration: CFTimeInterval = 1.5
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0.0, 0.2, 0.4, 0.8, 1.0]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.12, 0.25, 0.5, 0.6)
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.7]
        scaleAnimation.timingFunction = timingFunction
        scaleAnimation.values = [0, 1]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.keyTimes = [1, 0]
        opacityAnimation.timingFunctions = [timingFunction, timingFunction]
        opacityAnimation.values = [0, 1]
        opacityAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 5 {
            let circle = layerWith(size: size, color: UIColor.highlightTextColor.cgColor)
            let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                               y: (layer.bounds.size.height - size.height) / 2,
                               width: size.width,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
        
        
        
        let fadeAnimation = CAKeyframeAnimation(keyPath:"opacity")
        fadeAnimation.beginTime = CACurrentMediaTime()
        fadeAnimation.duration = 2
        fadeAnimation.keyTimes = [0, 0.125, 0.625, 1]
        fadeAnimation.values = [0.0, 0.0, 1.0, 1.0]
        fadeAnimation.isRemovedOnCompletion = false
        fadeAnimation.fillMode = CAMediaTimingFillMode.forwards
        layer.add(fadeAnimation, forKey:"animateOpacity")
        layer.opacity = 1.0
    }
    
    private func layerWith(size: CGSize, color: CGColor) -> CALayer {
        let layer: CAShapeLayer = CAShapeLayer()
        let path: UIBezierPath = UIBezierPath()
        let lineWidth: CGFloat = 5
        
        path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                    radius: size.width / 2,
                    startAngle: 0,
                    endAngle: CGFloat(2 * Double.pi),
                    clockwise: false)
        layer.fillColor = color
        layer.strokeColor = color
        layer.lineWidth = lineWidth
        
        layer.backgroundColor = nil
        layer.path = path.cgPath
        layer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        return layer
    }
}
typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical

    var startPoint : CGPoint {
        return points.startPoint
    }

    var endPoint : CGPoint {
        return points.endPoint
    }

    var points : GradientPoints {
        switch self {
        case .topRightBottomLeft:
            return (CGPoint(x: 0.0,y: 1.0), CGPoint(x: 1.0,y: 0.0))
        case .topLeftBottomRight:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 1,y: 1))
        case .horizontal:
            return (CGPoint(x: 0.0,y: 0.5), CGPoint(x: 1.0,y: 0.5))
        case .vertical:
            return (CGPoint(x: 0.0,y: 0.0), CGPoint(x: 0.0,y: 1.0))
        }
    }
}
