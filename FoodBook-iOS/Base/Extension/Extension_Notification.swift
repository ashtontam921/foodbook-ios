
import UIKit

extension Notification.Name {
    static let RefreshRecipeList = Notification.Name("RefreshRecipeList")
}
