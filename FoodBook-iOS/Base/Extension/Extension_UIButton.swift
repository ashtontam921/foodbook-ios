

import UIKit

extension UIButton {
    
    func setEnable(state: Bool) {
        self.isEnabled = state
        self.alpha = state ? 1.0 : 0.5
    }
}
