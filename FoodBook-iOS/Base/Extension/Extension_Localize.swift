

import Foundation
import Localize_Swift

extension Localize{
    
    static var currentLanguage_Server: String{
        var lang = Localize.currentLanguage()
        switch Localize.currentLanguage(){
        case "zh-Hans":
            lang = "zh"
        default:
            break
        }
        
        return lang
    }
    
    static func languageToServer(language: String) -> String{
        var lang = language
        switch language{
        case "zh-Hans":
            lang = "zh"
        default:
            break
        }
        
        return lang
    }
    
    static func langugeFromServer(language: String) -> String{
        var lang = language
        switch language{
        case "zh":
            lang = "zh-Hans"
        default:
            break
        }
        
        return lang
    }
}
