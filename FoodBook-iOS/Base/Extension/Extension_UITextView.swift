
import UIKit

extension UITextView{
    func withLinkDetector(){
        self.textContainer.lineFragmentPadding = 0
        self.textContainerInset = UIEdgeInsets.zero
        self.isEditable = false
        self.isSelectable = true
        self.isUserInteractionEnabled = true
        self.dataDetectorTypes = .link
        self.linkTextAttributes = [NSAttributedString.Key.underlineStyle : 1, NSAttributedString.Key.foregroundColor : UIColor.blue]
    }
    
    func removeAllPadding(){
        let padding = self.textContainer.lineFragmentPadding
        
        self.textContainerInset =  UIEdgeInsets(top: 0, left: -padding, bottom: 0, right: -padding)
    }
}
