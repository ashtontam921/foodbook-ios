

import UIKit

extension Int{
    func pricingFormat(digits: Int = 2, currency: String? = nil, isLeadingFormat: Bool = true) -> String{
        return Double(self).pricingFormat(digits: digits, currency: currency, isLeadingFormat: isLeadingFormat)
    }
}
