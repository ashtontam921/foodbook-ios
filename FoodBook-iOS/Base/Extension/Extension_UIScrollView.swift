

import UIKit
import UIScrollView_InfiniteScroll

extension UIScrollView {

    // Scroll to a specific view so that it's top is at the top our scrollview
    func scrollToView(view:UIView, animated: Bool) {
        if let childStartPoint = view.superview?.convert(view.frame.origin, to: nil){
            if childStartPoint.x + view.frame.size.width > frame.size.width{
                setContentOffset(CGPoint(x: view.frame.origin.x + view.frame.size.width - frame.size.width + 20, y: 0), animated: true)
            }else if childStartPoint.x < 0{
                setContentOffset(view.frame.origin, animated: true)
            }
            print(childStartPoint)
        }
    }

    func withPagination(viewModel: BaseViewModel, completion: (() -> Void)?){
        self.infiniteScrollIndicatorStyle = .white
        self.addInfiniteScroll { (tableView) -> Void in
            if !viewModel.pagination.isEndOfList && !viewModel.pagination.isFetching {
                DispatchQueue.main.async {
                    completion?()
                }
            }
        }
        
        self.setShouldShowInfiniteScrollHandler { _ -> Bool in
            return !viewModel.pagination.isEndOfList
        }
    }
    
    func endInfiniteAndReloadData(){
        if self.isAnimatingInfiniteScroll{
            self.finishInfiniteScroll()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                if let collectionView = self as? UICollectionView{
                    collectionView.reloadData()
                }else if let tableView = self as? UITableView{
                    tableView.reloadData()
                }
            }
        }else{
            if let collectionView = self as? UICollectionView{
                collectionView.reloadData()
            }else if let tableView = self as? UITableView{
                tableView.reloadData()
            }
        }
    }
    
    func addRefreshControl(target: UIViewController, selector: Selector){
        let control = UIRefreshControl()
        control.tintColor = .textColor

        if #available(iOS 10.0, *) {
            self.refreshControl = control
        } else {
            self.addSubview(control)
        }
        
        control.addTarget(target, action: selector, for: .valueChanged)
    }
}
