

import Foundation

extension Date {
    func formattedToString(_ dateFormat: String? = nil) -> String{
        let dateFormatter = DateFormatter.currentDateFormatter
        dateFormatter.locale =  Locale(identifier: "en_US_POSIX")
        if let dateFormat = dateFormat{
            dateFormatter.dateFormat = dateFormat
        }
        return dateFormatter.string(from: self)
    }
    
    func formatToCommunityDateFormat() -> String{
        return formattedToString("MMM d, yyyy") + " \("at".localized()) " + formattedToString("h:mm a")
    }
    
    func formatToServerDateFormat() -> String{
        return formattedToString("yyyy-MM-dd hh:mm:ss")
    }
    
    static func parseDate(from timestamp: Any?) -> Date? {
        var data = Double.castToDouble(from: timestamp)
        guard data != 0 else {
            return nil
        }
        if data >= 10000000000 {
            data = data/1000
        }
        if data != -1 {
            let date = Date(timeIntervalSince1970: data)
            return date
        }
        return nil
    }
    
    public func days(from date: Date? = nil) -> Int? {
        if let date = date {
            return Calendar.current.dateComponents([.day], from: date, to: self).day
        }
        return Calendar.current.dateComponents([.day], from: self).day
    }
    
    func formDateString() -> String {
        let currentDate = Date()
        if let days = currentDate.days(from: self), days < 1, Calendar.current.isDate(self, inSameDayAs: currentDate) {
            return self.formattedToString("h:mm a")
        } else {
            return self.formattedToString("yyyy-MM-dd h:mm a")
        }
    }
    
    func getStartOfDate() -> Date {
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: self)
        
        return startOfDay
    }
    
    func getEndOfDate() -> Date {
        let calendar = Calendar.current
        let startOfDay = calendar.startOfDay(for: self)
        let endOfDay = calendar.date(byAdding: .day, value: 1, to: startOfDay)

        return endOfDay ?? Date()
    }
    
    public func setDateComponent(year: Int? = nil, month: Int? = nil, day: Int? = nil, hour: Int? = nil, min: Int? = nil, sec: Int? = nil) -> Date? {
        let x: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]
        let cal = Calendar.current
        var components = cal.dateComponents(x, from: self)
        
        components.year = year ?? components.year
        components.month = month ?? components.month
        components.day = day ?? components.day
        components.hour = hour ?? components.hour
        components.minute = min ?? components.minute
        components.second = sec ?? components.second
        
        return cal.date(from: components)
    }
    
    func dateString(with formatter: DateFormatter) -> String {
        return formatter.string(from: self)
    }
}
