

import Foundation
import Kingfisher
import UIKit
import LetterAvatarKit

extension UIImageView{
    func setImageURL(with url: URL?, placeholder: Placeholder? = nil, letterAvatarName: String? = nil, bgColor: UIColor = .mainColor, letterColor: UIColor = .white, withCache: Bool = true, completionHandler: ((UIImage?) -> Void)? = nil){
        
        var placeholderImg = placeholder
        if let name = letterAvatarName{
            placeholderImg = getLetterAvatar(with: name, bgColor: bgColor, letterColor: letterColor)
        }
        
        let cacheOption: KingfisherOptionsInfo = [
            .processor(DownsamplingImageProcessor(size: self.frame.size)),
            .scaleFactor(UIScreen.main.scale),
            .backgroundDecode
        ]
        
        let refreshOption: KingfisherOptionsInfo = [
            .processor(DownsamplingImageProcessor(size: self.frame.size)),
            .scaleFactor(UIScreen.main.scale),
            .backgroundDecode,
            .forceRefresh
        ]
        
        self.kf.indicatorType = .activity
        self.kf.setImage(with: url, placeholder: placeholderImg, options: withCache ? cacheOption : refreshOption, completionHandler: { result in
            switch result {
            case .success(let value):
                completionHandler?(value.image)
            case .failure(let error):
                print("Error: \(error)")
                completionHandler?(nil)
            }
        })
    }
    
    func setLetterAvatar(with name: String, bgColor: UIColor = .mainColor, letterColor: UIColor = .white){
        self.image = getLetterAvatar(with: name, bgColor: bgColor, letterColor: letterColor)
    }
    
    func getLetterAvatar(with name: String, bgColor: UIColor = .mainColor, letterColor: UIColor = .white) -> UIImage?{
        let showLetters = name.split(separator: " ")
        var showLetter  = ""
        
        if let letter = showLetters[safe: 0], let firstLetter = letter.first{
            showLetter = String(firstLetter)
        }
        if let letter = showLetters[safe: 1], let firstLetter = letter.first{
            showLetter =  showLetter + String(firstLetter)
        }
        
        showLetter = showLetter.uppercased()
        
        let config = LetterAvatarBuilderConfiguration()
        config.lettersFont = UIFont.boldSystemFont(ofSize: 30)
        config.backgroundColors = [bgColor]// [self.getLetterColor(with: showLetter)]
        config.lettersColor = letterColor
        config.useSingleLetter = showLetter.count <= 1
        config.username = showLetter
        
        return UIImage.makeLetterAvatar(withConfiguration: config) ?? UIImage(named: "Place_Holder_Square")
    }
    
    func getLetterColor(with str: String) -> UIColor{
        switch str.first?.lowercased() {
        case "a":
            return #colorLiteral(red: 0.007843137255, green: 0.4470588235, blue: 0.6235294118, alpha: 1)
        case "b":
            return #colorLiteral(red: 0.1058823529, green: 0.7333333333, blue: 0.6078431373, alpha: 1)
        case "c":
            return #colorLiteral(red: 0.3019607843, green: 0.2980392157, blue: 0.368627451, alpha: 1)
        case "d":
            return #colorLiteral(red: 0.2705882353, green: 0.4117647059, blue: 0.4980392157, alpha: 1)
        case "e":
            return #colorLiteral(red: 0.8, green: 0.6862745098, blue: 0.0431372549, alpha: 1)
        case "f":
            return #colorLiteral(red: 0.8156862745, green: 0.2862745098, blue: 0.2705882353, alpha: 1)
        case "g":
            return #colorLiteral(red: 0.9529411765, green: 0.6117647059, blue: 0.06666666667, alpha: 1)
        case "h":
            return #colorLiteral(red: 0.4509803922, green: 0.5568627451, blue: 0.5960784314, alpha: 1)
        case "i":
            return #colorLiteral(red: 0.2745098039, green: 0.5843137255, blue: 0.7019607843, alpha: 1)
        case "j":
            return #colorLiteral(red: 0.831372549, green: 0.5960784314, blue: 0.3607843137, alpha: 1)
        case "k":
            return #colorLiteral(red: 0.2078431373, green: 0.2235294118, blue: 0.3607843137, alpha: 1)
        case "l":
            return #colorLiteral(red: 0.6196078431, green: 0.7333333333, blue: 0.6235294118, alpha: 1)
        case "m":
            return #colorLiteral(red: 0.9137254902, green: 0.4470588235, blue: 0.3803921569, alpha: 1)
        case "n":
            return #colorLiteral(red: 0.3647058824, green: 0.5098039216, blue: 0.2980392157, alpha: 1)
        case "o":
            return #colorLiteral(red: 0.4470588235, green: 0.7529411765, blue: 0.7607843137, alpha: 1)
        case "p":
            return #colorLiteral(red: 0.8431372549, green: 0.5450980392, blue: 0.4901960784, alpha: 1)
        case "q":
            return #colorLiteral(red: 0.3843137255, green: 0.5254901961, blue: 0.5529411765, alpha: 1)
        case "r":
            return #colorLiteral(red: 0.4039215686, green: 0.6196078431, blue: 0.7882352941, alpha: 1)
        case "s":
            return #colorLiteral(red: 0.6470588235, green: 0.3215686275, blue: 0.2980392157, alpha: 1)
        case "t":
            return #colorLiteral(red: 0.1294117647, green: 0.4862745098, blue: 0.4941176471, alpha: 1)
        case "u":
            return #colorLiteral(red: 0.1607843137, green: 0.2509803922, blue: 0.3215686275, alpha: 1)
        case "v":
            return #colorLiteral(red: 0.9215686275, green: 0.6901960784, blue: 0.2078431373, alpha: 1)
        case "w":
            return #colorLiteral(red: 0.3843137255, green: 0.5450980392, blue: 0.3803921569, alpha: 1)
        case "x":
            return #colorLiteral(red: 0.8156862745, green: 0.5411764706, blue: 0.6862745098, alpha: 1)
        case "y":
            return #colorLiteral(red: 0.5411764706, green: 0.2117647059, blue: 0.3019607843, alpha: 1)
        case "z":
            return #colorLiteral(red: 0.3960784314, green: 0.431372549, blue: 0.4588235294, alpha: 1)
        default:
            return #colorLiteral(red: 0.003921568627, green: 0.6705882353, blue: 0.7215686275, alpha: 1)
        }
    }
}
