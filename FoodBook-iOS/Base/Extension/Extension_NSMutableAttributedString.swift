

import Foundation
import UIKit

extension NSMutableAttributedString {
    @discardableResult func bold(_ text:String) -> NSMutableAttributedString {
        //let attrs = [NSFontAttributeName : UIFont(name: "AvenirNext-Medium", size: 14)!]
        let attrs = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 13.0)]
        let boldString = NSMutableAttributedString(string:"\(text)", attributes:attrs)
        self.append(boldString)
        return self
    }
    
    @discardableResult func normal(_ text:String)->NSMutableAttributedString {
        let normal =  NSAttributedString(string: text)
        self.append(normal)
        return self
    }
}
