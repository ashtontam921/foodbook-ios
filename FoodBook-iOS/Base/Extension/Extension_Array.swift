

import Foundation


extension Array {
    
    public subscript(safe index: Int) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
}

extension Array where Element: Equatable {
    mutating func remove(_ obj: Element) {
        if let index = self.firstIndex(of: obj){
            self.remove(at: index)
        }
    }
}
