

import Foundation
import UIKit

// MARK: UIWindow
extension UIWindow {
    func visibleViewController() -> UIViewController? {
        if let rootViewController: UIViewController = self.rootViewController {
            return UIWindow.getVisibleViewControllerFrom(controller: rootViewController)
        }
        return nil
    }
    
    class func getVisibleViewControllerFrom(controller: UIViewController) -> UIViewController {
        if controller.isKind(of: UINavigationController.self), let navigationController = controller as? UINavigationController, let visibleController = navigationController.visibleViewController {
            return UIWindow.getVisibleViewControllerFrom(controller: visibleController)
            
        } else if controller.isKind(of: UITabBarController.self), let tabBarController = controller as? UITabBarController, let selectedController = tabBarController.selectedViewController {
            return UIWindow.getVisibleViewControllerFrom(controller: selectedController)
            
        } else {
            if let presentedViewController = controller.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(controller: presentedViewController)
            } else {
                return controller
            }
        }
    }
}
