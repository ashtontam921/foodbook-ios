
import UIKit
import MessageUI
import MapKit


class ExternalHelper: NSObject {
    private override init() { } // Singleton
    static let shared: ExternalHelper = ExternalHelper()
    
    class func shareAction(withData: (url: URL?, content: String?, image: UIImage?), textPriority: Bool = true, source: Any? = nil, on viewController: UIViewController) {
        var contentArray: [ActivityItemSource] = []
        let _textPriority = (withData.content != nil) ? textPriority : false
        
        if let url = withData.url {
            contentArray.append(ActivityItemSource(with: url, textPriority: _textPriority))
        }
        if let content = withData.content {
            contentArray.append(ActivityItemSource(with: content, textPriority: _textPriority))
        }
        if let image = withData.image {
            contentArray.append(ActivityItemSource(with: image, textPriority: _textPriority))
        }
        
        let activity = UIActivityViewController(activityItems: contentArray, applicationActivities: nil)
        
        viewController.present(activity, animated: true, completion: nil)
    }
    
    class func openLink(with url: URL, pushFrom navigationController: UINavigationController? = nil, title: String? = nil) -> Bool {
        var urlString = url.absoluteString
        if !(urlString.hasPrefix("http://") || urlString.hasPrefix("https://")) {
            urlString = "http://" + urlString
        }
        
        if let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
            return true
        }
        return false
    }
    
    class func openLink(with htmlStr: String, pushFrom navigationController: UINavigationController? = nil, title: String? = nil) -> Bool {
        
//        if let controller = navigationController {
//            let webViewController = UIStoryboard.init(name:"Shared", bundle: nil).instantiateViewController(withIdentifier: "InAppWebViewController") as! InAppWebViewController
//            webViewController.htmlStr = htmlStr
//            webViewController.hidesBottomBarWhenPushed = true
//            webViewController.title = title
//            
//            controller.pushViewController(webViewController, animated: true)
//            return true
//        }
        return false
    }
    
    class func callPhone(withNumber: String, completion:(Bool, Int)->()) {
        /*
         * Code
         * 0: Success
         * 1: Failure. No number
         * 2: Failure. Fail to call
        */
        guard withNumber != "" else {
            completion(false, 1)
            return
        }
        
        let filteredNumber = withNumber.replacingOccurrences(of: "-", with: "").replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+", with: "")
        guard let url = URL(string: "tel://\(filteredNumber)") else {
            completion(false, 2)
            return
        }
        
        if UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
            completion(true, 0)
        } else {
            completion(false, 2)
        }
    }
    
    //MARK: - Instance Functions
    func composeMail(withEmail: [String], from controller: UIViewController) {
        let mailComposeViewController = configuredMailComposeViewController(withRecipients: withEmail)
        if MFMailComposeViewController.canSendMail() {
            mailComposeViewController.navigationBar.tintColor = .navigationColor
            controller.present(mailComposeViewController, animated: true, completion: nil)
        } else {
            AlertUtil.showAlert(withTitle: "email_compose_error".localized(), message: "email_compose_error_message".localized())
        }
    }
    
    //MARK: - Private Functions
    fileprivate func configuredMailComposeViewController(withRecipients: [String]) -> MFMailComposeViewController {
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        
        composer.setToRecipients(withRecipients)
        composer.setSubject("")
        
        return composer
    }
}

// MARK: - MFMailComposeViewControllerDelegate
extension ExternalHelper: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - UIActivityItemSource
fileprivate class ActivityItemSource: UIActivityItemProvider {
    var content: Any
    var textPriority: Bool = false
    
    init(with content: Any, textPriority: Bool) {
        self.content = content
        self.textPriority = textPriority
        super.init(placeholderItem: content)
    }
    
    override func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return content
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        guard let type = activityType else {
            return content
        }

        if textPriority, type.rawValue == "com.facebook.Messenger.ShareExtension", content is UIImage {
            // NOTE: Prevent sharing image on facebook messenger as it overrides the text. Only triggers when textPriority flag is 'true'
            return nil
        }
        return content
    }
    
    override func activityViewController(_ activityViewController: UIActivityViewController, thumbnailImageForActivityType activityType: UIActivity.ActivityType?, suggestedSize size: CGSize) -> UIImage? {
        if let image = content as? UIImage {
            return image
        }
        return nil
    }
}
