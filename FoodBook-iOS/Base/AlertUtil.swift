

import UIKit

enum SnackbarDirection {
    case top, bottom
}

class AlertUtil: NSObject {
    private override init() { } // NOTE: Partially defeats it's purpose as utility class. Instance is required to ensure the snackbar only show ONCE
    private static let shared: AlertUtil = AlertUtil()
    
    class func showAlert(withTitle: String?, message: String?, options: [(String, UIAlertAction.Style)] = [("ok".localized(), .default)], completion: ((Int) -> Void)? = nil) {
        let alertController = UIAlertController.init(title: withTitle, message: message, preferredStyle: .alert)
        createAlertController(from: alertController, with: options, completion: completion)
    }
    
    class func showAlertSheet(withTitle: String?, message: String?, options: [(String, UIAlertAction.Style)] = [("cancel".localized(), .cancel)], sourceView: Any? = nil, completion:((Int) -> Void)?  = nil) {
        let alertController = UIAlertController(title: withTitle, message: message, preferredStyle: .actionSheet)
        let options = options
        let offset = 0
        createAlertController(from: alertController, indexOffset: offset, with: options, completion: completion)
    }
    
    class func showActionSheet(controller: UIViewController, title: String?, dataArr: [String], completion: ((String) -> Void)?){
        DispatchQueue.main.async {
            controller.view.endEditing(true)

            let alert = UIAlertController(title: title, message: nil, preferredStyle: .actionSheet)
            
            for data in dataArr{
                let button = UIAlertAction(title: data, style: .default, handler: { action in
                    
                    completion?(data)
                    
                })
                alert.addAction(button)
            }
            
            let cancelBtn = UIAlertAction(title: ("cancel".localized()), style: .cancel, handler: nil)
            
            alert.addAction(cancelBtn)
            
            //Configure for iPad presentation
            alert.popoverPresentationController?.sourceView = controller.view
            alert.popoverPresentationController?.sourceRect = CGRect(x: controller.view.bounds.midX, y: controller.view.bounds.midY, width: 0, height: 0)
            alert.popoverPresentationController?.permittedArrowDirections = []
            
            controller.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showError(with message: String, onToast: Bool = false, yMargin: CGFloat = 0) {
        if onToast {
            MessageManager.showMessage(message: message, type: .STAlertError)
        } else {
            let alertController = UIAlertController(title: "error".localized(), message: message, preferredStyle: .alert)
            createAlertController(from: alertController, with: [("ok".localized(), .default)], completion: nil)
        }
    }
    
    class func showError(with error: Error, onToast: Bool = false, yMargin: CGFloat = 0, separateSnackbarInstance: Bool = false) {
        if onToast {
            MessageManager.showMessage(message: error.localizedDescription, type: .STAlertError)
        } else {
            let alertController = UIAlertController(title: "error".localized(), message: error.localizedDescription, preferredStyle: .alert)
            createAlertController(from: alertController, with: [("ok".localized(), .default)], completion: nil)
        }
    }
    
    class func showError(with error: ServiceError, onToast: Bool = false, yMargin: CGFloat = 0, separateSnackbarInstance: Bool = false) {
        if onToast {
            MessageManager.showMessage(message: error.message, type: .STAlertError)
        } else {
            let alertController = UIAlertController(title: "error".localized(), message: error.message, preferredStyle: .alert)
            createAlertController(from: alertController, with: [("ok".localized(), .default)], completion: nil)
        }
    }
    
    class func requestPasscode(controller: UIViewController? = nil, handler: @escaping(String?) -> ()) {
        let alert = UIAlertController(title: "passcode_required".localized(), message: "passcode_required_message".localized(), preferredStyle: .alert)
        alert.addTextField(configurationHandler: { textField in
            textField.isSecureTextEntry = true
            textField.keyboardType = .numberPad
        })
        
        createAlertController(from: alert, in: controller, with: [("cancel".localized(), .cancel), ("ok".localized(), .default)], completion: { index in
            if index == 1, let text = alert.textFields?.first?.text {
                handler(text)
            } else {
                handler(nil)
            }
        })
    }
    
    // MARK: - Private class functions
    fileprivate class func createAlertController(from alert: UIAlertController, in controller: UIViewController? = nil,  indexOffset: Int? = nil, with actions: [(title: String, style: UIAlertAction.Style)] = [], completion:((Int) -> Void)?) {
        for (index, option) in actions.enumerated() {
            let action = UIAlertAction(title: option.title, style: option.style, handler: { handler in
                if let completion = completion {
                    if let indexOffset = indexOffset {
                        completion(index + indexOffset)
                    } else {
                        completion(index)
                    }
                }
            })
            alert.addAction(action)
        }
        
        if #available(iOS 13.0, *) {
            alert.overrideUserInterfaceStyle = .light
        }
        
        if let controller = controller {
            controller.present(alert, animated: true, completion: nil)
        } else {
//            let window = UIWindow(frame: UIScreen.main.bounds)
//            window.rootViewController = UIViewController()
//            window.windowLevel = UIWindow.Level.alert + 1
//            window.makeKeyAndVisible()
//            window.rootViewController?.present(alert, animated: true, completion: nil)
            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
        }
    }
}
