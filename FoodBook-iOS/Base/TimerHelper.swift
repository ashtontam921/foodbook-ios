
import UIKit

class TimerHelper: NSObject {
    static let shared = TimerHelper()
    
    var timerLength: Double = 0
    var timer = Timer()

    func setCountDownTimer(length: Double = 120.0, interval: Double = 1.0, completion: ((Double, String) -> Void)?){
        self.timerLength = length
        self.timer.invalidate()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (timer) in
            
            if self.timerLength <= 0{
                self.timerLength = 0
                timer.invalidate()
            }else{
                self.timerLength -= timer.timeInterval
            }
            
            completion?(self.timerLength, self.timerLength.secondsToMinutesSeconds())
        })
    }
    
    func setCountUpTimer(interval: Double = 1.0, completion: ((Double, String) -> Void)?){
        self.timerLength = 0
        self.timer.invalidate()
        
        self.timer = Timer.scheduledTimer(withTimeInterval: interval, repeats: true, block: { (timer) in
            
            self.timerLength += timer.timeInterval
            completion?(self.timerLength, self.timerLength.secondsToMinutesSeconds())
        })
    }
    
    func invalidated(){
        self.timer.invalidate()
    }
}
