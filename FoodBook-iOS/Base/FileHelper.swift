

import UIKit
import Foundation
import ALLoadingView

class FileHelper: NSObject {
    private static var privateInstance : FileHelper? // NOTE: Implementation may be thread unsafe. Refactor if found thread-safe method while allowing the singleton to be reset back to nil
    
    static let manager = FileManager.default
    private var task: URLSessionDownloadTask?
    private var alertView: UIAlertController?
    private var progressView: UIProgressView?
    private var session: URLSession?
    private var outputFileURL: URL?
    private var completion: ((Bool) -> ())?
    
    // MARK: - Singleton
    final class func shared() -> FileHelper {
        guard let shared = privateInstance else {
            privateInstance = FileHelper()
            return privateInstance!
        }
        return shared
    }
    
    func dispose() {
        session?.invalidateAndCancel()
        task?.cancel()
        task = nil
        FileHelper.privateInstance = nil
    }
    
    // MARK: - Functions
    func download(with url: URL?, completion: @escaping (Bool, URL) -> ()){
        guard let url = url else { return }
        downloadFile(with: url, completion: completion)
    }
    
    // MARK: Private Functions
    private func load(url: URL, completion: @escaping (Bool) -> ()) {
        let sessionConfig = URLSessionConfiguration.background(withIdentifier: "VideoPlayUntilSessionConfig")
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue.main)
        let request = try! URLRequest(url: url, method: .get)
        
        self.completion = completion
        self.task = session?.downloadTask(with: request)
        self.task?.resume()
    }
    
    private func downloadFile(with url: URL, completion: @escaping (Bool, URL) -> ()) {
        outputFileURL = FileHelper.getTempFilePath(with: url)
        guard let outputFileURL = outputFileURL else { return }
        if let fileURL = FileHelper.validateFile(at: outputFileURL){
            completion(true, fileURL)
        } else {
            downloadAlert(with: url, completion: completion)
        }
    }
    
    private func downloadAlert(with url: URL, completion: @escaping (Bool, URL) -> ()) {
        alertView = UIAlertController(title: "downloading".localized(), message: "downloading_message".localized(), preferredStyle: .alert)

        guard let alertView = self.alertView else { return }
        alertView.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: { (alertAction) in
            self.dispose()
        }))
        
        UIApplication.topViewController()?.present(alertView, animated: true, completion: { [weak self] in
            guard let self = self else { return }
            let margin:CGFloat = 8.0
            let rect = CGRect(x: margin, y: 72.0, width: alertView.view.frame.width - margin * 2.0, height: 2.0)
            self.progressView = UIProgressView(frame: rect)
            guard let progressView = self.progressView else { return }
            progressView.progress = 0
            progressView.tintColor = UIColor.blue
            alertView.view.addSubview(progressView)

            self.load(url: url) {[weak self] success in
                guard let self = self else { return }
                guard let outputFileURL = self.outputFileURL else { return }
                completion(success, outputFileURL)
                self.dispose()
            }
        })
    }
    
    // MARK: - Class Functions
    // MARK: File Download
    class func load(url: URL, to localUrl: URL, completion: @escaping (Bool) -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        let request = try! URLRequest(url: url, method: .get)
        
        let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let _ = (response as? HTTPURLResponse)?.statusCode {
                    //CommonUtil.log("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion(true)
                } catch (let err) {
                    completion(false)
                }
                
            } else {
                completion(false)
            }
        }
        task.resume()
    }
    
    /**
     Checks whether the file exists. Returns the path back if the file exists or nil for otherwise.
     */
    class func validateFile(at path: URL) -> URL? {
        var isDir: ObjCBool = false
        if manager.fileExists(atPath: path.path, isDirectory: &isDir) {
            if isDir.boolValue {
                return nil
            } else {
                return path
            }
        }
        return nil
    }
    
    /**
     Checks whether the directory exists and creates new directory if it does not exist. Returns the path back if the directory exists or when the directory is successfully created.
     */
    class func validateDirectory(at path: URL) -> URL? {
        var isDir: ObjCBool = true
        if manager.fileExists(atPath: path.path, isDirectory: &isDir) {
            if isDir.boolValue {
                return path
            } else {
                return nil
            }
        } else {
            do {
                try manager.createDirectory(atPath: path.path, withIntermediateDirectories: true, attributes: nil)
                return path
            } catch let error {
            }
        }
        return nil
    }
    
    class func getTempFilePath(with url: URL) -> URL{
        var tempDirectoryURL: URL
        if #available(iOS 10.0, *) {
            tempDirectoryURL = FileManager.default.temporaryDirectory
        } else {
            tempDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory())
        }
        tempDirectoryURL.appendPathComponent(url.lastPathComponent)
        return tempDirectoryURL
    }
    
    /**
     Get current folder directory
     */
    class func getFolderDirectory() -> URL? {
        let documentPath = manager.urls(for: .documentDirectory, in: .userDomainMask)
        return documentPath.first
    }
    
    class func createFolder(name: String){
        guard let documentDirectory = FileHelper.getFolderDirectory()?.appendingPathComponent(name)  else {
            return
        }
        
        if let _ = FileHelper.validateDirectory(at: documentDirectory) {
        } else {
            do {
                try FileManager.default.createDirectory(at: documentDirectory, withIntermediateDirectories: false, attributes: nil)
            } catch let error as NSError {
            }
        }
    }
    
    class func checkFileExist(url: String) -> Bool {
        return FileManager.default.fileExists(atPath: url)
    }
    
    class func deleteFile(at path: URL) -> Error? {
        do {
            try manager.removeItem(at: path)
            return nil
        } catch let error {
            return error
        }
    }
}

extension FileHelper: URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        guard let alertView = self.alertView else { return }
        alertView.dismiss(animated: true, completion: nil)
        
        if let _ = (downloadTask.response as? HTTPURLResponse)?.statusCode {
            //CommonUtil.log("Success: \(statusCode)")
        }
        
        do {
            guard let outputFileURL = self.outputFileURL else { return }
            try FileManager.default.copyItem(at: location, to: outputFileURL)
            self.completion?(true)
        } catch (let err) {
            self.completion?(false)
        }
    }
    
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        if totalBytesExpectedToWrite > 0 {
            let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
            self.progressView?.progress = progress
        }
    }
}


//with filename
extension FileHelper{
    
    // MARK: - Functions
    func download(with urlRequest: URLRequest?, fileName: String, completion: @escaping (Bool, URL) -> ()){
        guard let urlRequest = urlRequest else { return }
        downloadFile(with: urlRequest, fileName: fileName, completion: completion)
    }
    
    // MARK: Private Functions
    private func load(urlRequest: URLRequest, completion: @escaping (Bool) -> ()) {
        let sessionConfig = URLSessionConfiguration.background(withIdentifier: "VideoPlayUntilSessionConfig")
        session = URLSession(configuration: sessionConfig, delegate: self, delegateQueue: OperationQueue.main)
        
        self.completion = completion
        self.task = session?.downloadTask(with: urlRequest)
        self.task?.resume()
    }
    
    private func downloadFile(with urlRequest: URLRequest, fileName: String, completion: @escaping (Bool, URL) -> ()) {
        outputFileURL = FileHelper.getTempFilePath(with: fileName)
        guard let outputFileURL = outputFileURL else { return }
        if let fileURL = FileHelper.validateFile(at: outputFileURL){
            completion(true, fileURL)
        } else {
            downloadAlert(with: urlRequest, completion: completion)
        }
    }
    
    private func downloadAlert(with urlRequest: URLRequest, completion: @escaping (Bool, URL) -> ()) {
        alertView = UIAlertController(title: "downloading".localized(), message: "downloading_message".localized(), preferredStyle: .alert)

        guard let alertView = self.alertView else { return }
        alertView.addAction(UIAlertAction(title: "cancel".localized(), style: .cancel, handler: { (alertAction) in
            self.dispose()
        }))
            
            UIApplication.topViewController()?.present(alertView, animated: true, completion: {[weak self] in
                guard let self = self else { return }
                self.load(urlRequest: urlRequest) {[weak self] success in
                    guard let self = self else { return }
                    guard let outputFileURL = self.outputFileURL else { return }
                    completion(success, outputFileURL)
                    self.dispose()
                }
            })
            
        }
    
    
    // MARK: - Class Functions
    // MARK: File Download
    class func load(urlRequest: URLRequest, to localUrl: URL, completion: @escaping (Bool) -> ()) {
        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig)
        
        let task = session.downloadTask(with: urlRequest) { (tempLocalUrl, response, error) in
            if let tempLocalUrl = tempLocalUrl, error == nil {
                // Success
                if let _ = (response as? HTTPURLResponse)?.statusCode {
                    //CommonUtil.log("Success: \(statusCode)")
                }
                
                do {
                    try FileManager.default.copyItem(at: tempLocalUrl, to: localUrl)
                    completion(true)
                } catch (let err) {
                    completion(false)
                }
                
            } else {
                completion(false)
            }
        }
        task.resume()
    }
    
    class func getTempFilePath(with fileName: String) -> URL{
        var tempDirectoryURL: URL
        if #available(iOS 10.0, *) {
            tempDirectoryURL = FileManager.default.temporaryDirectory
        } else {
            tempDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory())
        }
        tempDirectoryURL.appendPathComponent(fileName)
        return tempDirectoryURL
    }
    
}


public enum DirectoryName {
    static let BUS_TICKET = "BusTicket"
    static let BILL_PAYMENT = "BillPayment"
    static let BILL_PAYMENT_INVOICE = "BillPaymentInvoice"
    static let ENQUIRY_DOCUMENT = "EnquiryDocument"
}
