

import UIKit
import DropDown

class DropDownHelper: NSObject {
    static let shared = DropDownHelper()
    let dropdown = DropDown()
    
    func configureDropDown(options: [String], anchorView: UIView, backgroundColor: UIColor = .white, width: CGFloat, xOffset: CGFloat = 0, font: UIFont = UIFont.systemFont(ofSize: 14), completion: ((Int, String) -> Void)?) {
        dropdown.backgroundColor = backgroundColor
        dropdown.selectionBackgroundColor = .lightGray
        dropdown.anchorView = anchorView
        dropdown.width = width
        dropdown.bottomOffset = CGPoint(x: xOffset, y:(dropdown.anchorView?.plainView.bounds.height)!)
                
        dropdown.dataSource = options
        dropdown.textFont = font
        dropdown.direction = .any
        dropdown.dismissMode = .onTap
        dropdown.selectionAction = { index, item in
            DispatchQueue.main.async {
                self.dropdown.hide()
                completion?(index, item)
            }
        }
    }
    
    func setSelected(index: Int?){
        self.dropdown.selectRow(at: index)
        
        if let unwrapIndex = index, let string = self.dropdown.dataSource[safe: unwrapIndex]{
            self.dropdown.selectionAction?(unwrapIndex, string)
        }
    }
    
    func showDropDown(){
        self.dropdown.show()
    }
    
    func hideDropDown(){
        self.dropdown.hide()
    }
}
