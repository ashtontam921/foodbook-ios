
//
//import UIKit
//import ObjectMapper
//
//class VersionControlUtil: NSObject {
//    static var isDisplayingAppUpdateNotification = false
//
//    private static let versionApi = LoginAPI.getAppVersion
//    private static var shouldPromptSoftUpdate = true
//
//    static func checkVersion(){
//        if !VersionControlUtil.isDisplayingAppUpdateNotification{
//            compareLatestAppVersion(completion: {versionModel in
//                DispatchQueue.main.async {
//                    if versionModel.forceUpdate {
//                        VersionControlUtil.isDisplayingAppUpdateNotification = true
//
//                        let alert = UIAlertController.init(title: "update_available_forced".localized(), message: "update_available_forced_message".localized(), preferredStyle: .alert)
//                        alert.addAction(UIAlertAction.init(title: "update".localized(), style: .default, handler: { alertAction in
//                            VersionControlUtil.isDisplayingAppUpdateNotification = false
//
//                            if let url = versionModel.updateUrl, UIApplication.shared.canOpenURL(url)  {
//                                UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                            }
//                        }))
//
//                        UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
//                    }else{
//                        if VersionControlUtil.shouldPromptSoftUpdate{
//                            VersionControlUtil.isDisplayingAppUpdateNotification = true
//
//                            let alert = UIAlertController.init(title: "update_available".localized(), message: "update_available_message".localized(), preferredStyle: .alert)
//                            alert.addAction(UIAlertAction.init(title: "cancel".localized(), style: .destructive, handler: { alertAction in
//                                VersionControlUtil.isDisplayingAppUpdateNotification = false
//                                VersionControlUtil.shouldPromptSoftUpdate = false
//
//                                //Check if stuck at initial view controller
//                                if let vc = UIApplication.topViewController(), vc is InitializeViewController {
//                                    vc.viewWillAppear(false)
//                                }
//                            }))
//                            alert.addAction(UIAlertAction.init(title: "update".localized(), style: .default, handler: { alertAction in
//                                VersionControlUtil.isDisplayingAppUpdateNotification = false
//                                VersionControlUtil.shouldPromptSoftUpdate = false
//
//                                //Check if stuck at initial view controller
//                                if let vc = UIApplication.topViewController(), vc is InitializeViewController {
//                                    vc.viewWillAppear(false)
//                                }
//                                if let url = versionModel.updateUrl, UIApplication.shared.canOpenURL(url)  {
//                                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
//                                }
//                            }))
//
//                            UIApplication.topViewController()?.present(alert, animated: true, completion: nil)
//                        }
//                    }
//                }
//            })
//        }
//    }
//
//    private static func compareLatestAppVersion(completion: @escaping (AppVersionModel) -> Void) {
////        RequestClient.request(Router.Whale(api: VersionControlUtil.versionApi), completion: { result in
////
////            switch result{
////            case .success(let json):
////                guard let data = json?.rawString(), let versionModel = Mapper<AppVersionModel>().map(JSONString: data), self.verifyAPIVersion(with: versionModel.versionName) else {
////                    return
////                }
////                completion(versionModel)
////            case .error(_):
////                break
////            }
////        })
//    }
//
//    private static func verifyAPIVersion(with APIVersion: String) -> Bool {
//        if let currentVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String {
//            return compareVersion(a: APIVersion, b: currentVersion)
//        }
//
//        return false
//    }
//
//    private static func compareVersion(a: String, b: String) -> Bool {
//        let componentA = a.components(separatedBy: ".")
//        let componentB = b.components(separatedBy: ".")
//
//        let iteration = max(componentA.count, componentB.count)
//        for index in 0..<iteration {
//            var valA = 0
//            var valB = 0
//            if index < componentA.count, let valueA = Int(componentA[index]) {
//                valA = valueA
//            }
//            if index < componentB.count, let valueB = Int(componentB[index]) {
//                valB = valueB
//            }
//            if index == iteration - 1 || valB != valA {
//                return valB < valA
//            }
//        }
//
//        return false
//    }
//}
