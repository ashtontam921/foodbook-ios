

import UIKit
import DKImagePickerController

class ImagePickerHelper: NSObject {
    
    static let shared = ImagePickerHelper()
    
    var target: BaseViewController?
    var maxUploadCount = 1
    var completion: (([UIImage]) -> Void)?
    
    override init() {
        super.init()
    }
    
    func presentPicker(target: BaseViewController, maxUploadCount: Int = 1, completion: (([UIImage]) -> Void)?){
        self.target = target
        self.maxUploadCount = maxUploadCount
        self.completion = completion
        
        presentOptionPicker()
    }
}

extension ImagePickerHelper: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: false, completion: {
            if let image = info[.originalImage] as? UIImage{
                self.completion?([image])
            }
        })
    }
    
    //MARK:- Function
    fileprivate func presentOptionPicker() {
        let option: [(String, UIAlertAction.Style)] = [("cancel".localized(), .cancel), ("take_photo".localized(), .default),("camera_roll".localized(), .default)]
        
        AlertUtil.showAlertSheet(withTitle: nil, message: nil, options: option, completion: { index in
            DispatchQueue.main.async {
                if index == 1 {
                    if UIImagePickerController.isSourceTypeAvailable(.camera) {
                        PermissionUtil.getCameraPermission(block: {success in
                            DispatchQueue.main.async {
                                guard success else {
                                    AlertUtil.showError(with: "camera_denied_message".localized())
                                    return
                                }
                                let picker = UIImagePickerController()
                                picker.delegate = self
                                picker.sourceType = .camera
                                self.target?.present(picker, animated: true, completion: nil)
                            }
                        })
                    } else {
                        self.presentImagePicker(hasCamera: false)
                    }
                } else if index == 2 {
                    self.presentImagePicker()
                }
            }
        })
    }
    
    fileprivate func presentImagePicker(hasCamera: Bool = true) {
        let imagePicker = DKImagePickerController()
        imagePicker.sourceType = hasCamera ? .both : .photo
        imagePicker.assetType = .allPhotos
        imagePicker.maxSelectableCount = maxUploadCount
        imagePicker.showsCancelButton = true
        imagePicker.navigationController?.navigationItem.setRightBarButton(nil, animated: true)
        
        imagePicker.didSelectAssets = { [weak self] (assets: [DKAsset]) in
            guard let self = self else {return}
            
            let group = DispatchGroup()
            let queue = DispatchQueue.global(qos: .userInitiated)
            var tempImages: [UIImage] = []
            var dispatchError: Bool = false
            
            self.target?.setLoadingState(enabled: true)
            DispatchQueue.global(qos: .userInitiated).async {
                for asset in assets {
                    group.enter()
                    queue.async(group: group, execute: {
                        asset.fetchOriginalImage(completeBlock: { image, info in
                            if let image = image {
                                tempImages.append(image)
                            } else {
                                dispatchError = true
                            }
                            group.leave()
                        })
                    })
                    group.wait()
                }
                
                group.notify(queue: DispatchQueue.main) {
                    self.target?.setLoadingState(enabled: false)
                    if dispatchError {
                        AlertUtil.showError(with: "unexpected_error_message".localized(), onToast: true)
                        return
                    }
                    
                    self.completion?(tempImages)
                }
            }
        }
        
        target?.present(imagePicker, animated: true, completion: nil)
    }
}
