//
//  XMLHelper.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 08/08/2021.
//

import Foundation
import SWXMLHash

class XMLHelper {
    init() {
        
    }
    
    func map(xmlFileName: String) -> XMLIndexer?{
        if let url = Bundle.main.url(forResource: xmlFileName, withExtension: "xml"), let dataString: String = try? String.init(contentsOf: url) {
            let xml = SWXMLHash.parse(dataString)
            
            return xml
        }else{
            return nil
        }
    }
}

extension XMLHelper {
    class func getRecipeType() -> [String]{
        let fileName = "recipetypes"
        let xmlHelper = XMLHelper()
        
        if let xmlIndexer = xmlHelper.map(xmlFileName: fileName){
            let data = xmlIndexer["data"]["recipetypes"].all.map({ $0.element?.text ?? "" })
            
            return data
        }
        
        return []
    }
}
