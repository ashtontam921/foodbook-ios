

import UIKit


class CountryUtil: NSObject {
    typealias CountryTuple = (num: String, code: String, name: String)
    
    private static let favList: [String] = ["CN", "MY", "VN", "JP", "KR", "LA", "SG", "TH", "PH", "ID", "TW"]
    
    private override init() { }
    private static let fileName = "PhoneCountries"
    
    class func getCountryImage(by code: String) -> UIImage? {
        let name = String(format: "%@_flag.png", code.lowercased())
        return UIImage.init(named: name)
    }
    
    class func getCountryIndex(number: String, from list: [CountryTuple]) -> Int? {
        if let index = list.firstIndex(where: {$0.num == number}) {
            return index
        } else {
            return nil
        }
    }
    
    class func getCountryIndex(by code: String, from list: [CountryTuple]) -> Int? {
        if let index = list.firstIndex(where: {$0.code == code}) {
            return index
        } else {
            return nil
        }
    }
    
    class func getCountry(by code: String, from list: [CountryTuple]) -> CountryTuple? {
        guard let index = getCountryIndex(by: code, from: list) else {
            return nil
        }
        return list[safe: index]
    }
    
    class func fetchCountryList(ascending: Bool = true) -> [CountryTuple] {
        var list: [CountryTuple] = []
        if let url = Bundle.main.url(forResource: fileName, withExtension: "txt"), let dataString: String = try? String.init(contentsOf: url) {
            for countryData in dataString.components(separatedBy: "\n") {
                let data = countryData.components(separatedBy: ";")
                
                let temp: CountryTuple = (num: data[safe: 0] ?? "", code: data[safe: 1] ?? "", name: data[safe: 2] ?? "")
                if temp.num != "" {
                    list.append(temp)
                }
            }
        }
        if ascending {
            list.sort(by: {$0.name.compare($1.name) == .orderedAscending})
        }
        return list
        
        
//        var favCollections:[CountryTuple] = []
//        for fav in favList{
//            if let item = (list.filter{ $0.code == fav }).first{
//                favCollections.append(item)
//            }
//        }
//
//        return favCollections
    }
    
    class func getGroupedCountryList(with sortedCountryList: [CountryTuple]) -> Array<[CountryTuple]> {
        
        var groupedContacts = sortedCountryList.reduce([[CountryTuple]]()) {
            guard var last = $0.last else { return [[$1]] }
            var collection = $0
            if last.first!.name.first == $1.name.first {
                last += [$1]
                collection[collection.count - 1] = last
            } else {
                collection += [[$1]]
            }
            return collection
        }
        
        var favCollections:[CountryTuple] = []
        for fav in favList{
            if let item = (sortedCountryList.filter{ $0.code == fav }).first{
                favCollections.append(item)
            }
        }
        
        groupedContacts.insert(favCollections, at: 0)

        return groupedContacts
                
//        groupedContacts.insert(favCollections, at: 0)
//
//        return groupedContacts
    }
}
