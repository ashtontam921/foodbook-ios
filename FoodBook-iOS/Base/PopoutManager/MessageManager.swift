

import UIKit

class MessageManager: NSObject {
    
    //MARK:- STAlertSuccess, STAlertError
    class func showMessage(message: String, type: STAlertType){
        self.showMessage(message: message, type: type, displayType: .STAlertDisplayTypeTop)
    }
    
    class func showMessage(error: Error, type: STAlertType){
        self.showMessage(message: error.localizedDescription, type: type, displayType: .STAlertDisplayTypeTop)
    }
    
    class func showMessage(error: ServiceError, type: STAlertType){
        self.showMessage(message: error.message, type: type, displayType: .STAlertDisplayTypeTop)
    }
    
    class func showMessage(message: String, type: STAlertType, displayType: STAlertDisplayType){
        STAlertController.presentSTAlert(stAlertType: type, stAlertDisplayType: displayType, message: message)
    }
}
