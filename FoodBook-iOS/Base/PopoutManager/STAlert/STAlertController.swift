

import UIKit
enum STAlertType: Int {
    case STAlertSuccess = 1,
    STAlertError,
    STAlertPopover
}

enum STDismissType: Int {
    case STDismissFromTop = 1,
    STDismissFromLeft,
    STDismissFromRight
}

enum STAlertDisplayType: Int {
    case STAlertDisplayTypeTop = 1,
    STAlertDisplayTypeOverlayNavBar,
    STAlertDisplayTypeOverlayNavBar_Custom,
    STAlertDisplayTypeBottom,
    STAlertDisplayTypeOverlayTabBar
}

class STAlertController: NSObject, UIPopoverPresentationControllerDelegate {
    typealias TapClose = ()->()
    
    
    static let sharedInstance = STAlertController()
    
    var showView:STAlertTypeViewController?
    var messages:[STAlertTypeViewController] = []
    var isShowing:Bool = false
    
    let defaultDuration:TimeInterval = 0.25
    let defaultshowDuration:TimeInterval = 3
    
    override init() {
        super.init()
    }
    
    // MARK: - Popover
    class func popoverErrorMessage(message: String, target: Any, popFrom: Any){
        let vc = STAlertTypeViewController.init(stAlertType: .STAlertPopover, message: message)
        
        let popFromTarget = popFrom as! UIView
        
        vc.preferredContentSize = CGSize(width: popFromTarget.frame.size.width, height: 50)
        
        vc.view.clipsToBounds = true
        vc.modalPresentationStyle = .popover
        
        let popController = vc.popoverPresentationController
        popController?.permittedArrowDirections = .down
        popController?.delegate = self.sharedInstance
        popController?.backgroundColor = vc.view.backgroundColor
        
        popController?.sourceView = (target as! UIViewController).view
        popController?.sourceRect = self.getViewRect(view: popFromTarget, target: target)
//        popController?.canOverlapSourceViewRect = false
        (target as! UIViewController).present(vc, animated: true, completion: nil)
    }
    
    class func getViewRect(view: UIView, target: Any) -> CGRect{
        let viewPoint = view.convert(view.bounds.origin, to: (target as! UIViewController).view)
        return CGRect(x: viewPoint.x, y: viewPoint.y, width: view.frame.size.width, height: view.frame.size.height)
    }
    
    private class func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
    // MARK: - Show Alert
    //set 1
    class func presentSTAlert(stAlertType: STAlertType, stAlertDisplayType: STAlertDisplayType, message: String){
        self.presentSTAlert(stAlertType: stAlertType, stAlertDisplayType: stAlertDisplayType, message: message, tapClose: nil)
    }
    
    class func presentSTAlert(stAlertType: STAlertType, stAlertDisplayType: STAlertDisplayType, message: String, tapClose: TapClose?){
        self.presentSTAlert(stAlertType: stAlertType, stAlertDisplayType: stAlertDisplayType, message: message, tapClose: tapClose, duration: self.sharedInstance.defaultDuration)
    }
    
    class func presentSTAlert(stAlertType: STAlertType, stAlertDisplayType: STAlertDisplayType, message: String, tapClose: TapClose?, duration: TimeInterval){
        self.presentSTAlert(stAlertType: stAlertType, stAlertDisplayType: stAlertDisplayType, message: message, tapClose: tapClose, duration: self.sharedInstance.defaultDuration, showDuration: self.sharedInstance.defaultshowDuration)
    }
    
    class func presentSTAlert(stAlertType: STAlertType, stAlertDisplayType: STAlertDisplayType, message: String, tapClose: TapClose?=nil, duration: TimeInterval, showDuration: TimeInterval){
        let vc = STAlertTypeViewController.init(stAlertType: stAlertType, stAlertDisplayType: stAlertDisplayType, message: message, duration: duration, showDuration: showDuration, topView: UIApplication.topViewController(), tapClose: tapClose)
        _ = vc.view
        self.prepareNotificationToBeShown(messageView: vc)
    }
    
    class func prepareNotificationToBeShown(messageView: STAlertTypeViewController){
        for vc in self.sharedInstance.messages{
            if(vc.message == messageView.message){
                self.sharedInstance.messages = []
                return
            }
        }
        
        self.sharedInstance.messages.append(messageView)
        
        if(self.sharedInstance.isShowing == false){
            self.sharedInstance.showAlert()
        }
    }
    
    func showAlert(){
        if(self.messages.count == 0){
            return
        }
        
        self.showView = self.messages[0] as STAlertTypeViewController
        
        if(self.showView?.topView.self != UIApplication.topViewController().self){
            self.prepareForNextMessage()
            return
        }
        
        self.isShowing = true
        self.showView?.show(completion: {
            self.prepareForNextMessage()
        })
    }
    
    
    func prepareForNextMessage(){
//        self.messages.remove(at: self.showView!)
        self.messages.remove(self.showView!)
        self.isShowing = false
        self.showAlert()
    }
}
