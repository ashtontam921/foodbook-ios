

import UIKit

class STAlertTypeViewController: UIViewController {
    @IBOutlet var successView: UIView!
    @IBOutlet var errorView: UIView!
    @IBOutlet var popoverView: UIView!
    
    var topView: UIViewController?
    var message: String = ""
    
    private var oriHeight: CGFloat = 0
    private var stAlertType: STAlertType = .STAlertSuccess
    private var stAlertDisplayType: STAlertDisplayType = .STAlertDisplayTypeTop
    private var stDismissType: STDismissType = .STDismissFromTop
    private var duration: TimeInterval = 0.25
    private var showDuration: TimeInterval = 3
    private var didTapCloseBlock: STAlertController.TapClose?
    private var timer:Timer?
    private var completion: (() -> Void)?
    private var currentView: UIView{
        switch self.stAlertType {
        case .STAlertSuccess:
            return self.successView
        case .STAlertError:
            return self.errorView
        case .STAlertPopover:
            return self.popoverView
        }
    }
    
    var oriYGap: CGFloat = 0//5.0
    
    convenience init(stAlertType: STAlertType, message: String) {
        self.init()
        self.stAlertType = stAlertType
        self.message = message
    }
    
    convenience init(stAlertType: STAlertType,
                     stAlertDisplayType: STAlertDisplayType,
                     message: String,
                     duration: TimeInterval,
                     showDuration: TimeInterval,
                     topView: UIViewController?,
                     tapClose: STAlertController.TapClose?) {
        self.init()
        self.stAlertType = stAlertType
        self.stAlertDisplayType = stAlertDisplayType
        self.message = message
        self.duration = duration
        self.showDuration = showDuration
        self.topView = topView
        self.didTapCloseBlock = tapClose
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view = currentView
        if(self.stAlertType != .STAlertPopover){
            self.setupDisplayType()
            self.setupTouchEvent()
        }else{
            self.setupPopoverDisplayType()
        }
    }
    
    // MARK: - Display type
    private func setupDisplayType(){
        
        var rect = view.frame
        rect.size.width = UIScreen.main.bounds.size.width-20
        rect.origin.x = 10
        view.frame = rect
        
        let label:UILabel = view.viewWithTag(1) as! UILabel
        label.text = self.message
        label.sizeToFit()
        
        rect.size.height = label.frame.size.height + 20
        view.frame = rect
        self.oriHeight = view.frame.size.height
    }
    
    // MARK: - Popover Display type
    func setupPopoverDisplayType(){
        let label:UILabel = view.viewWithTag(1) as! UILabel
        label.text = self.message
        
        var rect = label.frame
        rect.size.width = self.preferredContentSize.width
        label.frame = rect
        label.sizeToFit()
        
        rect = view.frame
        rect.size.width = label.frame.size.width + 20
        rect.size.height = label.frame.size.height + 20
        view.frame = rect
        
        self.preferredContentSize = CGSize(width: view.frame.size.width, height: view.frame.size.height)
        
        self.oriHeight = view.frame.size.height
    }
    
    // MARK: - Popover Display type
    private func setupTouchEvent(){
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(close))
        view.addGestureRecognizer(tapRecognizer)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(close))
        swipeUp.direction = UISwipeGestureRecognizer.Direction.up
        view.addGestureRecognizer(swipeUp)
    }
    
    // MARK: - show shadow
    private func showShadow(){
        view.layer.masksToBounds = false
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.1
        view.layer.shadowRadius = 4.0
        view.layer.shadowOffset = CGSize(width: 3.0, height: 3.0)
    }
    
    // MARK: - Close
    @objc func closeFromLeft(){
        self.stDismissType = .STDismissFromLeft
        self.close()
    }
    
    @objc func closeFromRight(){
        self.stDismissType = .STDismissFromRight
        self.close()
    }
    @objc func close(){
        if(self.didTapCloseBlock != nil){
            self.didTapCloseBlock!()
        }
        self.fadeMeOut()
    }
}

//MARK: -UI
extension STAlertTypeViewController{
    private func getSTAlertPositionY() -> CGFloat{
        //top
        topView?.viewWillAppear(false)
        let vcParent = topView?.parent
        
        if(vcParent?.isKind(of: UITabBarController.self) == true){
            if #available(iOS 11, *) {
                if let guide = vcParent?.presentingViewController?.view.safeAreaLayoutGuide{
                    return guide.layoutFrame.height
                }
            }else{
                if let y = vcParent?.presentingViewController?.view.frame.origin.y{
                    return y
                }
            }
            //        }else if(vcParent?.isKind(of: UINavigationController.self) == true){
        }else if let vcParent = topView?.navigationController{
            if #available(iOS 11, *), vcParent.view.backgroundColor != .clear && vcParent.navigationBar.isHidden != true{
                var topPadding: CGFloat = 20
                
                if let window = UIApplication.shared.keyWindow{
                    topPadding = window.safeAreaInsets.top
                }
                let guide = vcParent.view.safeAreaLayoutGuide
                
                return guide.layoutFrame.minY-topPadding
            }else{
                return (stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar || stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar_Custom || vcParent.navigationBar.isHidden) ? UIApplication.shared.statusBarFrame.size.height : self.get_visible_size_top().height
            }
        }else if((vcParent?.presentedViewController) != nil){
            if #available(iOS 11, *) {
                if let top = vcParent?.presentedViewController?.view.safeAreaInsets.top{
                    return top
                }
            }else{
                if let y = vcParent?.presentedViewController?.view.frame.origin.y{
                    return y
                }
            }
        }else{
            if #available(iOS 11, *) {
                if let top = vcParent?.presentedViewController?.view.safeAreaInsets.top{
                    return top
                }
            }else{
                if let y = vcParent?.presentedViewController?.view.frame.origin.y{
                    return y
                }
            }
        }
        
        return get_visible_size_top().height
    }
    
    private func get_visible_size_top() -> CGSize{
        var result:CGSize!
        var size:CGSize!
        
        size = UIApplication.shared.statusBarFrame.size
        result = size
        
        if(topView?.navigationController != nil && topView?.navigationController?.isNavigationBarHidden == false){
            size = topView?.navigationController?.navigationBar.frame.size
            result.height = result.height + size.height
        }
        return result
    }
    
    private func get_visible_size_bottom() -> CGSize{
        var result:CGSize!
        var size:CGSize!
        
        size = UIScreen.main.bounds.size
        result = size
        
        if let topVC = UIApplication.topViewController(), topVC.navigationController != nil{
            size = topView?.tabBarController?.tabBar.frame.size
            result.height = result.height - min(size.width, (topView?.tabBarController?.tabBar.isHidden)! ? 0 : size.height);
        }
        return result
    }
}

//MARK:- Show
extension STAlertTypeViewController{
    func show(completion: @escaping (() -> Void)){
        self.completion = completion
        var rect = view.frame
        if(stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar || stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar_Custom){
            rect.origin.y = 0 - oriHeight
            view.frame = rect
            rect.origin.y = getSTAlertPositionY() + oriYGap
        }else{
            rect.origin.y = self.getSTAlertPositionY() - oriYGap - oriHeight
            view.frame = rect
        }
        
        view.layer.zPosition = 1
        if(stAlertDisplayType == .STAlertDisplayTypeBottom || stAlertDisplayType == .STAlertDisplayTypeOverlayTabBar){
            let oriY = rect.origin.y
            rect.origin.y = oriY - oriHeight
        }
        
        rect.origin.y = getSTAlertPositionY() - oriYGap
        
        var mainView = topView?.view
        if(topView?.navigationController != nil && (stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar || stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar_Custom)){
            mainView = topView?.navigationController?.view
            UIApplication.shared.delegate?.window??.windowLevel = UIWindow.Level.statusBar+1
        }else if(topView?.tabBarController != nil && stAlertDisplayType == .STAlertDisplayTypeOverlayTabBar){
            mainView = topView?.tabBarController?.view
        }
        
        if let mainView = mainView{
            mainView.addSubview(view)
            view.layoutSubviews()
            setupDisplayType()
            view.translatesAutoresizingMaskIntoConstraints = false
            let topConstraint = NSLayoutConstraint(item: view!, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mainView, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1, constant: rect.origin.y)
            let horizontalConstraint = NSLayoutConstraint(item: view!, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mainView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0)
            let widthConstraint = NSLayoutConstraint(item: view!, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: view.frame.size.width)
            let heightConstraint = NSLayoutConstraint(item: view!, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: view.frame.size.height)
            mainView.addConstraints([topConstraint, horizontalConstraint, widthConstraint, heightConstraint])
        }
        UIView.animate(withDuration: duration, animations: {
            self.view.frame = rect
        }) { (finished) in
            if finished{
                self.showShadow()
                self.timer?.invalidate()
                self.timer = Timer.scheduledTimer(timeInterval: self.showDuration, target: self, selector: #selector(self.dismissAlert_Timer), userInfo: ["animation" :true], repeats: false)
            }else{
                completion()
            }
        }
    }
    
    //MARK:- Dismiss
    private func fadeMeOut(){
        if let timer = timer, timer.isValid{
            self.timer?.invalidate()
            self.dismissAlert()
        }
    }
    
    private func dismissAlert(){
        self.dismissAlert(animation: true)
    }
    
    @objc private func dismissAlert_Timer(){
        if let timer = self.timer{
            let userInfo = timer.userInfo as! Dictionary<String, AnyObject>
            let animation:Bool = (userInfo["animation"] as! Bool)
            self.dismissAlert(animation: animation)
        }
    }
    
    private func dismissAlert(animation: Bool){
        var rect = view.frame
        if(stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar || stAlertDisplayType == .STAlertDisplayTypeOverlayNavBar_Custom){
            if(stDismissType == .STDismissFromLeft){
                rect.origin.x = -view.frame.size.width
            }else if(stDismissType == .STDismissFromRight){
                rect.origin.x = UIScreen.main.bounds.size.width
            }else{
                rect.origin.y = 0 - view.frame.size.height
            }
        }else{
            rect.origin.y = 0 - view.frame.size.height
        }
        
        if(animation == true){
            UIView.animate(withDuration: duration, animations: {
                self.view.frame = rect
            }) { (finished) in
                UIApplication.shared.delegate?.window??.windowLevel = UIWindow.Level.normal
                self.view.removeFromSuperview()
                self.completion?()
            }
        }else{
            view.frame = rect
            view.removeFromSuperview()
            self.completion?()
        }
    }
}
