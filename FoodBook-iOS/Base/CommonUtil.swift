

import AVFoundation
import LocalAuthentication
import Photos
import UserNotifications

import SKPhotoBrowser
import SwiftyJSON
import ObjectMapper
import VideoToolbox

import STPopup

// MARK: - Common Utilities
class CommonUtil {
//    // MARK: Device
//    enum DeviceType {
//        case iPhone, iPad, iPod
//    }
//    
//    class func deviceType() -> DeviceType {
//        switch UIDevice.current.userInterfaceIdiom {
//        case .phone:
//            if UIDevice.current.model.contains("iPod", compareOption: .caseInsensitive) {
//                return .iPod
//            }
//            return .iPhone
//        case .pad:
//            return .iPad
//        default:
//            return .iPhone
//        }
//    }
    
    // MARK: Data
    class func formDayList() -> [(identifier: Int, title: String)] {
        return [
            (1, "monday"),
            (2, "tuesday"),
            (3, "wednesday"),
            (4, "thursday"),
            (5, "friday"),
            (6, "saturday"),
            (7, "sunday")
        ]
    }
    
    // MARK: Casting
    class func castStringToInt(from data: inout Any?) -> Int {
        // Note: Works only if the variable is originally String or Int
        let value = data
        data = nil
        return self.castStringToInt(from: value)
    }
    
    class func castStringToInt(from data: Any?) -> Int {
        // Note: Works only if the variable is originally String or Int
        if let unwrappedData = data as? String, let castedData = Int(unwrappedData) {
            return castedData
        } else if let castedData = data as? Int {
            return castedData
        }
        return -1
    }
    
    class func castStringToDouble(from data: inout Any?) -> Double {
        // Note: Works only if the variable is originally String, Int or Double
        let value = data
        data = nil
        return self.castStringToDouble(from: value)
    }
    
    class func castStringToDouble(from data: Any?) -> Double {
        // Note: Works only if the variable is originally String, Int or Double
        if let unwrappedData = data as? String, let castedData = Double(unwrappedData) {
            return castedData
        } else if let unwrappedData = data as? Int {
            return Double(unwrappedData)
        } else if let castedData = data as? Double {
            return castedData
        }
        return -1
    }
    
    // MARK: Text Field
    class func setTextFieldTint(color: UIColor, for view: UIView) {
        if let textField = view as? UITextField {
            textField.tintColor = color
            textField.textColor = color
        }
        for subview in view.subviews {
            setTextFieldTint(color: color, for: subview)
        }
    }
    
    // MARK: Drawable Paths / Shape Layers
    class func formShapeLayer(frame: CGRect, subtractionFrame: CGRect?) -> CAShapeLayer {
        let mask = CAShapeLayer()
        let path = UIBezierPath(rect: frame)
        
        if let subtractionFrame = subtractionFrame {
            path.move(to: CGPoint(x: subtractionFrame.origin.x, y: subtractionFrame.origin.y))
            path.addLine(to: CGPoint(x: subtractionFrame.origin.x + subtractionFrame.size.width, y: subtractionFrame.origin.y))
            path.addLine(to: CGPoint(x: subtractionFrame.origin.x + subtractionFrame.size.width, y: subtractionFrame.origin.y + subtractionFrame.size.height))
            path.addLine(to: CGPoint(x: subtractionFrame.origin.x, y: subtractionFrame.origin.y + subtractionFrame.size.height))
            path.close()
            mask.fillRule = CAShapeLayerFillRule.evenOdd
        }
        mask.path = path.cgPath
        return mask
    }
    
    // MARK: Calculation
    class func getDividedItemWidth(from totalWidth: CGFloat, rowCount: CGFloat, spacing: CGFloat, insets: UIEdgeInsets, offsetValue: CGFloat = 0) -> CGFloat {
        let constant: CGFloat = totalWidth - (insets.left + insets.right + offsetValue)
        let itemGap: CGFloat = spacing * (rowCount - 1.0)
        return ((constant - itemGap) / rowCount) - 0.00001
    }
    
    // MARK: Context
    @available(iOS 11, *)
    class func getBiometricType(with context: LAContext? = nil) -> LABiometryType? {
        if let context = context {
            return context.biometryType
        } else {
            let context = LAContext()
            var error: NSError? = nil
            if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
                return context.biometryType
            }
        }
        return nil
    }
    
    public static let hasHEVCHardwareEncoder: Bool = {
        let spec: [CFString: Any]
        #if os(macOS)
        spec = [ kVTVideoEncoderSpecification_RequireHardwareAcceleratedVideoEncoder: true ]
        #else
        spec = [:]
        #endif
        var outID: CFString?
        var properties: CFDictionary?
        if #available(iOS 11.0, *) {
            let result = VTCopySupportedPropertyDictionaryForEncoder(width: 1920, height: 1080, codecType: kCMVideoCodecType_HEVC, encoderSpecification: spec as CFDictionary, encoderIDOut: &outID, supportedPropertiesOut: &properties)
            if result == kVTCouldNotFindVideoEncoderErr {
                return false // no hardware HEVC encoder
            }
            return result == noErr
        } else {
            return false
        }
    }()
    
    // MARK: Generation & Metadata
    // NOTE: May use temporary directory
    class func generateAVPlayer(from asset: AVAsset, name: String, completion: @escaping (AVPlayer?) -> Void) {
        CommonUtil.generateAVAssetPath(from: asset, name: name, completion: { url in
            guard let url = url else {
                completion(nil)
                return
            }
            completion(AVPlayer(url: url))
        })
    }
    
    class func generateAVAssetPath(from asset: AVAsset, name: String, forceExport: Bool = false, completion: @escaping (URL?) -> Void) {
        if ((asset is AVComposition && hasHEVCHardwareEncoder) || forceExport), let exporter = AVAssetExportSession(asset: asset, presetName: AVAssetExportPreset1920x1080)  {
            let pathName = String(format: "temp-composition-%@.mov", name.replacingOccurrences(of: " ", with: ""))
            let tempPath = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(pathName)
            
            if FileManager.default.fileExists(atPath: tempPath.path), name != "" {
                completion(tempPath)
            } else {
                exporter.outputURL = tempPath
                exporter.outputFileType = .mov
                exporter.shouldOptimizeForNetworkUse = true
                exporter.exportAsynchronously(completionHandler: {
                    switch exporter.status {
                    case .completed:
                        completion(exporter.outputURL)
                    default:
                        completion(nil)
                    }
                })
            }

        } else if let asset = asset as? AVURLAsset {
            completion(asset.url)
        } else {
            completion(nil)
        }
    }
    
    class func getFileSize(ofFile path: URL) -> UInt64? {
        do {
            let attributes = try FileManager.default.attributesOfItem(atPath: path.path)
            return attributes[FileAttributeKey.size] as? UInt64
        } catch {
            return nil
        }
    }
    
    // MARK: Developer Debug
    class func log(_ string: String?) {
        if AppBuild.isDev() {
            debugPrint("PRINT LOG: " + (string ?? ""))
        }
    }

    class func apiLog(_ any: Any?) {
        if AppBuild.isDev() {
            debugPrint(any ?? "API: Failed to log API")
        }
    }
    
    class func printElapsedTime(name: String, operation:()->()) {
        let startTime = CFAbsoluteTimeGetCurrent()
        operation()
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        print("Time elapsed for \(name): \(timeElapsed) s.")
    }
    
    class func elapsedTimeInSeconds(operation: ()->()) -> Double {
        let startTime = CFAbsoluteTimeGetCurrent()
        operation()
        let timeElapsed = CFAbsoluteTimeGetCurrent() - startTime
        return Double(timeElapsed)
    }
    
    class func logUserDefaultContent() {
        for (key, value) in UserDefaults.standard.dictionaryRepresentation() {
            print("\(key) = \(value) \n")
        }
    }
}

// MARK: - Date Utilities
class DateUtil {
    class func parseDate(from timestamp: Int) -> Date? {
        guard timestamp != 0 else {
            return nil
        }
        
        let data = Double(timestamp)
        let date = Date(timeIntervalSince1970: data)
        
        return date
    }
    
    class func parseDate(from timestamp: inout Any?, normalize: Bool = false) -> Date? {
        var data = CommonUtil.castStringToDouble(from: &timestamp)
        guard data != 0 else {
            return nil
        }
        if normalize, data >= 10000000000 {
            data = data/1000
        }
        if data != -1 {
            let date = Date(timeIntervalSince1970: data)
            timestamp = nil
            return date
        }
        return nil
    }
    
    class func parseDates(from timestamps: inout Any?) -> [Date] {
        var dateList: [Date] = []
        if let data = timestamps as? [Any] {
            for timestamp in data {
                let time = CommonUtil.castStringToInt(from: timestamp)
                if time != -1 {
                    let date = Date(timeIntervalSince1970: Double(time))
                    dateList.append(date)
                }
            }
        }
        timestamps = nil
        return dateList
    }
    
    class func parseDate(from string: String, format: String) -> Date? {
        let df = DateFormatter()
        df.dateFormat = format
        df.locale =  Locale(identifier: "en_US_POSIX")
        df.timeZone = TimeZone.current
        
        return df.date(from: string)
    }
}

// UITextField Subclass
class UITextFieldPasteExclusion: UITextField {
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(UIResponderStandardEditActions.paste(_:)) {
            return false
        }
        return super.canPerformAction(action, withSender: sender)
    }
}

// MARK: - Object Mapper
// Mappable transform
// Object mapper needed
public let valueToBoolTransform = TransformOf<Bool, Any>(fromJSON: { (value: Any?) -> Bool? in
    if let value = value as? Double {
        return !(value == 0)
    }
    if let value = value as? Int {
        return !(value == 0)
    }
    if var value = value as? String {
        value = value.lowercased()
        if value == "y" || value == "n" {
            return (value == "y")
        } else if value == "true" || value == "false" {
            return (value == "true")
        } else if value == "yes" || value == "no" {
            return (value == "yes")
        } else {
            return (value != "")
        }
    }
    if let value = value as? Bool {
        return value
    }
    return nil
}, toJSON: { (value: Bool?) -> Any? in
    // transform value from Bool? to String?
    if let value = value {
        return String(value)
    }
    return nil
})
