
import UIKit

class TintTextField: UITextField {
    
    var tintedClearImage: UIImage?
    var submitBtn: UIButton?
    var editingChanged: ((String) -> Void)?
    var range: ClosedRange<Double>?
    var maxDecimalPlaces = 2
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        tintClearImage()
    }
    
    private func tintClearImage() {
        var tintedClearImage: UIImage?
        for view in subviews {
            if view is UIButton {
                let button = view as! UIButton
                if let uiImage = button.image(for: .highlighted) {
                    if tintedClearImage == nil {
                        tintedClearImage = tintImage(image: uiImage, color: tintColor)
                    }
                    button.setImage(tintedClearImage, for: .normal)
                    button.setImage(tintedClearImage, for: .highlighted)
                }
            }
        }
    }
    
    func tintImage(image: UIImage, color: UIColor) -> UIImage? {
        let size = image.size
        
        UIGraphicsBeginImageContextWithOptions(size, false, image.scale)
        if let context = UIGraphicsGetCurrentContext(){
            image.draw(at: CGPoint.zero, blendMode: CGBlendMode.normal, alpha: 1.0)
            
            context.setFillColor(color.cgColor)
            context.setBlendMode(CGBlendMode.sourceIn)
            context.setAlpha(1.0)
            
            let rect = CGRect(x: CGPoint.zero.x, y: CGPoint.zero.y, width: image.size.width, height: image.size.height)
            UIGraphicsGetCurrentContext()!.fill(rect)
            let tintedImage = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            
            return tintedImage
        }
        return nil
    }
    
    func isAmount(submitBtn: UIButton? = nil, range: ClosedRange<Double>? = nil, maxDecimalPlaces: Int = 2, editingChanged: ((String) -> Void)? = nil){
        self.submitBtn = submitBtn
        self.range = range
        self.maxDecimalPlaces = maxDecimalPlaces
        self.editingChanged = editingChanged
        self.keyboardType = .decimalPad
        self.delegate = self
        self.addTarget(self, action: #selector(textFieldDidChanged(_:)), for: .editingChanged)
    }
}

extension TintTextField: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard string != "" else {
            return true
        }
        
        let currentText = textField.text ?? ""
        let replacementText = (currentText as NSString).replacingCharacters(in: range, with: string)
        
        return replacementText.isValidCurrencyDouble(maxDecimalPlaces: maxDecimalPlaces, minimumValue: self.range?.lowerBound ?? 0, maximumValue: self.range?.upperBound)
    }
    
    @objc func textFieldDidChanged(_ textField: UITextField){
        guard let text = textField.text else { return }
        
        if let btn = submitBtn{
            if text.isEmpty{
                btn.setEnable(state: false)
            }else{
                if let amount = Double(text){
                    if amount >= range?.lowerBound ?? 0.01{
                        btn.setEnable(state: true)
                    }else{
                        btn.setEnable(state: false)
                    }
                }
            }            
        }
        
        editingChanged?(text)
    }
}
