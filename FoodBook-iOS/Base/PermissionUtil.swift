

import Foundation
import AVFoundation
import Photos

// MARK: - Permission Utilities
class PermissionUtil {
    class func getCameraPermission(block:@escaping (Bool) -> ()) {
        switch AVCaptureDevice.authorizationStatus(for: AVMediaType.video) {
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: AVMediaType.video, completionHandler: { granted in
                block(granted)
            })
            break
        case .denied, .restricted:
            block(false)
            break
        case .authorized:
            block(true)
            break
        @unknown default:
            break
        }
    }
    
    class func getPhotosPermission(block:@escaping (Bool) -> ()) {
        PHPhotoLibrary.requestAuthorization({ status in
            switch status {
            case .notDetermined:break
            case .denied, .restricted:
                block(false)
                break
            case .authorized:
                block(true)
                break
            @unknown default:
                break
            }
        })
    }
}
