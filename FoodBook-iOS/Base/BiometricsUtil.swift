

import Foundation
import UIKit
import LocalAuthentication

class BiometricsUtil{
    static let ENABLED_LOCAL_AUTHENTICATION = "ENABLED_LOCAL_AUTHENTICATION"
    
    class func getBiometricType() -> LABiometryType {
        let context = LAContext()
        var error: NSError? = nil
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            return context.biometryType
        }
        return .LABiometryNone
    }
    
    class func isLocalAuthenticationEnabled() -> Bool {
        return UserDefaults.standard.object(forKey: ENABLED_LOCAL_AUTHENTICATION) as? Bool ?? false
    }
    
    class func setLocalAuthentication(enabled: Bool) {
        UserDefaults.standard.set(enabled, forKey: ENABLED_LOCAL_AUTHENTICATION)
    }
    
    class func biometricsAuthentication(from controller: BaseViewController, reason: String, completion: @escaping (Bool)-> Void){
        let context = LAContext()
        var error: NSError? = nil
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            if isLocalAuthenticationEnabled() {
                context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: { validated, error in
                    if error != nil {
                        completion(false)
                    }
                    completion(validated)
                })
            } else {
                let type = context.biometryType.title
                enableLocalAuthentication(from: controller, type: type, completion: { success in
                    if success {
                        // FUTURE: Repeated code in line 317. Refactor required
                        context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason, reply: { validated, error in
                            if error != nil {
                                completion(false)
                            }
                            completion(validated)
                        })
                    } else {
                        completion(false)
                    }
                })
            }
        } else {
            completion(false)
        }
    }
    
    class func enableLocalAuthentication(from controller: BaseViewController, type: String, completion: @escaping (Bool) -> Void) {
        let title = String(format: "request_biometric_title".localized(), type)
        let message = String(format: "request_biometric_message".localized(), type)
        AlertUtil.showAlert(withTitle: title, message: message, options: [("cancel".localized(), .cancel), ("enable".localized(), .default)], completion: { index in
            DispatchQueue.main.async {
                if index == 1 {
                    AlertUtil.requestPasscode(controller: controller, handler: { value in
                        DispatchQueue.main.async {
                            guard let value = value else {
                                completion(false)
                                return
                            }
                            
                            verifyPasskey(from: controller, passkey: value, completion: { isSuccess in
                                completion(isSuccess)
                            })
                        }
                    })
                    
                } else {
                    self.setLocalAuthentication(enabled: false)
                    completion(false)
                }
            }
        })
    }
    
    class func verifyPasskey(from controller: BaseViewController, passkey: String, completion: @escaping (Bool) -> Void){
//        controller.setLoadingState(enabled: true)
//        MessageManager.showMessage(message: "", type: .STAlertError)
        
        completion(true)
    }
}

extension LABiometryType{
    var title: String{
        switch self {
        case .faceID:
            return "face_id".localized()
        case .touchID:
            return "touch_id".localized()
        default:
            return ""
        }
    }
    
    var image: UIImage?{
        switch self {
        case .faceID:
            return #imageLiteral(resourceName: "Icon_FaceID")
        case .touchID:
            return #imageLiteral(resourceName: "Icon_TouchID")
        default:
            return nil
        }
    }
    
    var image_small: UIImage?{
        switch self {
        case .faceID:
            return #imageLiteral(resourceName: "Icon_FaceID_Small")
        case .touchID:
            return #imageLiteral(resourceName: "Icon_TouchID_Small")
        default:
            return nil
        }
    }
}
