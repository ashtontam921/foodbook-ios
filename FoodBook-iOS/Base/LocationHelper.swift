
import UIKit
import CoreLocation

typealias locationObject = (location: CLLocation, searchName: String)

class LocationHelper: NSObject, CLLocationManagerDelegate {
    private override init() { }
    private static var privateInstance : LocationHelper? // NOTE: Implementation may be thread unsafe. Refactor if found thread-safe method while allowing the singleton to be reset back to nil
    private var locationManager = CLLocationManager()
    private let defaultCoordinate = CLLocation(latitude: 3.0152424, longitude: 101.6133448)
    var lastLocation: CLLocation?
    var completion: (([CLLocation], Error?) -> Void)? = nil
    var nearByCompletion: (([CLLocation], String? , Error?) -> Void)? = nil
    
    // MARK: - Singleton
    final class func shared() -> LocationHelper {
        guard let shared = privateInstance else {
            privateInstance = LocationHelper()
            return privateInstance!
        }
        return shared
    }
    
    func dispose() {
        LocationHelper.privateInstance = nil
    }

    // MARK: Functions
    public func getCurrentLocation(completion: @escaping(([CLLocation], Error?) -> Void)) {
        self.completion = completion
        locationManager.delegate = self
        if let accepted = requestWhenInUseLocation() {
            if accepted{
                locationManager.startUpdatingLocation()
            } else {
                completion([], ServiceError.unknownError(code: -1, message: "location_denied_message".localized()))
            }
        }
    }
    
    public func getNearByCurrentLocation(completion: @escaping(([CLLocation], String?, Error?) -> Void)){
        self.nearByCompletion = completion
        locationManager.delegate = self
        if let accepted = requestWhenInUseLocation() {
            if accepted{
                locationManager.startUpdatingLocation()
            }
        }
    }
    
    public func requestWhenInUseLocation() -> Bool? {
        let status = CLLocationManager.authorizationStatus()
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            return true
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            return nil
        case .denied, .restricted:
            return false
        }
    }

    // MARK: - CLLocationManagerDelegate
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        lastLocation = locations.first
        if let completion = completion {
            completion(locations, nil)
            self.completion = nil
        }
        if let nearByCompletion = nearByCompletion {
            nearByCompletion(locations, nil, nil)
            self.nearByCompletion = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        if let completion = completion {
            completion([], error)
            self.completion = nil
        }
        if let nearByCompletion = nearByCompletion {
            nearByCompletion([], nil, error)
            self.nearByCompletion = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse:
            if let completion = self.completion {
                getCurrentLocation(completion: completion)
            }
            if let nearByCompletion = self.nearByCompletion {
                getNearByCurrentLocation(completion: nearByCompletion)
            }
        case .authorizedAlways:
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        case .denied, .restricted:
            if let completion = self.completion {
                completion([], ServiceError.unknownError(code: -1, message: "location_denied_message".localized()))
            }
            
            nearByCompletion?([defaultCoordinate], nil, ServiceError.unknownError(code: -1, message: "location_denied_message".localized()))
        }
    }
}
