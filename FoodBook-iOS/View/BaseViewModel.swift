
import UIKit
import Alamofire
import ObjectMapper
import SwiftyJSON

struct Pagination {
    var currentPage = 1
    var currentIndex = 0
    var pageSize = 10
    var isFetching = false
    var isEndOfList = false
    var totalRecordKey = "totalRecords"
    
    mutating func reset(){
        currentPage = 1
        currentIndex = 0
        isEndOfList = false
    }
}

class BaseViewModel: NSObject {
    var successMessage: String = ""
    var isLoading: Bool = false{
        didSet{
            showLoading?(isLoading)
        }
    }
    
    var error: ServiceError? = nil{
        didSet{
            showError?(error)
        }
    }
    
    var showLoading: ((Bool) -> ())?
    var showError: ((ServiceError?) -> ())?
    var pagination = Pagination()
    
    override init() {
        
    }
    
    func defaultRequestClientForObject<T: Mappable>(api: URLRequestConvertible, type: T.Type, key: String, isLoading: Bool, showError: Bool, completion: @escaping (T?, JSON?) -> ()){
        self.isLoading = isLoading
        
        RequestClient.request(api, completion: { (result) in
            self.isLoading = false
            
            switch result{
            case .success(let json):
                guard let data = key.isEmpty ? json?.rawString() : json?[key].rawString(), let object = Mapper<T>().map(JSONString: data) else {
                    if showError{
                        self.error = ServiceError.unknownError(code: -1, message: "unexpected_error_message".localized())
                    }
                    
                    completion(nil, json)
                    return
                }
                
                completion(object, json)
                break
            case .error(let error):
                let json = JSON(dictionaryLiteral: ("Error", error.message))
                
                if showError{
                    self.error = error
                }
                
                completion(nil, json)
                break
            }
        })
    }
    
    func defaultRequestClientForArray<T: Mappable>(api: URLRequestConvertible, type: T.Type, keys: [JSONSubscriptType] , isLoading: Bool, showError: Bool, completion: @escaping ([T]?, JSON?) -> ()){
        self.isLoading = isLoading
        self.pagination.isFetching = true
        
        RequestClient.request(api, completion: { (result) in
            self.isLoading = false
            self.pagination.isFetching = false
            
            switch result{
            case .success(let json):
                guard let data = json?[keys].rawString(), let object = Mapper<T>().mapArray(JSONString: data) else {
                    if showError{
                        self.error = ServiceError.unknownError(code: -1, message: "unexpected_error_message".localized())
                    }
                    
                    completion(nil, json)
                    return
                }
                
                //Handle pagination
                if let totalRecords = json?[self.pagination.totalRecordKey].int{
                    let retrievedRecords = self.pagination.currentPage * self.pagination.pageSize
                    
                    self.pagination.isEndOfList = retrievedRecords >= totalRecords
                    self.pagination.currentIndex = retrievedRecords >= totalRecords ? totalRecords : retrievedRecords
                    self.pagination.currentPage += 1
                }else{
                    let retrievedRecords = (self.pagination.currentPage - 1) * self.pagination.pageSize + object.count
                    let totalExpectedRecord = self.pagination.currentPage * self.pagination.pageSize

                    self.pagination.currentIndex = retrievedRecords
                    self.pagination.isEndOfList = retrievedRecords < totalExpectedRecord
                     
                    if self.pagination.isEndOfList == false{
                        self.pagination.currentPage += 1
                    }
                }
                                
                completion(object, json)
                
                break
            case .error(let error):
                let json = JSON(dictionaryLiteral: ("Error", error.message))

                if showError{
                    self.error = error
                }
                
                completion(nil, json)
                
                break
            }
        })
    }
    
    func defaultUploadRequestClient<T: Mappable>(api: URLRequestConvertible, requestUploadObject: [(fileParam: String, fileName: String, data: Data)], param: [String: Any]?, dataType: T.Type, key: String, isLoading: Bool, showError: Bool, completion: @escaping (Bool, T?, JSON?) -> ()){
        self.isLoading = isLoading
        
        RequestClient.request(api, params: param ?? [:], method: .post, requestUploadObject: requestUploadObject) { (result) in
            self.isLoading = false
            
            switch result{
            case .success(let json):
                guard let data = key.isEmpty ? json?.rawString() : json?[key].rawString(), let object = Mapper<T>().map(JSONString: data) else {
                    
                    completion(true, nil, json)
                    return
                }
                
                completion(true, object, json)
                break
            case .error(let error):
                let json = JSON(dictionaryLiteral: ("Error", error.message))
                
                if showError{
                    self.error = error
                }
                
                completion(false, nil, json)
                break
            }
        }
    }
}

