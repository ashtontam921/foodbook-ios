
import UIKit
import Toast_Swift
import Localize_Swift
import StatefulViewController

class BaseViewController: UIViewController, StatefulViewController, ErrorViewDelegate, EmptyViewDelegate, UIScrollViewDelegate {
    enum NavigationBarStatus{
        case normal
        case transparent
        case hide
    }
    
    private var naviBarStatus: NavigationBarStatus?{
        didSet{
            if let status = naviBarStatus{
                updateNavigationBarColor(with: status)
            }
        }
    }
    
    var currentNaviBarStatus: NavigationBarStatus{
        return naviBarStatus ?? NavigationBarStatus.normal
    }
    
    private var fakeNavBar = UIView()
    private var titleLbl = UILabel()
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarStatus(.normal)
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        // Do any additional setup after loading the view.
        
        setupLocalized()
        NotificationCenter.default.addObserver(self, selector: #selector(languageChange), name: NSNotification.Name(LCLLanguageChangeNotification), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.refreshNavigationBarStyle()
        if modalPresentationStyle == .overCurrentContext{
            UIView.animate(withDuration: 0.3, animations: {
                self.presentingViewController?.view.alpha = 0.5
            })
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        refreshNavigationBarStyle()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if isMovingFromParent{
            NotificationCenter.default.removeObserver(self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isBeingDismissed, modalPresentationStyle == .overCurrentContext{
            UIView.animate(withDuration: 0.3, animations: {
                self.presentingViewController?.view.alpha = 1.0
            })
        }
    }
    
    override func present(_ viewControllerToPresent: UIViewController,
                          animated flag: Bool,
                          completion: (() -> Void)? = nil) {
        if #available(iOS 13.0, *) {
            if viewControllerToPresent.modalPresentationStyle == .pageSheet || viewControllerToPresent.modalPresentationStyle == .automatic{
                viewControllerToPresent.modalPresentationStyle = .fullScreen
            }
        } else {
            // Fallback on earlier versions
        }
        
      super.present(viewControllerToPresent, animated: flag, completion: completion)
    }
    
    @objc private func languageChange(){
        setupLocalized()
        (emptyView as? EmptyView)?.reloadView()
        (errorView as? ErrorView)?.reloadView()
    }
    
    func setupLocalized(){
        
    }
    
    //Basic binding like loading, success, error
    func setupViewModelBinding(with viewModel: BaseViewModel){
        viewModel.showLoading = { [weak self] isLoading in
            guard let self = self else { return }
            self.setLoadingState(enabled: isLoading)
        }
        
        viewModel.showError = { [weak self] error in
            guard let self = self else { return }
            if let error = error{
                print(self.emptyView)
                print(self.loadingView)
                print(self.emptyView)
                print(self.currentState)
                if (self.emptyView != nil) && (self.loadingView != nil) && (self.emptyView != nil) && self.currentState == .loading{
                    self.endLoading(animated: true, error: error, completion: {
                        MessageManager.showMessage(error: error, type: .STAlertError)
                    })
                }else{
                    MessageManager.showMessage(error: error, type: .STAlertError)
                }
            }
        }
    }
    
    //MARK:- Fake Navigation
    func addFakeNavigationViewWithTitle(title: String) -> (UIView, UILabel){
        let viewHeight: CGFloat = topBarHeight()
        let navView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize().width, height: viewHeight))
        
        navView.backgroundColor = .navigationColor
        navView.alpha = 0
        
        let lblHeight: CGFloat = 20
        let xOffset: CGFloat = 56
        let yOffset: CGFloat = lblHeight + 12
        let titleLbl = UILabel(frame: CGRect(x: xOffset, y: viewHeight - yOffset, width: navView.frame.size.width - (xOffset * 2), height: lblHeight))
        
        titleLbl.text = title
        titleLbl.textAlignment = .center
        titleLbl.textColor = .white
        titleLbl.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        
        navView.addSubview(titleLbl)
        self.view.addSubview(navView)
        fakeNavBar = navView
        self.titleLbl = titleLbl
        return (navView, titleLbl)
    }
    
    func addFakeNavigationViewWithTitleAndRightBarBtn(title: String, rightBarBtnImage: UIImage, selector: Selector){
        let viewHeight: CGFloat = topBarHeight()
        let navView = UIView(frame: CGRect(x: 0, y: 0, width: screenSize().width, height: viewHeight))
        
        navView.backgroundColor = .navigationColor
        navView.alpha = 0
        
        let lblHeight: CGFloat = 20
        let xOffset: CGFloat = 56
        let yOffset: CGFloat = lblHeight + 12
        let titleLbl = UILabel(frame: CGRect(x: xOffset, y: viewHeight - yOffset, width: navView.frame.size.width - (xOffset * 2), height: lblHeight))
        
        titleLbl.text = title
        titleLbl.textAlignment = .center
        titleLbl.textColor = .white
        titleLbl.font = UIFont.systemFont(ofSize: 17, weight: .semibold)
        
        let btnSize: CGFloat = 20
        let xSpacing: CGFloat = 25
        let rightBarBtn = UIButton(frame: CGRect(x: navView.frame.size.width - btnSize - xSpacing, y: viewHeight - btnSize - 12, width: btnSize, height: btnSize))
        
        rightBarBtn.setImage(rightBarBtnImage, for: .normal)
        rightBarBtn.addTarget(self, action: selector, for: .touchUpInside)
        
        navView.addSubview(titleLbl)
        navView.addSubview(rightBarBtn)
        self.view.addSubview(navView)
        fakeNavBar = navView
        self.titleLbl = titleLbl
    }
    
    dynamic func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let maxOffset: CGFloat = 25
        let yOffset = scrollView.contentOffset.y
        self.fakeNavBar.alpha = yOffset/maxOffset
    }
    
    // MARK: - Stateful Views
    func initStatefulView(update: Bool = false, on edgeInsets: UIEdgeInsets? = nil, backgroundColor: UIColor? = nil, hidesEmptyReloadButton: Bool = false, isNoRecentView: Bool = false) {
        startLoading()
        if let loadingNib = Bundle.main.loadNibNamed(LoadingView.nibName, owner: self, options: nil)?.first as? LoadingView {
            loadingNib.setupView(customInset: edgeInsets)
            loadingView = loadingNib
            if let color = backgroundColor {
                loadingView?.backgroundColor = color
            }
            loadingView?.frame = view.frame
            loadingView?.layoutIfNeeded()
            loadingNib.loadingAnimation()
        }
        if let emptyNib = Bundle.main.loadNibNamed(EmptyView.nibName, owner: self, options: nil)?.first as? EmptyView {
            emptyNib.setupView(customInset: edgeInsets)
            emptyNib.setReloadButton(hidden: hidesEmptyReloadButton)
            emptyNib.delegate = self
            emptyView = emptyNib
            if let color = backgroundColor {
                emptyView?.backgroundColor = color
            }
            emptyView?.frame = view.frame
            
//            if isNoRecentView{
//                emptyNib.loadNoRecentView()
//            }
        }
        if let errorNib = Bundle.main.loadNibNamed(ErrorView.nibName, owner: self, options: nil)?.first as? ErrorView {
            errorNib.setupView(customInset: edgeInsets)
            errorNib.delegate = self
            errorView = errorNib
            if let color = backgroundColor {
                errorView?.backgroundColor = color
            }
            errorView?.frame = view.frame
        }
        
        if !update {
            setupInitialViewState()
        }
    }
    
    func hasContent() -> Bool {
        return false
    }
    
    // MARK: - ErrorViewDelegate
    func errorViewReloadTapped() {
    }
    
    // MARK: - EmptyViewDelegate
    func emptyViewReloadTapped() {
    }
    
    
    // MARK: Loading states
    func setLoadingState(enabled: Bool, includeNavigationBar: Bool = false) {
        self.view.hideToastActivity()
        navigationBarInteraction(enabled: includeNavigationBar ? !enabled : true)
        if enabled {
            self.view.makeToastActivity(.center)
            self.view.isUserInteractionEnabled = false
        } else {
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func navigationBarInteraction(enabled: Bool) {
        if enabled {
            navigationController?.navigationBar.isUserInteractionEnabled = true
        } else {
            navigationController?.navigationBar.isUserInteractionEnabled = false
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK:- NavigationBar UI
extension BaseViewController{
    func setNavigationBarStatus(_ status: NavigationBarStatus){
        naviBarStatus = status
    }
    
    private func refreshNavigationBarStyle(){
        guard let status = naviBarStatus else { return }
        guard let navi = self.navigationController else { return }
        
            switch status {
            case .normal,
                 .transparent:
                navi.setNavigationBarHidden(false, animated: true)
            case .hide:
                navi.setNavigationBarHidden(true, animated: true)
            }
            self.updateNavigationBarColor(with: status)
    }
    
    private func updateNavigationBarColor(with status: NavigationBarStatus){
            switch status {
            case .normal:
                self.navigationController?.navigationBar.tintColor = .white
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = nil
                self.navigationController?.navigationBar.isTranslucent = false
                self.navigationController?.view.backgroundColor = .mainColor
                self.navigationController?.navigationBar.barTintColor = .mainColor
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            case .transparent:
                self.navigationController?.navigationBar.tintColor = .white
                self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.navigationBar.backgroundColor = .clear
                self.navigationController?.view.backgroundColor = .clear
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            case .hide:
                self.navigationController?.navigationBar.shadowImage = UIImage()
                self.navigationController?.navigationBar.isTranslucent = true
                self.navigationController?.navigationBar.backgroundColor = .clear
                self.navigationController?.view.backgroundColor = .clear
            }
//        })
        self.setNeedsStatusBarAppearanceUpdate()
    }
}


extension UIApplication {
    var statusView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}
