
import UIKit

class LoadingStatusViewController: BaseViewController {
    
    @IBOutlet weak var progressBarView: UIProgressView!
    @IBOutlet weak var cancelBtn: UIButton!
    var progress: Float = 0.0{
        didSet{
            progressBarView.progress = progress
        }
    }
    var dismissClicked: (() -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func setupLocalized() {
        cancelBtn.setTitle("cancel".localized(), for: .normal)
    }

    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: {
            self.dismissClicked?()
        })
    }
}
