
import UIKit
import WebKit

class InAppWebViewController: BaseViewController {
    @IBOutlet var webView: WKWebView!
    
    var url: URL? = nil
    var htmlStr: String? = nil
    var urlRequest: URLRequest? = nil

    override func viewDidLoad() {
        super.viewDidLoad()
        
        extendedLayoutIncludesOpaqueBars = true
        webView.navigationDelegate = self
        webView.withProgressView()

        if url != nil {
            loadLink()
        }else if let htmlStr = htmlStr {
            webView.backgroundColor = .white
            webView.loadHTMLString(htmlStr, baseURL: nil)
        }else if let request = urlRequest{
            setLoadingState(enabled: true)
            webView.load(request)
        }
    }
    
    private func loadLink() {
        if var url = url {
            if !url.absoluteString.starts(with: "http://") && !url.absoluteString.starts(with: "https://") {
                let urlString = "http://\(url.absoluteString)"
                if let formattedURL = URL(string: urlString) {
                    url = formattedURL
                }
            }
            let request = URLRequest(url: url)
            webView.load(request)
            setLoadingState(enabled: true)
        }
    }
}

extension InAppWebViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        webView.evaluateJavaScript("document.title") { (response, error) in
            if let webTitle = response as? String{
                self.title = webTitle
            }
        }
        
        setLoadingState(enabled: false)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        setLoadingState(enabled: false)
        AlertUtil.showError(with: error)
    }
}
