
import UIKit

class NoDataCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    class var nibName: String {
        return String(describing: self)
    }
    class var identifier: String {
        return "NoDataCell"
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        titleLabel.text = "no_data".localized()
        messageLabel.text = "no_data_message".localized()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
