
import UIKit

class DefaultTableViewCell: UITableViewCell {
    class var nibName: String {
        return String(describing: self)
    }
    class var identifier: String {
        return "DefaultTableViewCell"
    }
    @IBOutlet var imageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet var imageViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet var cellImageView: UIImageView!
    @IBOutlet var badgeImageView: UIImageView!
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var detailLabel: UILabel!
    
    func resetValues() {
        titleLabel.text = ""
        detailLabel.text = ""
        badgeImageView.isHidden = false
        imageViewCenterConstraint.isActive = false
    }
    
    func setValues(title: String, detail: String, image: UIImage?, badge: Bool = false) {
        titleLabel.text = title
        detailLabel.text = detail
        badgeImageView.isHidden = !badge
        
        if let image = image {
            imageViewLeadingConstraint.constant = 8
            imageViewHeightConstraint.constant = 50
            cellImageView?.isHidden = false
            
            cellImageView?.image = image
        } else {
            imageViewLeadingConstraint.constant = 0
            imageViewHeightConstraint.constant = 0
            cellImageView?.isHidden = true
            badgeImageView.isHidden = true // Automatically hides the badge if the left image view is hidden
        }
    }
    
    // Drawer for OmniMall
    func setValuesForDrawer(title: String, image: UIImage?) {
        titleLabel.text = title
        titleLabel.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        badgeImageView.isHidden = true
        
        if let image = image {
            imageViewLeadingConstraint.constant = 20
            imageViewHeightConstraint.constant = 20
            cellImageView?.isHidden = false
            cellImageView?.image = image
            imageViewCenterConstraint.isActive = true
        } else {
            imageViewLeadingConstraint.constant = 12
            imageViewHeightConstraint.constant = 0
            cellImageView?.isHidden = true
        }
    }
}
