
import UIKit

typealias CountryTuple = (num: String, code: String, name: String)

protocol CountryCodePickerDelegate: class {
    func didSelectCountry(_ picker: CountryCodePickerViewController, country: CountryTuple)
}

class CountryCodePickerViewController: BaseViewController {
    @IBOutlet var tableView: UITableView!
    
    private var fileName = "PhoneCountries"

    var groupedCountryList: Array<[CountryTuple]> = []
    var initialIndex: Int = -1
    weak var delegate: CountryCodePickerDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if groupedCountryList.count == 0 {
            let countryList = CountryUtil.fetchCountryList()
            groupedCountryList = CountryUtil.getGroupedCountryList(with: countryList)
        }
        
        if #available(iOS 11, *) {
            tableView.contentInsetAdjustmentBehavior = .never
        }
        tableView.dataSource = self
        tableView.delegate = self
        
        title = "Country".localized()
        registerNib()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: - Functions
    private func registerNib() {
        tableView.register(UINib(nibName: DefaultTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: DefaultTableViewCell.identifier)
    }
}

// MARK: - Table View
extension CountryCodePickerViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: Table View Data Source
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return ["*"]+UILocalizedIndexedCollation.current().sectionIndexTitles
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return groupedCountryList.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let country = groupedCountryList[safe: section]?.first,section > 0 else {
            return nil
        }
        return String(country.name.first ?? Character.init(""))
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groupedCountryList[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let countryTuple = groupedCountryList[safe: indexPath.section]?[safe: indexPath.row] else {
            return UITableViewCell()
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: DefaultTableViewCell.identifier, for: indexPath) as? DefaultTableViewCell else {
            
            let cell = UITableViewCell.init(style: .value1, reuseIdentifier: "TableViewCell")

            
            cell.textLabel?.text = countryTuple.name
            cell.detailTextLabel?.text = "+" + countryTuple.num
            cell.imageView?.image = CountryUtil.getCountryImage(by: countryTuple.code)
            
            return cell
        }
        
        cell.setValues(title: countryTuple.code.getLocalizedCountryName(), detail: "+" + countryTuple.num, image: CountryUtil.getCountryImage(by: countryTuple.code))
        return cell
    }
    
    // MARK: Table View Delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        guard let country = groupedCountryList[safe: indexPath.section]?[safe: indexPath.row] else {
            return
        }
        
        if let delegate = delegate {
            delegate.didSelectCountry(self, country: country)
        }
        self.navigationController?.popViewController(animated: true)
    }
}
