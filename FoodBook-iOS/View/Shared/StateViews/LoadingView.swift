
import UIKit
import StatefulViewController

class LoadingView: UIView, StatefulPlaceholderView {
    @IBOutlet var loadingView: UIView!
    
    private var defaultInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    class var nibName: String {
        return String(describing: self)
    }
    
    class func instanceFromNib() -> LoadingView {
        return UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! LoadingView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    func setupView(customInset: UIEdgeInsets? = nil) {
        if let customInset = customInset {
            defaultInset = customInset
        }
    }
    
    func loadingAnimation() {
        setUpAnimation(in: loadingView.layer, size: CGSize(width: loadingView.frame.size.width, height: loadingView.frame.size.height))
        
        let fadeAnimation = CAKeyframeAnimation(keyPath:"opacity")
        fadeAnimation.beginTime = CACurrentMediaTime()
        fadeAnimation.duration = 3
        fadeAnimation.keyTimes = [0, 0.125, 0.625, 1]
        fadeAnimation.values = [0.0, 0.0, 1.0, 1.0]
        fadeAnimation.isRemovedOnCompletion = false
        fadeAnimation.fillMode = CAMediaTimingFillMode.forwards
        loadingView.layer.add(fadeAnimation, forKey:"animateOpacity")
        loadingView.layer.opacity = 1.0
    }

    // StatefulPlaceholderView Protocol
    func placeholderViewInsets() -> UIEdgeInsets {
        return defaultInset
    }
    
    func setUpAnimation(in layer: CALayer, size: CGSize) {
        let duration: CFTimeInterval = 1.5
        let beginTime = CACurrentMediaTime()
        let beginTimes = [0.0, 0.75]
        let timingFunction = CAMediaTimingFunction(controlPoints: 0.12, 0.25, 0.5, 0.75)
        
        // Scale animation
        let scaleAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        
        scaleAnimation.keyTimes = [0, 0.7]
        scaleAnimation.timingFunction = timingFunction
        scaleAnimation.values = [0, 1]
        scaleAnimation.duration = duration
        
        // Opacity animation
        let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
        
        opacityAnimation.keyTimes = [1, 0]
        opacityAnimation.timingFunctions = [timingFunction, timingFunction]
        opacityAnimation.values = [0, 1]
        opacityAnimation.duration = duration
        
        // Animation
        let animation = CAAnimationGroup()
        
        animation.animations = [scaleAnimation, opacityAnimation]
        animation.duration = duration
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        
        // Draw circles
        for i in 0 ..< 2 {
            let circle = layerWith(size: size, color: UIColor.mainColor.cgColor)
            let frame = CGRect(x: (layer.bounds.size.width - size.width) / 2,
                               y: (layer.bounds.size.height - size.height) / 2,
                               width: size.width,
                               height: size.height)
            
            animation.beginTime = beginTime + beginTimes[i]
            circle.frame = frame
            circle.add(animation, forKey: "animation")
            layer.addSublayer(circle)
        }
    }
    
    func layerWith(size: CGSize, color: CGColor) -> CALayer {
        let layer: CAShapeLayer = CAShapeLayer()
        let path: UIBezierPath = UIBezierPath()
        let lineWidth: CGFloat = 5
        
        path.addArc(withCenter: CGPoint(x: size.width / 2, y: size.height / 2),
                    radius: size.width / 2,
                    startAngle: 0,
                    endAngle: CGFloat(2 * Double.pi),
                    clockwise: false)
        layer.fillColor = nil
        layer.strokeColor = color
        layer.lineWidth = lineWidth
        
        layer.backgroundColor = nil
        layer.path = path.cgPath
        layer.frame = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        return layer
    }
}
