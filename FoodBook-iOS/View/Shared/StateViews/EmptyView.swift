
import UIKit
import StatefulViewController

enum EmptyViewType {
    case normal, insufficientBalance, comingSoon, noMission, maintenance
}

protocol EmptyViewDelegate {
    func emptyViewReloadTapped()
}

class EmptyView: UIView, StatefulPlaceholderView {
    var defaultInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    var delegate: EmptyViewDelegate?
    
    @IBOutlet var emptyImage: UIImageView!
    @IBOutlet var emptyImageWidthToBtnRatio: NSLayoutConstraint!
    @IBOutlet var emptyImageTopConstraint: NSLayoutConstraint!
    @IBOutlet var emptyImageBtmConstraint: NSLayoutConstraint!
    @IBOutlet var emptyTitle: UILabel!
    @IBOutlet var emptyMessage: UILabel!
    @IBOutlet var refreshButton: UIButton!
    @IBOutlet var refreshButtonHeightConstraint: NSLayoutConstraint!
    
    var data: Any?
    
    class var nibName: String {
        return String(describing: self)
    }
    
    class func instanceFromNib() -> EmptyView {
        return UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EmptyView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(customInset: UIEdgeInsets? = nil, type: EmptyViewType = .normal) {
        if let customInset = customInset {
            defaultInset = customInset
        }
        
        switch type {
        case .comingSoon:
            setComingSoon()
            
            break
        case .maintenance:
            setMaintenance()
            
            break
        default:
            reloadView()
            
            break
        }
    }
    
    func setReloadButton(hidden: Bool) {
        refreshButton.isHidden = hidden
    }
    
    func setComingSoon(){
        emptyImage.image = #imageLiteral(resourceName: "Icon_Coming_Soon")
        emptyTitle.text = "coming_soon".localized()
        emptyMessage.text = "coming_soon_desc".localized()
        
        setReloadButton(hidden: true)
    }
    
    func setMaintenance(){
        emptyImage.image = UIImage(named: "Placeholder_Maintenance")
        emptyTitle.text = "error_maintenance".localized()
        emptyMessage.text = data as? String ?? "error_maintenance_desc".localized()
        refreshButton.setTitle("error_maintenance_exit".localized(), for: .normal)
    }
    
    func reloadView() {
        emptyImage.image = UIImage(named: "Placeholder_NoData")
        emptyTitle.text = "No Data".localized()
        emptyMessage.text = "No Data For Now".localized()
        
        refreshButton.setTitle("refresh".localized(), for: .normal)
        setReloadButton(hidden: false)
    }
    
    @IBAction func refreshAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.emptyViewReloadTapped()
        }
    }
    
    // StatefulPlaceholderView Protocol
    func placeholderViewInsets() -> UIEdgeInsets {
        return defaultInset
    }
}

