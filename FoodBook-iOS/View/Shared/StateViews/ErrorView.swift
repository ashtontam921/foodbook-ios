
import UIKit
import StatefulViewController

protocol ErrorViewDelegate {
    func errorViewReloadTapped()
}

class ErrorView: UIView, StatefulPlaceholderView {
    var defaultInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    @IBOutlet var reloadButton: UIButton!
    @IBOutlet var errorTitle: UILabel!
    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var errorImage: UIImageView!
    
    var delegate: ErrorViewDelegate?
    
    class var nibName: String {
        return String(describing: self)
    }
    
    class func instanceFromNib() -> ErrorView {
        return UINib(nibName: String(describing: type(of: self)), bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ErrorView
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setupView(customInset: UIEdgeInsets? = nil) {
        if let customInset = customInset {
            defaultInset = customInset
        }
        reloadView()
    }
    
    func reloadView() {
        errorTitle.text = "Opps".localized()
        errorLabel.text = "Something Went Wrong".localized()
        reloadButton.setTitle("Try Again".localized(), for: .normal)
    }
    
    @IBAction func reloadAction(_ sender: UIButton) {
        if let delegate = delegate {
            delegate.errorViewReloadTapped()
        }
    }
    
    // StatefulPlaceholderView Protocol
    func placeholderViewInsets() -> UIEdgeInsets {
        return defaultInset
    }
}

extension ErrorView: StatefulDataProvider {
    func didReceive(value: Any?) {
        if let error = value as? ServiceError {
            errorLabel.text = error.message
        } else if let error = value as? Error {
            errorLabel.text = error.localizedDescription
        } else {
            errorLabel.text = "something_went_wrong".localized()
        }
    }
}
