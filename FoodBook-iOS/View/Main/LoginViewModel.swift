//
//  LoginViewModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import SwiftyJSON
import ObjectMapper

class LoginViewModel: BaseViewModel {
    let countryList = CountryUtil.fetchCountryList()

    func login(mobile_no: String, password: String, completion: (() -> Void)?){
        
        UserModel.getUserFromUserDefault(username: mobile_no, password: password, completion: completion)
    }
}
