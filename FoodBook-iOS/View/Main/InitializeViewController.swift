//
//  InitializeViewController.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit

class InitializeViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarStatus(.hide)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
                
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if UserModel.getCurrentUser() != nil{
            AppDelegate.setRootToMainView()
        }else{
            AppDelegate.setRootToLoginView()
        }
    }
}
