//
//  CreateNewRecipeViewController.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import DKImagePickerController
import ActionSheetPicker_3_0

enum CrateNewRecipeViewControllerType{
    case new, edit
}

class CreateNewRecipeViewController: BaseViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var recipeTypeView: UIView!
    @IBOutlet weak var recipeTypeContainerView: UIView!
    @IBOutlet weak var recipeTypeTitleLbl: UILabel!
    @IBOutlet weak var recipeTypeTF: UITextField!
    
    @IBOutlet weak var recipeNameView: UIView!
    @IBOutlet weak var recipeNameTitleLbl: UILabel!
    @IBOutlet weak var recipeNameTF: UITextField!
    
    @IBOutlet weak var recipeDescView: UIView!
    @IBOutlet weak var recipeDescTitleLbl: UILabel!
    @IBOutlet weak var recipeDescTV: PlaceholderTextView!
    
    @IBOutlet weak var IngredientView: UIView!
    @IBOutlet weak var IngredientTitleLbl: UILabel!
    @IBOutlet weak var IngredientTV: PlaceholderTextView!
    
    @IBOutlet weak var preparationStepView: UIView!
    @IBOutlet weak var preparationStepTitleLbl: UILabel!
    @IBOutlet weak var preparationStepTV: PlaceholderTextView!
    
    @IBOutlet weak var RecipeAlbumView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var submitView: UIView!
    @IBOutlet weak var submitBtn: UIButton!
    
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var deleteBtn: UIButton!
    
    let viewModel = CreateNewRecipeViewModel()
    var recipeTypePicker = ActionSheetStringPicker()
    
    var controllerType: CrateNewRecipeViewControllerType = .new
    var model: RecipeModel? {
        didSet{
            self.viewModel.recipeData = model
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configRecipeTypePicker()
        recipeTypeView.addTapGesture(target: self, selector: #selector(recipeTypeDidTapped))
        collectionView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
        recipeDescTV.removeAllPadding()
        IngredientTV.removeAllPadding()
        preparationStepTV.removeAllPadding()
        
        switch controllerType{
        case .new:
            title = "Add New Recipe".localized()
            submitBtn.setTitle("Add", for: .normal)
            deleteView.isHidden = true
            
            break
        case .edit:
            title = "Edit Recipe".localized()
            submitBtn.setTitle("Save", for: .normal)
            
            if let data = viewModel.recipeData{
                recipeTypeTF.text = data.type
                recipeNameTF.text = data.name
                recipeDescTV.text = data.description
                IngredientTV.text = data.ingredient
                preparationStepTV.text = data.step
                viewModel.selectedAttachedMedia = data.images
            }
            
            break
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let targetView = object as? UICollectionView, targetView == collectionView{
            collectionViewHeightConstraint.constant = targetView.collectionViewLayout.collectionViewContentSize.height
        }
    }
    
    override func setupLocalized() {
    }
    
    //MARK: - Button Action
    @IBAction func submitBtnDidPressed(_ sender: Any) {
        if controllerType == .new{
            self.addRecipe()
        }else{
            self.updateRecipe()
        }
    }
    
    @IBAction func deleteBtnDidPressed(_ sender: Any) {
        AlertUtil.showAlert(withTitle: "Delete Recipe", message: "Are you sure to delete this recipe?", options: [("Cancel", .destructive), ("Confirm", .default)]) { index in
            
            if index == 1{
                self.deleteRecipe()
            }
        }
    }
    
    @objc func recipeTypeDidTapped(){
        recipeTypePicker.show()
    }
    
    //MARK: - Functions
    private func configRecipeTypePicker(){
        let typeList = XMLHelper.getRecipeType()
        
        recipeTypePicker = ActionSheetStringPicker(title: "Recipe's Type", rows: typeList, initialSelection: 0, doneBlock: { picker, index, data in
            if let str = data as? String{
                self.recipeTypeTF.text = str
            }
        }, cancel: { picker in
            
        }, origin: self.view)
        
        recipeTypePicker.tapDismissAction = .cancel
    }
    
    func validateData() -> (String, String, String, String)?{
        guard let type = recipeTypeTF.text, type.count > 0 else{
            AlertUtil.showError(with: "Please select your recipe's type", onToast: true)
            return nil
        }
        
        guard let name = recipeNameTF.text, name.count > 0 else {
            AlertUtil.showError(with: "Please enter your recipe's name", onToast: true)
            return nil
        }
        
        guard let desc = recipeDescTV.text, desc.count > 0 else {
            AlertUtil.showError(with: "Please enter your recipe's description", onToast: true)
            return nil
        }
        
        guard let ingredient = IngredientTV.text, ingredient.count > 0 else {
            AlertUtil.showError(with: "Please enter your recipe's ingredient", onToast: true)
            return nil
        }
        
        return (type, name, desc, ingredient)
    }
    
    func save(){
        UserModel.getCurrentUser()?.saveUserToUserDefault()
        NotificationCenter.default.post(name: .RefreshRecipeList, object: nil)
    }
    
    func addRecipe(){
        if let (type, name, desc, ingredient) = validateData(){
            let steps = preparationStepTV.text
            let newRecipe = RecipeModel(type: type, name: name, description: desc, ingredient: ingredient, step: steps, images: viewModel.selectedAttachedMedia)
            
            UserModel.getCurrentUser()?.recipeList.append(newRecipe)
            save()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.navigationController?.popViewController(animated: true)
                MessageManager.showMessage(message: "Recipe was added successfully", type: .STAlertSuccess)
            }
        }
    }
    
    func updateRecipe(){
        if let (type, name, desc, ingredient) = validateData(){
            let steps = preparationStepTV.text

            viewModel.recipeData?.update(type: type, name: name, description: desc, ingredient: ingredient, step: steps, images: viewModel.selectedAttachedMedia)
            save()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.navigationController?.popViewController(animated: true)
                MessageManager.showMessage(message: "Recipe was updated successfully", type: .STAlertSuccess)
            }
        }
    }
    
    func deleteRecipe(){
        if let obj = model{
            UserModel.getCurrentUser()?.recipeList.remove(obj)
//            UserModel.getCurrentUser()?.recipeList.removeAll(where: { $0 == obj })
            save()
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.navigationController?.popViewController(animated: true)
                MessageManager.showMessage(message: "Recipe was deleted successfully", type: .STAlertSuccess)
            }
        }
    }
}

extension CreateNewRecipeViewController: UIImagePickerControllerDelegate{
    fileprivate func presentImagePicker(hasCamera : Bool = true) {
        self.view.endEditing(true)
        
        let imagePicker = DKImagePickerController()
        imagePicker.sourceType = hasCamera ? .both : .photo
        imagePicker.assetType = .allPhotos
        imagePicker.maxSelectableCount = viewModel.maxUploadCount - viewModel.selectedAttachedMedia.count
        imagePicker.showsCancelButton = true
        imagePicker.navigationController?.navigationItem.setRightBarButton(nil, animated: true)
        
        imagePicker.didSelectAssets = { [weak self] (assets: [DKAsset]) in
            guard let self = self else {return}
            
            let newAssets = assets.map { AttachedMediaModel.init(asset: $0)}
            self.viewModel.selectedAttachedMedia = self.viewModel.selectedAttachedMedia + newAssets
            self.collectionView.reloadData()
        }
        
        present(imagePicker, animated: true, completion: nil)
    }
}

extension CreateNewRecipeViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(viewModel.maxUploadCount, self.viewModel.selectedAttachedMedia.count + 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCouponCollectionViewCell", for: indexPath) as! SelectRecipeImageCollectionViewCell
        
        cell.process(with: viewModel.selectedAttachedMedia[safe: indexPath.row], index: indexPath.row, removeBlock: { [weak self] index in
            guard let self = self else { return }
            
            if let imageObject = self.viewModel.selectedAttachedMedia[safe: indexPath.row] {
                AlertUtil.showAlert(withTitle: "Remove Image".localized(), message: "Are you sure to remove this image?".localized(), options: [("Cancel".localized(), style: .default), ("Confirm".localized(), style: .default)]) { selectionIndex in
                    
                    if selectionIndex == 1{
                        self.viewModel.selectedAttachedMedia.remove(at: index)
                        self.collectionView.reloadData()
                    }
                }
            }else{
                self.viewModel.selectedAttachedMedia.remove(at: index)
                self.collectionView.reloadData()
            }
        })
        
        return cell
    }
    
    
}
extension CreateNewRecipeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        view.layoutSubviews()
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let maxRowItem: CGFloat = 3.0
        let maxRowGap: CGFloat = maxRowItem - 1.0
        let width =  (collectionView.frame.size.width - (layout.minimumInteritemSpacing * maxRowGap))/maxRowItem
        
        return CGSize(width: width, height: width)
    }
}

extension CreateNewRecipeViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if viewModel.selectedAttachedMedia[safe: indexPath.row] == nil{
            self.presentImagePicker()
        }
    }
}
