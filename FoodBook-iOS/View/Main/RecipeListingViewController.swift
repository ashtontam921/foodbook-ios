//
//  RecipeListingViewController.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import ActionSheetPicker_3_0

class RecipeListingViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel = RecipeListingViewModel()
    var recipeTypePicker = ActionSheetStringPicker()
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: .RefreshRecipeList, object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(forName: .RefreshRecipeList, object: nil, queue: nil) { [weak self] (notification) in
            guard let self = self else { return }
            
            self.refreshData()
        }
        
        addNavBtns()
        configTypePicker()
        tableView.addRefreshControl(target: self, selector: #selector(didRefresh(refreshControl:)))
        
        setupViewModelBinding(with: viewModel)
        initStatefulView()
        refreshData()
    }
    
    override func setupLocalized() {
        title = "FoodBook's Recipe"
    }
    
    override func hasContent() -> Bool {
        return viewModel.dataArr.count > 0
    }
    
    override func errorViewReloadTapped() {
        startLoading()
        refreshData()
    }
    
    override func emptyViewReloadTapped() {
        startLoading()
        refreshData()
    }
    
    //MARK:- Function
    func addNavBtns(){
        //Add filter btn
        let filterBarBtn = UIBarButtonItem(image: UIImage(named: "Icon_Filter")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(filterBtnDidPressed))
        filterBarBtn.tintColor = .white
        
        //Add create recipe btn
        let createRecipeBarBtn = UIBarButtonItem(image: UIImage(named: "Icon_Add")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(addBtnDidPressed))
        filterBarBtn.tintColor = .white
        
        //Add right Nav btns
        self.navigationItem.setRightBarButtonItems([createRecipeBarBtn, filterBarBtn], animated: true)
        
        //Add Logout btn
        let logoutBarBtn = UIBarButtonItem(image: UIImage(named: "Icon_Logout")?.withRenderingMode(.alwaysTemplate), style: .done, target: self, action: #selector(logoutBtnDidPressed))
        logoutBarBtn.tintColor = .white
        
        //Add left Nav btn
        self.navigationItem.setLeftBarButton(logoutBarBtn, animated: true)
    }
    
    func configTypePicker(){
        var typeList = XMLHelper.getRecipeType()
        typeList.insert("All", at: 0)
        
        recipeTypePicker = ActionSheetStringPicker(title: "Recipe's Filter", rows: typeList, initialSelection: 0, doneBlock: { picker, index, data in
            if let str = data as? String{
                if index == 0{
                    self.viewModel.filter = ""
                }else{
                    self.viewModel.filter = str
                }
                
                self.refreshData()
            }
        }, cancel: { picker in
            
        }, origin: self.view)
        
        recipeTypePicker.tapDismissAction = .cancel
    }
    
    @objc func didRefresh(refreshControl: UIRefreshControl){
        refreshControl.endRefreshing()
        refreshData()
    }
    
    @objc func refreshData(isRefresh: Bool = true) {
        viewModel.getRecipeList(isRefresh: isRefresh) { [weak self] in
            guard let self = self else { return }
            
            self.endLoading()
            self.tableView.reloadData()
        }
    }
    
    @objc func filterBtnDidPressed() {
        recipeTypePicker.show()
    }
    
    @objc func addBtnDidPressed(){
        self.pushControllerWith(storyBoardID: "Main", type: CreateNewRecipeViewController.self){ (vc) in
            vc?.controllerType = .new
        }
    }
    
    @objc func logoutBtnDidPressed(){
        AlertUtil.showAlert(withTitle: "Logout", message: "Are you sure to logout?", options: [("Cancel", .destructive), ("Confirm", .default)]) { index in
            
            if index == 1{
                AppDelegate.setRootToLoginView()
            }
        }
    }
}

extension RecipeListingViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecipeListingCell
        
        if let model = viewModel.dataArr[safe: indexPath.row]{
            cell.process(model: model) { [weak self] in
                guard let self = self else { return }
                
                self.pushControllerWith(storyBoardID: "Main", type: CreateNewRecipeViewController.self) { vc in
                    vc?.controllerType = .edit
                    vc?.model = model
                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let model = viewModel.dataArr[safe: indexPath.row]{
            self.pushControllerWith(storyBoardID: "Main", type: RecipeDetailViewController.self){(vc) in
                vc?.model = model
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
}
