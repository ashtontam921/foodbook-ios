//
//  MyCouponCell.swift
//  MKWallet
//
//  Created by Wong Wei Yao on 14/06/2021.
//  Copyright © 2021 AIO Synergy Sdn Bhd. All rights reserved.
//

import UIKit

class RecipeListingCell: UITableViewCell {
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    
    private var editCompletion: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
//        dividerView.drawLine(at: .top, width: 1.0, color: .dividerColor, dashedPattern: [3.3])
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func process(model: RecipeModel, editCompletion: (() -> Void)?){
        imgView.image = model.images.first?.image ?? UIImage(named: "Image_Placeholder")
        titleLbl.text = model.name
        descLbl.text = model.description
        typeLbl.text = model.type
        self.editCompletion = editCompletion
    }
    
    @IBAction func editBtnDidPressed(_ sender: Any) {
        editCompletion?()
    }
}

