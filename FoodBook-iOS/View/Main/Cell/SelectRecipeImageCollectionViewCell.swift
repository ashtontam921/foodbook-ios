//
//  ImageCouponCollectionViewCell.swift
//  MKWallet
//
//  Created by Wong Wei Yao on 05/07/2021.
//  Copyright © 2021 AIO Synergy Sdn Bhd. All rights reserved.
//

import UIKit

class SelectRecipeImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var contentViewImage: UIView!
    @IBOutlet weak var imgViewCoupon: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var add_image: UIImageView!
    
    private var removeBlock: ((Int) -> Void)?
    private var index = 0
    
    func process(with mediaModel: AttachedMediaModel?, index: Int, removeBlock: @escaping ((Int) -> Void)){
        self.index = index
        self.removeBlock = removeBlock
        
        add_image.image = UIImage(named: "Icon_Image_Add")
        imgViewCoupon.image = nil
        addView.isHidden = false
        removeButton.isHidden = mediaModel == nil
        
        if let image = mediaModel?.image {
            imgViewCoupon.image = image
            addView.isHidden = true
            
        }else if let originalAsset = mediaModel?.asset?.originalAsset {
            switch originalAsset.mediaType {
            case .image:
                mediaModel?.asset?.fetchOriginalImage(completeBlock: { image, info in
                    if let image = image {
                        mediaModel?.image = image
                        self.imgViewCoupon.image = image
                    }
                })
                
                addView.isHidden = true
                
                break
            default:
                break
            }
        }        
    }
        
    //MARK: -IBAction
    @IBAction func removeAction(_ sender: Any) {
        removeBlock?(index)
    }
}
