//
//  CouponDetailImageCollectionViewCell.swift
//  MKWallet
//
//  Created by Wong Wei Yao on 15/06/2021.
//  Copyright © 2021 AIO Synergy Sdn Bhd. All rights reserved.
//

import UIKit

class RecipeImageCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imgView: UIImageView!
    
    func process(with model: AttachedMediaModel?){
        imgView.image = model?.image ?? UIImage(named: "Image_Placeholder")
    }
}
