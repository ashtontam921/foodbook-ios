//
//  RecipiListingViewModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit

class RecipeListingViewModel: BaseViewModel {

    var dataArr: [RecipeModel] = []
    var filter = ""
    
    func getRecipeList(isRefresh: Bool, completion: (() -> Void)?){
        dataArr = UserModel.getCurrentUser()?.recipeList ?? []
        
        //Apply filter, if any
        if filter.isEmpty == false{
            dataArr = dataArr.filter({ $0.type == filter})
        }
        
        completion?()
    }
}
