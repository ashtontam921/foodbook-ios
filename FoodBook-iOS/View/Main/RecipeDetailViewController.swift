//
//  CouponDetailsViewController.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import SKPhotoBrowser

class RecipeDetailViewController: BaseViewController {
    @IBOutlet weak var headerContentView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var barLineView: UIView!
    @IBOutlet weak var barView: UIView!
    @IBOutlet weak var headTitle: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
        
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnPhotos: UIButton!
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var typeLbl: UILabel!
    
    @IBOutlet weak var ingredientTitleLbl: UILabel!
    @IBOutlet weak var ingredientDescLbl: UILabel!
    
    @IBOutlet weak var preparationStepStackView: UIStackView!
    @IBOutlet weak var preparationStepTitleLbl: UILabel!
    @IBOutlet weak var preparationStepDescLbl: UILabel!
    
    fileprivate var selectedImageIndex: Int = 0
    
    var viewModel = RecipeDetailViewModel()
    var model: RecipeModel? {
        didSet{
            self.viewModel.recipeData = model
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarStatus(.hide)
        self.headerContentView.backgroundColor = .clear
        self.headTitle.textColor = .clear
        self.backButton.tintColor = .white
        
        backButton.addTarget(self, action: #selector(back), for: .touchUpInside)
        
        setupViewModelBinding(with: viewModel)
        initStatefulView()
        getData()
    }
    
    override func setupLocalized() {
        
    }
    
    override func hasContent() -> Bool {
        return viewModel.recipeData != nil
    }
    
    override func errorViewReloadTapped() {
        startLoading()
        getData()
    }
    
    override func emptyViewReloadTapped() {
        startLoading()
        getData()
    }
    
    
    @IBAction func btnPhotoDidPressed(_ sender: Any) {
        self.showImageBrowser()
    }
    
    func getData(){
        //Call api if neccesary
        
        //Set data
        if let data = viewModel.recipeData{
            headTitle.text = data.name
            titleLbl.text = data.name
            descLbl.text = data.description
            typeLbl.text = data.type
            ingredientDescLbl.text = data.ingredient
            preparationStepDescLbl.text = data.step
            preparationStepStackView.isHidden = preparationStepDescLbl.text?.isEmpty ?? true
            btnPhotos.setTitle(String(format: "%d photos", data.images.count), for: .normal)
            btnPhotos.isHidden = data.images.count <= 1
        }
    }
    
    @objc func back() {
        navigationController?.popViewController(animated: true)
    }
}

extension RecipeDetailViewController {
    private func get_visible_size_top() -> CGSize{
        var result:CGSize!
        var size:CGSize!

        size = UIApplication.shared.statusBarFrame.size
        result = size

        if(navigationController != nil){
            size = navigationController?.navigationBar.frame.size
            result.height = result.height + size.height
        }
        return result
    }

    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == scrollView{
            if scrollView.contentOffset.y >= (collectionView.frame.size.height - get_visible_size_top().height){
                UIView.animate(withDuration: 0.3, animations: {
                    self.headerContentView.backgroundColor = .mainColor
                    self.headTitle.textColor = .white
                })
            }else{
                UIView.animate(withDuration: 0.3, animations: {
                    self.headerContentView.backgroundColor = .clear
                    self.headTitle.textColor = .clear
                    self.backButton.tintColor = .white
                })
            }
            self.setNeedsStatusBarAppearanceUpdate()
        }
    }
}

extension RecipeDetailViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.recipeData?.images.count == 0 ? 1 : viewModel.recipeData?.images.count ?? 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! RecipeImageCollectionViewCell
        
        cell.process(with: self.viewModel.recipeData?.images[safe: indexPath.row])
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        for cell in collectionView.visibleCells {
            if let indexPath = collectionView.indexPath(for: cell){
                selectedImageIndex = indexPath.row
            }
        }
    }
}

extension RecipeDetailViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let model = viewModel.recipeData, model.images.count > 0 else { return }
        
        selectedImageIndex = indexPath.row
        showImageBrowser()
    }
}

extension RecipeDetailViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
}

extension RecipeDetailViewController: SKPhotoBrowserDelegate {
    func showImageBrowser(){
        guard let model = viewModel.recipeData, model.images.count > 0 else { return }
        var images: [SKPhoto] = []
        for imgModel in model.images {
            let img = imgModel.image ?? UIImage(named: "Image_Placeholder")!
            let skPhoto = SKPhoto.photoWithImage(img)
            
            images.append(skPhoto)
        }
        
        let browser =  SKPhotoBrowser(photos: images, initialPageIndex: selectedImageIndex)
        browser.delegate = self
        present(browser, animated: true, completion: nil)
    }
    
    func didScrollToIndex(_ browser: SKPhotoBrowser, index: Int) {
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .left, animated: false)
        selectedImageIndex = index
    }
    
    func didShowPhotoAtIndex(_ browser: SKPhotoBrowser, index: Int) {
        backButton.isHidden = true
    }
    
    func didDismissAtPageIndex(_ index: Int) {
        backButton.isHidden = false
    }
}

