//
//  LoginViewController.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import TTTAttributedLabel
import Localize_Swift

class LoginViewController: BaseViewController {
    @IBOutlet weak var countryCodeContentView: UIView!
    @IBOutlet weak var countryImageView: UIImageView!
    @IBOutlet weak var countryCodeLabel: UILabel!
    @IBOutlet weak var phoneNumberTitleLbl: UILabel!
    @IBOutlet weak var phoneNumberTextField: TintTextField!
    @IBOutlet weak var passwordTitlebl: UILabel!
    @IBOutlet weak var passwordTextField: TintTextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    fileprivate let viewModel = LoginViewModel()
    fileprivate var selectedCountryIndex: Int = -1
    
    var mobileNo: String{
        return ((self.countryCodeLabel.text ?? "") + (self.phoneNumberTextField.text ?? "")).replacingOccurrences(of: "+", with: "")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        countryCodeContentView.addTapGesture(target: self, selector: #selector(chooseCountryCode(_:)))
        phoneNumberTextField.delegate = self
        passwordTextField.delegate = self

        if selectedCountryIndex == -1 {
            if let countryCode = NSLocale.current.regionCode, let index = CountryUtil.getCountryIndex(by: countryCode, from: viewModel.countryList) {
                selectedCountryIndex = index
            } else if let index = CountryUtil.getCountryIndex(by: Global.defaultCountryCode, from: viewModel.countryList) {
                selectedCountryIndex = index
            } else {
                selectedCountryIndex = 0 // Fallback
            }
        }
        
        if debug == true{
            phoneNumberTextField.text = "123456789"
            passwordTextField.text = "123456"
        }
        
        countryCodeLabel.text = "+" + (viewModel.countryList[safe: selectedCountryIndex]?.num ?? "1")
        countryImageView.image = UIImage(named: "\(viewModel.countryList[safe: selectedCountryIndex]?.code.lowercased() ?? "")_flag")
        setupViewModelBinding(with: viewModel)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setNavigationBarStatus(.transparent)
    }
    
    override func setupLocalized() {
        
    }
    
    //MARK:- IBAction
    @objc func chooseCountryCode(_ sender: Any) {
        let controller = CountryCodePickerViewController()
        controller.initialIndex = selectedCountryIndex
        controller.groupedCountryList = CountryUtil.getGroupedCountryList(with: CountryUtil.fetchCountryList())
        controller.delegate = self
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func login(_ sender: Any) {
        view.endEditing(true)
        
        guard let countryCode = countryCodeLabel.text, let phoneNumber = phoneNumberTextField.text, phoneNumber.count >= 6 else{
            AlertUtil.showError(with: "Invalid phone number".localized(), onToast: true)
            return
        }
        
        guard let password = passwordTextField.text, password.count >= 6 else {
            AlertUtil.showError(with: "Invalid password".localized(), onToast: true)
            return
        }
        
        let fullPhoneNumber = (countryCode + phoneNumber.removeFrontZero()).replacingOccurrences(of: " ", with: "").replacingOccurrences(of: "+", with: "")
        
        viewModel.login(mobile_no: fullPhoneNumber, password: password) { [weak self] in
            guard let self = self else { return }
            
            UserModel.shared?.saveUserToUserDefault()
            AppDelegate.setRootToMainView()
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == phoneNumberTextField{
            passwordTextField.becomeFirstResponder()
        }else if textField == passwordTextField{
            login(loginBtn!)
        }
        
        return true
    }
}

extension LoginViewController: CountryCodePickerDelegate {
    func didSelectCountry(_ picker: CountryCodePickerViewController, country: CountryTuple) {
        if let index = viewModel.countryList.firstIndex(where: {$0.code == country.code}) {
            selectedCountryIndex = index
            countryCodeLabel.text = "+" + country.num
            countryImageView.image = UIImage(named: "\(country.code.lowercased())_flag")
        }
    }
}
