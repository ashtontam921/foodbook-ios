//
//  CreateNewRecipeViewModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import ObjectMapper
import Localize_Swift

class CreateNewRecipeViewModel: BaseViewModel {
    var recipeData: RecipeModel?
    var selectedAttachedMedia: [AttachedMediaModel] = []

    var maxUploadCount: Int = 5
    
    func createCoupon(title: String?,quantity: Int?, mkt_amount: Double?,type: String?,start_date: Date?,start_time: Date?,end_date:Date?,end_time: Date?,redemption_start: Date?,redemption_end: Date?,desc: String?,tnc: String?, attachedMediaList: [AttachedMediaModel], completion: @escaping (() -> ())){
        
    }
    
    func updateCoupon(coupon_id: Int?,title: String?,quantity: Int?, mkt_amount: Double?,type: String?,start_date: Date?,start_time: Date?,end_date:Date?,end_time: Date?,redemption_start: Date?,redemption_end: Date?,desc: String?,tnc: String?,activate: Bool?, attachedMediaList: [AttachedMediaModel],removedRefIDList: [Int],completion: @escaping (() -> ())){
        
    }
}
