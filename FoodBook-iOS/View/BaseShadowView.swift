
import UIKit

class BaseShadowView: UIView {
    @IBInspectable var shadowColor: UIColor = .black
    @IBInspectable var opacity: Float = 0.15
    
    override func layoutSubviews() {
        super.layoutSubviews()
        addShadow(opacity: opacity, shadowColor: shadowColor)
        backgroundColor = .clear
    }

}
