import Foundation
import Alamofire
import SwiftyJSON
import Localize_Swift

enum Router: URLRequestConvertible{
    
    case FoodBook(api: APIPathProtocol)
    case FoodBookUpload(api: APIPathProtocol)
    
    func asURLRequest() throws -> URLRequest {
        switch self{
        case .FoodBook(let api):
            
            let url = try API.SERVER.getServerPath().asURL()
            
            var urlRequest = URLRequest(url: url.appendingPathComponent(api.path))
            urlRequest.httpMethod = api.method.rawValue
            
            var param = api.param
            if Localize.currentLanguage().contains("zh"){
                param["lang"] = "cn"
            }else{
                param["lang"] = "en"
            }
            
            urlRequest = try URLEncoding.default.encode(urlRequest, with: param)
            
            return urlRequest
        case .FoodBookUpload(let api):
            let url = try API.SERVER.getServerPath().asURL()
            
            var urlRequest = URLRequest(url: url.appendingPathComponent(api.path))
            urlRequest.httpMethod = api.method.rawValue
            
            if let token = UserModel.getUserToken(){
                urlRequest.setValue("Bearer \(token)", forHTTPHeaderField: "Authorization")
                print("\n\nUser token = \(token)\n\n")
            }
            
            switch Localize.currentLanguage(){
            case "zh-Hans":
                urlRequest.setValue("zh", forHTTPHeaderField: "Content-Language")
            default:
                urlRequest.setValue(Localize.currentLanguage(), forHTTPHeaderField: "Content-Language")
            }
            
            if api.body.count > 0{
                let data = api.body.data(using: .utf8, allowLossyConversion: false)!
                
                if urlRequest.value(forHTTPHeaderField: "Content-Type") == nil {
                    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
                }
                let json = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                if let json = json {
                    print(json)
                }
                
                urlRequest.httpBody = data
            }
            
            return try URLEncoding.default.encode(urlRequest, with: api.param)
        }
    }
}

protocol APIPathProtocol {
    var path: String { get }
    var param: Parameters { get }
    var body: String { get }
    var method: HTTPMethod { get }
}

extension APIPathProtocol{
    var body: String{
        return ""
    }
}
