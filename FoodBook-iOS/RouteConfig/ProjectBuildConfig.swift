
import Foundation

struct Global {
    static let defaultCountryCode = "MY"
}

//MARK: -For multiple environment
struct AppBuild {
    static func isDemo() -> Bool{
        let bundleIdentifer:String = Bundle.main.bundleIdentifier!
        return bundleIdentifer.range(of: ".demo") != nil
    }
    
    static func isDev() -> Bool{
        let bundleIdentifer:String = Bundle.main.bundleIdentifier!
        return bundleIdentifer.range(of: ".dev") != nil
    }
    
    static func isLive() -> Bool{
        return !isDemo() && !isDev()
    }
}

//MARK: -API base on different environment
struct API {
    struct SERVER {
        struct Live {
            static let Url = ""
        }
        
        struct Demo {
            static let Url = ""
        }
        
        struct Dev {
            static let Url = ""
        }
        
        static func getServerPath(isAPI: Bool = true) -> String{
            let path = isAPI ? "/api/v1" : ""
            
            if(AppBuild.isDemo()){
                return API.SERVER.Demo.Url + path
            }else{
                var serverPath = AppBuild.isLive() ? API.SERVER.Live.Url : API.SERVER.Dev.Url
                if(AppBuild.isDev()){
                    serverPath = API.SERVER.Dev.Url
                }
                return serverPath + path
            }
        }
        
    }
}

//MARK: - Constant
let debug = true
