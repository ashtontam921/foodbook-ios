import Foundation
import SwiftyJSON
import Alamofire
import ObjectMapper

enum ServiceError: Error {
    case networkError(code: Int, message: String)
    case unknownError(code: Int, message: String)
    
    var message: String{
        switch self {
        case .networkError(_, let message):
            return message
        case .unknownError(_, let message):
            return message
        }
    }
}

enum Result<T> {
    case success(T)
    case error(ServiceError)
}

protocol Client{
    static func request(_ route: URLRequestConvertible, completion: @escaping (Result<JSON?>) -> ())
    static func request(_ route: URLRequestConvertible, params: Parameters, method: HTTPMethod, requestUploadObject: [(fileParam: String, fileName: String, data: Data)], completion: @escaping (Result<JSON?>) -> ())
}

class RequestClient: Client{    
    class func request(_ route: URLRequestConvertible, completion: @escaping (Result<JSON?>) -> ()){
        AF.request(route)
//            .validate()
            .responseJSON { (response) in
                switch response.result{
                case .success(let data):
                    let json = JSON(data)
                    print("request url = \(route.urlRequest?.url?.absoluteString ?? "")")
                    self.checkError(json, statusCode: response.response?.statusCode ?? 0, completion: { (isSuccess, error) in
                        if isSuccess{
                            completion(.success(json))
                        }else if let error = error{
                            completion(.error(error))
                        }
                    })
                case .failure(let error):
                    if ((error as NSError?)?.code ?? 0) != -999{
                        let errorMessage = error.localizedDescription
                        let error = ServiceError.unknownError(code: -1, message: errorMessage)
                        completion(.error(error))
                    }
                }
        }
            .responseString { (string) in
                print("responseString = \(string)")
        }
    }
    
    class func request(_ route: URLRequestConvertible, params: Parameters, method: HTTPMethod, requestUploadObject: [(fileParam: String, fileName: String, data: Data)], completion: @escaping (Result<JSON?>) -> ()) {
        let headers: HTTPHeaders = route.urlRequest?.headers ?? []
        
        let toUrl: String = (route.urlRequest?.url?.absoluteString)!
        
        AF.upload(multipartFormData: { (multipartFormData) in
            for data in requestUploadObject {
                multipartFormData.append(data.data, withName: data.fileParam, fileName: data.fileName, mimeType: data.fileName.mimeType())
            }
            for (key, value) in params {
                if value is String {
                    multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                } else if value is Int {
                    multipartFormData.append("\(value)".data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))!, withName: key)
                }
            }
        }, to: toUrl, method: method, headers: headers)
        .response { (response) in
                switch response.result{
                case .success(let data):
                    guard let data = data else {
                        let errorMessage = "unexpected_error_message".localized()
                        let error = ServiceError.unknownError(code: -1, message: errorMessage)
                        completion(.error(error))
                        return
                    }
                    let json = JSON(data)
                    print("request url = \(route.urlRequest?.url?.absoluteString ?? "")")
                    self.checkError(json, statusCode: response.response?.statusCode ?? 0, completion: { (isSuccess, error) in
                        if isSuccess{
                            completion(.success(json))
                        }else if let error = error{
                            completion(.error(error))
                        }
                    })
                case .failure(let error):
                    if ((error as NSError?)?.code ?? 0) != -999{
                        let errorMessage = error.localizedDescription
                        let error = ServiceError.unknownError(code: -1, message: errorMessage)
                        completion(.error(error))
                    }
                }
        }
    }
    
    private class func checkError(_ json: JSON, statusCode: Int, completion: (Bool, ServiceError?) -> ()){
        if let topViewController = UIApplication.topViewController(){
            if statusCode == 401 {
//                AppDelegate.setRootToLoginView(error: "session_expired".localized())
            }else if statusCode != 200{
                var message: String = "unexpected_error_message".localized()
                if let result = json.dictionaryObject?["message"] as? String{
                    message = result
                }
                
                completion(false, ServiceError.unknownError(code: statusCode, message: message))
            }else{
                //To check return result
                if let data = json.rawString(), let object = Mapper<BaseModel>().map(JSONString: data), let result = object.result{
                    if object.message_code == "0007"{
                        AppDelegate.setRootToLoginView(error: "session_expired".localized())
                        return
                    }
                    
                    if result == false{
                        if object.message.isEmpty == true{
                            completion(false, ServiceError.unknownError(code: statusCode, message: "unexpected_error_message".localized()))
                        }else{
                            var msg = object.message
                            
                            if let extraMsg = object.errors.first?.error{
                                msg = msg + " - " + extraMsg
                            }
                            
                            completion(false, ServiceError.unknownError(code: statusCode, message: msg))
                        }
                        
                        return
                    }
                }
                
                completion (true, nil)
            }
        }else{
            if statusCode != 200{
                var message: String = "unexpected_error_message".localized()
                if let result = json.dictionaryObject?["message"] as? String{
                    message = result
                }
                
                completion(false, ServiceError.unknownError(code: statusCode, message: message))
            }else{
                completion (true, nil)
            }
        }
    }
    
    class func stopAllSessions() {
        AF.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }
}
