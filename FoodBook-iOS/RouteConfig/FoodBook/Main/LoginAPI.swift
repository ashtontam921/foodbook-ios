import UIKit
import Alamofire
import Localize_Swift

enum LoginAPI: APIPathProtocol{
    case login(mobile_no: String, password: String)
    
    var path: String{
        switch self {
        case .login:
            return "login"
        }
    }
    
    var param: Parameters{
        var param:Parameters = [:]
        
        switch  self {
        case .login(let mobile_no, let password):
            param["mobile_no"] = mobile_no
            param["password"] = password
            param["language"] = getLanguageCode()
            
            break
        }
        
        return param
    }
    
    var method: HTTPMethod {
        switch  self {
        case .login:
            return .post
        default:
            return .get
        }
    }
}

extension APIPathProtocol{
    func getLanguageCode(language: String? = nil) -> String{
        let currentLanguage = language ?? Localize.currentLanguage()
        
        if currentLanguage.lowercased().contains("han") || currentLanguage.lowercased().contains("zh"){
            return "cn"
        }else{
            return "en"
        }
    }
}
