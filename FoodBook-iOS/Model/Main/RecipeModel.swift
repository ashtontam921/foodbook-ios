//
//  RecipeModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 06/08/2021.
//

import Foundation
import ObjectMapper
import SwiftyJSON

class RecipeModel: Mappable{
    var type = ""
    var name = ""
    var description = ""
    var ingredient = ""
    var step: String?
    var images: [AttachedMediaModel] = []
    
    //For image keeping
    var userStorageKey = ""
    var index = 0
    var storageKey: String{
        return (userStorageKey + "Image\(index)").sha256()
    }
        
    init(){
    }
    
    init(type: String, name: String, description: String, ingredient: String, step: String?, images: [AttachedMediaModel]) {
        self.type = type
        self.name = name
        self.description = description
        self.ingredient = ingredient
        self.step = step
        self.images = images
    }
    
    required init(map: Map) {
    }
    
    func update(type: String, name: String, description: String, ingredient: String, step: String?, images: [AttachedMediaModel]){
        self.type = type
        self.name = name
        self.description = description
        self.ingredient = ingredient
        self.step = step
        self.images = images
    }
    
    func mapping(map: Map) {
        //For API mapping
    }
        
    init(dictionary: Dictionary<String, Any>) {
        type = dictionary["type"] as? String ?? ""
        name = dictionary["name"] as? String ?? ""
        description = dictionary["description"] as? String ?? ""
        ingredient = dictionary["ingredient"] as? String ?? ""
        step = dictionary["step"] as? String ?? ""
        userStorageKey = dictionary["userStorageKey"] as? String ?? ""
        index = dictionary["index"] as? Int ?? 0
        
        getImageWithPath()
    }
    
    func encode() -> Dictionary<String, Any> {
        var dictionary : Dictionary = Dictionary<String, Any>()
        
        dictionary["type"] = type
        dictionary["name"] = name
        dictionary["description"] = description
        dictionary["ingredient"] = ingredient
        dictionary["step"] = step
        dictionary["userStorageKey"] = userStorageKey
        dictionary["index"] = index
        
        saveImageLocally()
        
        return dictionary
    }
    
    func getImageWithPath(){
        self.images.removeAll()
        
        if let imgDirArr = UserDefaults.standard.object(forKey: storageKey) as? [String] {
            for url in imgDirArr{
                let imgModel = AttachedMediaModel(fileURL: url)
                
                self.images.append(imgModel)
            }
        }
    }
    
    func saveImageLocally(){
        var paths: [String] = []
        
        //Write image to local directory
        for imgModel in images{
            if let imgData = imgModel.data, let fileURL = imgModel.fileURL, let url = URL(string: fileURL){
                //Store only if is new image
                if imgModel.hasStored == false{
                    //Checks if file exists, removes it if so for double protection
                    if FileManager.default.fileExists(atPath: url.path) {
                        do {
                            try FileManager.default.removeItem(atPath: url.path)
                        } catch let removeError {
                            print("Couldn't remove file at path with error: \(removeError.localizedDescription)")
                        }
                    }
                    
                    do {
                        try imgData.write(to: url)
                        
                        imgModel.hasStored = true
                        paths.append(fileURL)
                    } catch let error{
                        print("Error: \(error.localizedDescription)")
                    }
                }else{
                    paths.append(fileURL)
                }
            }
        }
        
        UserDefaults.standard.set(paths, forKey: storageKey)
        UserDefaults.standard.synchronize()
    }
}

extension RecipeModel: Hashable{
    static func == (lhs: RecipeModel, rhs: RecipeModel) -> Bool {
        return lhs.type == rhs.type && lhs.name == rhs.name && lhs.description == rhs.description && lhs.ingredient == rhs.ingredient && lhs.step == rhs.step && lhs.images == rhs.images;
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
        hasher.combine(name)
        hasher.combine(description)
        hasher.combine(ingredient)
        hasher.combine(step)
        hasher.combine(images)
    }
}

extension RecipeModel{
    class func sampleDish1() -> RecipeModel{
        let recipe = RecipeModel()
        recipe.type = "Main Dishes"
        recipe.name = "GRILLED BASIL CHICKEN"
        recipe.description = "Tart, fresh marinade livens up simple grilled chicken."
        recipe.ingredient = "¾ cup balsamic vinegar. \n¼ cup tightly packed fresh basil leaves, orange quote icon gently rub produce under cold running water. \n2 tbsp olive oil. \n1 garlic clove, minced. \n½ tsp salt. \n4 plum tomatoes, orange quote icon scrubbed with clean vegetable brush under running water. \n4 boneless skinless chicken breast halves (4 ounces each)"
        recipe.step = "Wash hands with soap and water. In large skillet over medium high-heat, warm 2 tablespoons olive oil. Season leg quarters with ½ teaspoon salt and ½ teaspoon black pepper; orange quote icon do not rinse raw poultry. \n\nAdd leg quarters to pan, skin-side down. orange quote icon Wash hands with soap and water after handling uncooked chicken. Brown chicken, turning once, 8 to 10 minutes per side. Remove chicken to plate and drain off all but 2 tablespoons oil. Add eggplant to hot pan and cook, stirring, 5 minutes. Add remaining tablespoon olive oil, along with zucchini, onion and garlic. Cook 5 minutes, stirring occasionally. \n\nRaise heat to high and add white wine to pan, stirring to scrape up any browned bits. Add all other ingredients, except parsley, and place chicken legs in mixture. Bring to boil, reduce heat to medium-low, cover and simmer for 35 to 40 minutes or until cooked through and orange quote icon internal temperature reaches 165 °F on food thermometer. To serve, place some of the eggplant ragout onto plates, top with a leg quarter and sprinkle with parsley."
        recipe.images = [AttachedMediaModel(image: UIImage(named: "Sample_1")!)]
        
        return recipe
    }
    
    class func sampleDish2() -> RecipeModel{
        let recipe = RecipeModel()
        recipe.type = "Main Dishes"
        recipe.name = "MISO MARINATED SHORT RIBS"
        recipe.description = "Build big flavor in a short amount of time by combining a few ready-made sauces."
        recipe.ingredient = "3 tbsp red or yellow miso. \n3 tbsp mayonnaise. \n3 tbsp (or more) unseasoned rice vinegar, divided. \n1 ½ lbs thin cross-cut bone-in short ribs(flank-style). \nKosher salt.\n 1 bunch small radishes, orange quote icon scrubbed with clean vegetable brush under running water. \n4 tsp vegetable oil. \n2 cups steamed rice. \n1 bunch watercress, tough stems removed and orange quote icon gently rubbed under cold running water. \nSriracha or hot sauce (for serving)"
        recipe.step = "Wash hands with soap and water. In small bowl, stir together miso, mayonnaise, and 2 tbsp vinegar. Place ribs on a rimmed baking sheet and season lightly with salt; orange quote icon do not rinse raw meat. Pour all but 2 tablespoons miso mixture over ribs; turn to coat. Set remaining miso mixture aside.\n\nThinly slice radishes and toss in a medium bowl with remaining 1 tbsp vinegar. Season with salt and toss again.\n\nHeat a large heavy skillet, preferably cast iron, over medium-high. When pan is hot, add 2 tsp. oil and swirl to coat. Remove ribs from marinade;  orange quote icon do not reuse marinade used on raw meat. \n\nOrange quote icon Wash hands with soap and water after handling uncooked meat. Add half of ribs to pan and cook without turning until underside is very dark brown and charred in spots, about 2 minutes. Turn ribs and cook until second side is golden brown, 1–2 minutes, and orange quote icon internal temperature reaches 145 °F on food thermometer. Transfer to a plate. Repeat with remaining 2 tsp. oil and remaining ribs. Let rest 5 minutes.\n\nSeason reserved miso mixture with Sriracha; adjust to taste. Add additional vinegar, if desired. Cut ribs crosswise into 2 or 3 pieces; serve over bowls of rice, topped with watercress and drained radishes and drizzled with spicy sauce."
        recipe.images = [AttachedMediaModel(image: UIImage(named: "Sample_2")!)]
        
        return recipe
    }
    
    class func sampleDish3() -> RecipeModel{
        let recipe = RecipeModel()
        recipe.type = "Main Dishes"
        recipe.name = "CHICKEN FLORENTINE STRATA"
        recipe.description = "This healthy and budget friendly recipe can be served for breakfast, brunch or dinner and can be prepared the night before which will make it one of your go-to favourites!"
        recipe.ingredient = "1½ lbs boneless, skinless chicken breast halves, sliced into thin strips. \n2 tbsp olive oil. \n2 shallots, orange quote icon scrubbed with clean vegetable brush under running water and minced. \n1 tsp salt. \n½ tsp black pepper. \n½ tsp cayenne pepper. \n10 slices white bread, cut into cubes. \n1 (16 oz) package frozen spinach, thawed and well-drained. \n1 cup sundried tomatoes, drained and chopped. \n2 cups grated Monterey Jack cheese. \n6 eggs. \n2 cups whole milk. \n2 tsp Dijon mustard. \n¼ tsp nutmeg"
        recipe.step = "Wash hands with soap and water.  Lightly grease a 3-quart casserole dish and set aside.\n\nIn large skillet over medium-high heat, warm oil. Add shallots and sauté 1 minute. Add chicken strips and cook 5 minutes more.  orange quote icon Wash hands with soap and water after handling raw chicken.\n\nPlace a layer of bread cubes in the bottom of the dish. In alternating layers, add chicken, spinach, sundried tomatoes, cheese and bread, ending with a sprinkling of bread on top.\n\nIn a large bowl, combine eggs, milk, mustard and nutmeg with a whisk.  orange quote icon Wash hands after touching raw eggs. Pour mixture over the casserole, cover and refrigerate on lowest shelf 4 hours or overnight.  orange quote icon Wash counter after touching eggs.\n\nPreheat oven to 350°F. While oven heats, remove casserole from refrigerator. Place casserole in oven and bake 35—40 minutes, until golden brown on top.  orange quote icon Casserole is done with chicken reaches 165 °F as measured with a food thermometer. Remove from oven and cool 5 minutes before serving. Serve hot."
        recipe.images = [AttachedMediaModel(image: UIImage(named: "Sample_3")!)]
        
        return recipe
    }
}

