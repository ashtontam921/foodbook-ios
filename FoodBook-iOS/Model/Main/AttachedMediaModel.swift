//
//  AttachedMediaModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 05/08/2021.
//

import UIKit
import DKImagePickerController
import Photos

class AttachedMediaModel: NSObject{
    var asset: DKAsset?
    var data: Data?
    var image: UIImage?
    var fileURL: String?
    var fileName: String?
    var hasStored = false
    
    convenience init(asset: DKAsset) {
        self.init()
        self.asset = asset
        if let originalAsset = asset.originalAsset {
            if let fileName = asset.localIdentifier.split(separator: "/").first{
                self.fileName = fileName + (originalAsset.mediaType == .image ? ".png" : ".mov")
            }
            
            if let docPath = documentDirectoryPath()?.absoluteString{
                self.fileURL = docPath + (self.fileName ?? "content_\(Date().timeIntervalSince1970)\(originalAsset.mediaType == .image ? ".png" : ".mov")")
            }
            
            switch originalAsset.mediaType {
            case .image:
                asset.fetchOriginalImage(completeBlock: { image, info in
                    self.image = image
                    
                    if let image = image {
                        self.data = image.jpegData(compressionQuality: 0.2)
                    }
                })
            default:
                break
            }
        }
    }
    
    convenience init(image: UIImage) {
        self.init()

        self.image = image
        self.data = image.jpegData(compressionQuality: 0.2)
        self.fileName = "content_\(Date().timeIntervalSince1970).\(self.data?.imageFormat.rawValue ?? "png")"
        
        if let docPath = documentDirectoryPath()?.absoluteString{
            self.fileURL = docPath + self.fileName!
        }
    }
    
    convenience init(fileURL: String) {
        self.init()
        self.fileName =  URL(string: fileURL)?.lastPathComponent ?? ""
        self.hasStored = true

        if let docPath = documentDirectoryPath()?.absoluteString{
            self.fileURL = docPath + self.fileName!
        }
        
        if let url = URL(string: self.fileURL ?? ""){
            do {
                self.data = try Data(contentsOf: url)
                
                if let imgData = self.data{
                    self.image = UIImage(data: imgData)
                }
            } catch let error{
                print("Error: \(error.localizedDescription)")
            }
        }
    }
    
    func documentDirectoryPath() -> URL? {
        let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        
        return path.first
    }
}
