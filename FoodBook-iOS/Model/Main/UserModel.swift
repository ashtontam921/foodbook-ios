//
//  UserModel.swift
//  FoodBook-iOS
//
//  Created by AshtonTam on 06/08/2021.
//
import UIKit
import ObjectMapper
import SwiftyJSON
import CryptoSwift

class UserModel: Mappable{
    static let userKey = "UserKey"
    static var shared: UserModel?
    
    //For data keeping
    var storageKey = ""
    
    var token = "" //Session token, if any
    var recipeList: [RecipeModel] = []
    
    init(){
        
    }
    
    required init(map: Map) {
    }
    
    func mapping(map: Map) {
        //For API mapping
    }
    
    init(dictionary: Dictionary<String, Any>) {
        storageKey = dictionary["storageKey"] as? String ?? ""
        token = dictionary["token"] as? String ?? ""
        
        recipeList.removeAll()
        if let recipeDic = dictionary["recipeList"] as? [Dictionary<String, Any>]{
            for dic in recipeDic{
                recipeList.append(RecipeModel(dictionary: dic))
            }
        }
    }
    
    func encode() -> Dictionary<String, Any> {
        var dictionary : Dictionary = Dictionary<String, Any>()
        dictionary["storageKey"] = storageKey
        dictionary["token"] = token
        
        var recipeDic: [Dictionary<String, Any>] = []
        for (index, obj) in recipeList.enumerated(){
            obj.userStorageKey = self.storageKey
            obj.index = index
            
            recipeDic.append(obj.encode())
        }
        dictionary["recipeList"] = recipeDic
        
        return dictionary
    }
    
    func saveUserToUserDefault() {
        let encodedObject = encode()
        UserDefaults.standard.set(encodedObject, forKey: UserModel.userKey)
        UserDefaults.standard.set(encodedObject, forKey: storageKey)
        UserDefaults.standard.synchronize()
    }
}


extension UserModel{
    class func getUserFromUserDefault(username: String, password: String, completion: (() -> Void)?){
        let user = UserModel()
        user.storageKey = UserModel.getUserStorageKey(username: username, password: password)
        
        //Get user record from local, if exist
        if let dict = UserDefaults.standard.object(forKey: user.storageKey) as? Dictionary<String, Any> {
            UserModel.shared = UserModel(dictionary: dict)
        }else{
            //Create a User record with sample records
            user.recipeList = [RecipeModel.sampleDish1(), RecipeModel.sampleDish2(), RecipeModel.sampleDish3()]
            
            UserModel.shared = user
        }
        
        completion?()
    }
    
    class func getCurrentUser() -> UserModel? {
        if UserModel.shared == nil{
            if let dict = UserDefaults.standard.object(forKey: UserModel.userKey) as? Dictionary<String, Any> {
                UserModel.shared = UserModel(dictionary: dict)
            }else{
                AppDelegate.setRootToLoginView(error: "Session Expired".localized())
            }
        }
        
        return UserModel.shared
    }
    
    class func getUserToken() -> String?{
        return UserModel.getCurrentUser()?.token
    }
    
    class func getUserStorageKey(username: String, password: String) -> String{
        return (username + password).sha256()
    }
}
