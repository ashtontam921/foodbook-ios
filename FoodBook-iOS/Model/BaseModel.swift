
import UIKit
import ObjectMapper

class BaseModel: Mappable {
    var result: Bool?
    var message = ""
    var message_code = ""
    var errors: [BaseErrorModel] = []
    
    required init(map: Map) {
    }
    
    func mapping(map: Map) {
        message             <- map["message"]
        message_code        <- map["message_code"]
        errors              <- map["errors"]
        
        var temp: Int?
        temp                <- map["result"]
        
        if let unwrapTemp = temp{
            result = unwrapTemp == 1 ? true : false
        }
    }
}

class BaseErrorModel: Mappable {
    var param = ""
    var error = ""
    
    required init(map: Map) {
    }
    
    func mapping(map: Map) {
        param       <- map["param"]
        error       <- map["error"]
    }
}
